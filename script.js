// Load the stylesheet dynamically
function loadStylesheet() {
  var link = document.createElement('link');
  link.rel = 'stylesheet';
  link.href = 'style.css';
  document.head.appendChild(link);
}

function updatePage() {
  var logListContainer = document.getElementById('log-list-container');
  var prevScrollBottom = logListContainer ? logListContainer.scrollHeight - logListContainer.scrollTop : 0;
  var wasAtTop = logListContainer ? logListContainer.scrollTop === 0 : false;
  console.log('Previous Scroll Position from Bottom: ' + prevScrollBottom);

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      var content = document.getElementById("content");
      content.innerHTML = this.responseText;

      // Restore scroll position after updating
      logListContainer = document.getElementById('log-list-container');
      
      if (wasAtTop) {
        // If we were at the top, stay at the top
        logListContainer.scrollTop = 0;
      } else {
        // Else, keep the same position from the bottom
        logListContainer.scrollTop = logListContainer.scrollHeight - prevScrollBottom;
      }
      
      console.log('Restored Scroll Position from Bottom: ' + (logListContainer.scrollHeight - logListContainer.scrollTop));
    }
  };
  xmlhttp.open("GET", "/", true);
  xmlhttp.send();
}

// Refresh the page every 5 seconds
setInterval(updatePage, 5000);

// Load the stylesheet and update the page on initial load
window.onload = function () {
  loadStylesheet();
  updatePage();
};