"""This module contains the HTTP server for the CDC debug screen."""
import time

import http.server
import socketserver


def highlight_log(log):
  """Returns the CSS class for the given log entry.
  Args:
      log (str): the log entry as a string

  Returns:
      str: the CSS class for the given log entry

  """
  if "DEBUG" in log:
    return "log-debug"
  if "INFO" in log:
    return "log-info"
  if "ERROR" in log:
    return "log-error"
  if "WARNING" in log:
    return "log-warning"
  return ""


def generate_status_class(status, classes):
  """Returns the CSS class for the given status.

  Args:
      status (str): the status as a string
      classes (dict): the dictionary of status classes. The keys are the CSS class names, the values are lists of status strings

  Returns:
      str: the CSS class for the given status

  """
  status_class = ""
  for key, value in classes.items():
    if status in value:
      status_class = key
      break
  return status_class


def generate_range_info(cdc_range):
  """Returns the HTML for the range info.

  Args:
      cdc_range (str): the CDC range (OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A as strings)

  Returns:
      str: the HTML for the range info

  """
  if cdc_range == "OFF":
    range_class = "off"
  else:
    range_class = "on"
  return f'<div class="status-box {range_class}">{cdc_range}</div>'


def generate_relay_columns(relays, active_relays):
  """Returns the HTML for the relay columns.

  Args:
      relays (list): the list of existing relays as a list of strings
      active_relays (list): the list of active relays as a list of strings

  Returns:
      str: the HTML for the relay columns

  """
  return "".join(
      f'<th class="{"" if relay not in active_relays else "active"}">{relay}</th>'
      for relay in relays
  )


def generate_module_info(module):
  """Returns the HTML for the module info.

  Args:
      module (dict): the module info as a dictionary. It must contain the following keys:
          Name (str): the module name. Optional, defaults to the module ID
          ID (str): the module ID
          Temperature (str): the module temperature in °C

  Returns:
      str: the HTML for the module info

  """
  id_value = module.get("ID", None)
  module_name = module.get("Name", id_value if id_value is not None and isinstance(id_value, str) else "")
  temperature = module.get("Temperature", "")
  return f"<li>{module_name} - {temperature}°C</li>"


def calc_voltage_percentage(nominal, value):
  """Returns the percentage of the given value with respect to the nominal value, used to display the voltage meters.

  Args:
      nominal (float): the nominal value
      value (float): the value to calculate the percentage of the nominal value


  Returns:
      float: the percentage of the given value with respect to the nominal value
  """
  if nominal >= 0:
    lower_bound = nominal * 0.9
    upper_bound = nominal * 1.1
  else:
    lower_bound = nominal * 1.1
    upper_bound = nominal * 0.9

  if value <= lower_bound:
    return 0
  elif value >= upper_bound:
    return 100
  else:
    range_diff = upper_bound - lower_bound
    val_diff = value - lower_bound
    return (val_diff / range_diff) * 100


def generate_v_monitor_info(voltages):
  """Returns the HTML for the voltage monitor info.

  Args:
      voltages (dict): the voltage monitor info as a dictionary. It must contain the following keys:
          5V (float): the 5V rail voltage
          12V (float): the 12V rail voltage
          +15V (float): the +15V rail voltage
          -15V (float): the -15V rail voltage

  Returns:
      str: the HTML for the voltage monitor info

  """
  return f"""<div class="voltage-meter">
                  <h4>5V Rail</h4>
                  <div class="scale">
                    <div class="tall-mark" style="left: 0;"></div>
                    <div class="tall-mark" style="left: 50%;"></div>
                    <div class="tall-mark" style="right: 0;"></div>
                    <div class="marker" style="left: {calc_voltage_percentage(5, voltages['5V'])}%;">{voltages['5V']:.2f}V</div>
                  </div>
                  <div class="full-scale-values">
                    <span>4.5V</span>
                    <span>5.0V</span>
                    <span>5.5V</span>
                  </div>
                </div>

                <div class="voltage-meter">
                  <h4>12V Rail</h4>
                  <div class="scale">
                    <div class="tall-mark" style="left: 0;"></div>
                    <div class="tall-mark" style="left: 50%;"></div>
                    <div class="tall-mark" style="right: 0;"></div>
                    <div class="marker" style="left: {calc_voltage_percentage(12, voltages['12V'])}%;">{voltages['12V']:.2f}V</div>
                  </div>
                  <div class="full-scale-values">
                    <span>10.8V</span>
                    <span>12.0V</span>
                    <span>13.2V</span>
                  </div>
                </div>

                <div class="voltage-meter">
                  <h4>+15V Rail</h4>
                  <div class="scale">
                    <div class="tall-mark" style="left: 0;"></div>
                    <div class="tall-mark" style="left: 50%;"></div>
                    <div class="tall-mark" style="right: 0;"></div>
                    <div class="marker" style="left: {calc_voltage_percentage(15, voltages['+15V'])}%;">{voltages['+15V']:.2f}V</div>
                  </div>
                  <div class="full-scale-values">
                    <span>13.5V</span>
                    <span>15.0V</span>
                    <span>16.5V</span>
                  </div>
                </div>

                <div class="voltage-meter">
                  <h4>-15V Rail</h4>
                  <div class="scale">
                    <div class="tall-mark" style="left: 0;"></div>
                    <div class="tall-mark" style="left: 50%;"></div>
                    <div class="tall-mark" style="right: 0;"></div>
                    <div class="marker" style="left: {calc_voltage_percentage(-15, voltages['-15V'])}%;">{voltages['-15V']:.2f}V</div>
                  </div>
                  <div class="full-scale-values">
                    <span>-16.5V</span>
                    <span>-15.0V</span>
                    <span>-13.5V</span>
                  </div>
                </div>"""


def generate_html(data):
  """Returns the HTML for the debug screen.

  Args:
      data (dict): the CDC data. 

  Data:
        cdc_state (str): the CDC state (INIT, ACTIVE, NULLING, IDLE as strings)

        vb_status (str): the voltage booster status (VB_OK, VB_DISCONNECTED, VB_TEMP_FAULT, VB_PWR as strings) 

        fadem_status (str): the FADEM status (FADEM_NORMAL, FADEM_RAMPDOWN, FADEM_DEGAUSS, FADEM_INIT, FADEM_IDLE as strings) 

        primrel_status (str): the primrel status (ACTIVE, GET_GPIO, SET_GPIO, CFG_GPPU, CFG_IODIR, INIT_MCP, ERROR as strings) 

        log (list): the CDC log as a list of strings

        range (str): the CDC range (OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A as strings) 

        primary_relays (list): the list of primary relays as a list of strings 

        onoff_relays (list): the list of on/off relays as a list of strings 

        pol_relays (list): the list of polarity relays as a list of strings 

        output_current (float): the output current 

        v_monitor (dict): the voltage monitor info as a dictionary. It must contain the following keys: 5V (float): the 5V rail voltage, 12V (float): the 12V rail voltage, +15V (float): the +15V rail voltage,  -15V 
        (float): the -15V rail voltage


  Returns:
      str: the HTML for the debug screen

  """
  state = data["cdc_state"]
  voltage_booster_status = data["vb_status"]
  fadem_state = data["fadem_status"]
  primrel_state = data["primrel_status"]

  state_classes = {
      "error": ["INIT"],
      "ok": ["ACTIVE"],
      "busy": ["NULLING", "IDLE"],
  }
  state_class = generate_status_class(state, state_classes)

  vb_status_classes = {
      "error": ["VB_DISCONNECTED", "VB_TEMP_FAULT", "VB_PWR"],
      "ok": ["VB_OK"],
  }
  vb_status_class = generate_status_class(
      voltage_booster_status, vb_status_classes)

  fadem_state_classes = {
      "busy": ["FADEM_RAMPDOWN", "FADEM_DEGAUSS"],
      "ok": ["FADEM_NORMAL"],
      "error": ["FADEM_INIT", "FADEM_IDLE"],
  }
  fadem_state_class = generate_status_class(fadem_state, fadem_state_classes)

  primrel_state_classes = {
      "busy": ["GET_GPIO", "SET_GPIO", "CFG_GPPU", "CFG_IODIR", "INIT_MCP"],
      "ok": ["ACTIVE"],
      "error": ["ERROR"],
  }
  primrel_state_class = generate_status_class(
      primrel_state, primrel_state_classes)

  range_line = generate_range_info(data["range"])

  log_html = "".join(
      f'<li class="{highlight_log(log)}">{log}</li>' for log in reversed(data["log"]))

  html = f"""
        <!DOCTYPE html>
        <html>
        <head>
            <title>CDC Debug Screen</title>
            <meta charset="utf-8">
            <link rel="stylesheet" href="style.css">
            <script src="script.js"></script>
        </head>
        <body onload="updatePage()">
            <div id="content">
            <h1>CDC Debug Screen</h1>
            <div class="container">
                <div class="status-column">
                    <h3>CDC State <div class="status-box {state_class}">{state}</div></h3>
                    <h3>Voltage Booster <div class="status-box {vb_status_class}">{voltage_booster_status}</div></h3>
                    <h3>FADEM Status <div class="status-box {fadem_state_class}">{fadem_state}</div></h3>
                    <h3>Primrel Status <div class="status-box {primrel_state_class}">{primrel_state}</div></h3>
                </div>
                <div class="log-column">
                      <h2>Polarity Relays</h2>
                      <table class="relay-table">
                          <tr>
                              {generate_relay_columns(data["primary_relays"], data["pol_relays"])}
                          </tr>
                      </table>

                      <h2>On/Off Relays</h2>
                      <table class="relay-table">
                          <tr>
                              {generate_relay_columns(data["primary_relays"], data["onoff_relays"])}
                          </tr>
                      </table>
                  </div>
                <div class="info-column">
                <h2>Module Status</h2>
                <ul>
                    {"".join(generate_module_info(module) for module in data["module_status"])}
                </ul>
                </div>
            </div>
            <div class="container">
                <div class="status-column">
                    <h3>Range {range_line}</h3>
                    
                    <h3>Output current</h3>
                    <p style="font-size: 24px;">{data['output_current']:.8f}A</p>
                </div>
                <div  id="log-list-container"  class="log-column">
                    <h2>Log</h2>
                    <ul class="log-list">
                        {log_html}
                    </ul>
                </div>
                <div class="info-column">
                <h2>PSU Status</h2>
                {generate_v_monitor_info(data["v_monitor"])}
            </div>
          </div>
        </body>
        </html>
        """
  return html


class RequestHandler(http.server.SimpleHTTPRequestHandler):
  """HTTP request handler for the debug screen.

  Attributes:
      cdc (CDC): the CDC object
      sysmon (Sysmon): the Sysmon object
      buffer_time (float): data caching period (5 seconds)
      cache (dict): the cached data
      cache_timestamp (float): the timestamp of the cached data, for cache invalidation

  """

  def __init__(self, *args, **kwargs):
    """ Initializes the request handler.

    Args:
        *args: positional arguments
        **kwargs: keyword arguments. The following keyword arguments are required:
            cdc (CDC): the CDC object
            sysmon (Sysmon): the Sysmon object

    Raises:
        TypeError: if the required keyword arguments are not passed

    """
    self.cdc = kwargs.pop('cdc', None)
    self.sysmon = kwargs.pop('sysmon', None)
    self.buffer_time = 5.0  # data caching period (5 seconds)
    self.cache = None  # initialize the cache
    self.cache_timestamp = 0  # initialize the cache timestamp
    super().__init__(*args, **kwargs)

  def fetch_cdc_data(self):
    """Returns the CDC data for web visualization.

    Returns:
        dict: the CDC data
    """
    if self.cache is not None and time.time() - self.cache_timestamp <= self.buffer_time:
      # if data in cache is still within buffer time, return it
      return self.cache
    else:
      # Get CDC data
      cdc_state = self.cdc.state.name
      vb_status = self.cdc.primrel.vb_status_cache.name
      fadem_status = self.cdc.fadem.state_cache.name
      primrel_status = self.cdc.primrel.state_cache.name
      log_entries = self.cdc.logger.retrieve_logs()
      module_status = self.sysmon.onewire_devices
      primary_relays = [
          "2048T", "1024T", "512T", "256T", "128T", "64T", "32T",
          "16T", "8T", "4T", "2T", "1T", "1TCAL", "1TDAC"
      ]
      onoff_relays = self.cdc.primrel.on_off_relays_cache.get_set()
      pol_relays = self.cdc.primrel.pol_relays_cache.get_set()
      output_current = self.cdc.properties['CALSYS.CDC.I_REF'].value[0]
      cdc_range = self.cdc.properties['CALSYS.CDC.OUTPUT'].value[0]
      v_mon_5v = self.sysmon.voltage_rails['5V']['last_read_voltage']
      v_mon_12v = self.sysmon.voltage_rails['12V']['last_read_voltage']
      v_mon_15v = self.sysmon.voltage_rails['+15V']['last_read_voltage']
      v_mon_n15v = self.sysmon.voltage_rails['-15V']['last_read_voltage']

      # Update the cache and its timestamp
      self.cache = {
          "cdc_state": cdc_state,
          "vb_status": vb_status,
          "fadem_status": fadem_status,
          "primrel_status": primrel_status,
          "log": log_entries,
          "module_status": module_status,
          "pol_relays": pol_relays,
          "onoff_relays": onoff_relays,
          "primary_relays": primary_relays,
          "output_current": output_current,
          "range": cdc_range,
          "v_monitor": {
              "5V": v_mon_5v,
              "12V": v_mon_12v,
              "+15V": v_mon_15v,
              "-15V": v_mon_n15v
          }
      }
      self.cache_timestamp = time.time()

      return self.cache

  def do_GET(self):
    """Handles GET requests."""
    if self.path == "/":
      # Fetch CDC data
      data = self.fetch_cdc_data()

      # Generate HTML code
      html = generate_html(data)

      # Send response
      self.send_response(200)
      self.send_header("Content-type", "text/html")
      self.end_headers()
      self.wfile.write(bytes(html, "utf8"))
    elif self.path == "/style.css":
      super().do_GET()
    elif self.path == "/script.js":
      super().do_GET()
    else:
      # return 404 error
      self.send_error(404, "File not found")


def run_server(cdc, sysmon):
  """Starts the HTTP server.

  Args:
      cdc (CDC): the CDC object
      sysmon (Sysmon): the Sysmon object

  Returns:
      bool: true if the server was stopped intentionally, false if it was stopped due to an error

  """
  host = "0.0.0.0"
  port = 80

  def handler(*args, cdc=cdc, sysmon=sysmon, **kwargs):
    return RequestHandler(*args, cdc=cdc, sysmon=sysmon, **kwargs)

  try:
    with socketserver.TCPServer((host, port), handler) as server:
      cdc.logger.info(f"HTTP: Starting server at {host}:{port}")
      try:
        server.serve_forever()
      except KeyboardInterrupt:
        return True  # true means that the server was stopped intentionally

      server.server_close()
  except OSError as exc:
    cdc.logger.error(f"HTTP: Could not start server: {exc}")
    return False  # false means that the server was stopped due to an error
