# CDC Control application
Usefull script to send commands: https://gitlab.cern.ch/-/snippets/2706

## TODO
- [x] Unit test for fgc_ncrp_handler.py
- [x] Unit test for fgcd.py
- [x] Unit test for fgc_ncrp_server.py 
- [x] Unit test for cdc.py
- [x] Unit test for fgcd_gateway_serial.py [not testing it]
- [x] Unit test for fgcd_cdc.py
- [x] Unit test for drivers/spi_controller.py
- [x] Unit test for drivers/primrel.py
- [x] Unit test for drivers/fadem.py
- [ ] Add linting to pipeline
- [x] State machine for the CDC
- [x] Implementation of the cdc_properties.py
- [x] fadem_test.py should not rely on constants from fadem.py
- [x] Unit test for cdc_sysmon.py
- [x] Settings file should let you set most of the parameters.
- [x] Evaluate the possibility of a web page for the monitoring and fault diagnosis
- [x] Add a logger!! and logging policy
- [x] Consider splitting the cdc_properties.py into one file per class
- [x] Add led controls. One led should be reserved for petalinux boot, one for the correct starting of the cdc.py and all its threads, one may be indicator of nulling saturation and the last may be a fault indicator.
- [x] Move the nulling in the NULL.AUTO property from the CDC.OUTPUT
- [ ] Unit test all the things that make sense to be tested
- [ ] Write my master thesis :D

### LEDs
- gpio1014 is the red led on the cora board
- gpio1015 is the green led on the cora board
- gpio1016 is the blue led on the cora board
- gpio1020: Linux system boot. first green led on the carrier board
- gpio1021: CDC initialization. Second green led on the carrier board
- gpio1022: Null saturation. First red led on the carrier board
- gpio1023: Faults. Second red led on the carrier board


## Settings file
It may be a good idea to add a settings file that is parsed at the start of the program. This would allow us to set the following parameters:
### NCRP Server
- CDC ID
- CDC Name
- Enable gateway emulation
  - Gateway ID
  - Gateway Name
- Port number (default 1905)
### CDC


### Fadem
- SPI number

### PrimRel
- SPI number

### Nullmeter ADC
- SPI number

### Sysmon
- Onewire ip address
- Frontend voltage divider ratio
- Counts to voltage nominal gain
- Num of devices
- Onewire device IDs check
  - Onewire device IDs
- Max and min temperatures to trigger a fault
