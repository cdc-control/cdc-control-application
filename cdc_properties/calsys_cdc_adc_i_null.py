"""Module for the CalsysCdcAdcINull property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysCdcAdcINull(fgc_property.Property):
  """This property returns the value of the offset current measured by the current calibrator in uA.

  Set:
    This property cannot be set.

  Get:
    When this property is read, it returns the value of the offset current measured by the current calibrator in uA.

  NCRP Errors:
    - **11 set not perm**: this property cannot be set
  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.ADC.I_NULL',
        access='r',
        val_type=float,
        getters=[self.cdc.nullmeter.get_I_null],
        unit='uA',
        description='Accessing this property triggers a reading of the ADC channel which measures the offset current of the CDC and returns a value in uAmp. SET not permitted'
    )
