"""Module for the CalsysCdcDacRaw property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors


class CalsysCdcDacRaw(fgc_property.Property):
  """This class is used to create the CALSYS.CDC.DAC.RAW property. This property returns the raw value corresponding to the preset output of the specified DAC channel. It has two channels: FADEM dac and PRIMREL dac. The first is used to do the nulling in the CDC, the second sets the current in the 1 turn winding. Both DACs are 16 bit signed integers (0V = 0x8000)

  Set:
    When this property is set, it sets the raw value of the specified DAC channel. The value must be an integer between -0x7FFF and 0x7FFF. If the value is outside of this range, a BadValue error is raised. Both DACs can be set at the same time by sending a list of two integers. 

  Get:
    When this property is read, it returns the raw value corresponding to the preset output of the specified DAC channel. The value is an integer between -0x7FFF and 0x7FFF.

  NCRP Errors:
    - **28 out of limits**: the value is outside of the allowed range of values (-0x7FFF to 0x7FFF)
    - **20 bad array idx**: the array specifies a channel that does not exist, valid channels are 0 and 1
    - **21 bad array len**: the array is not the correct length ([idx], [start,], [start, stop], [start, stop, step] are all valid)
    - **18 bad integer**: the set value(s) are not integers

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.DAC.RAW',
        access='rw',
        val_type=int,
        length=2,
        getters=[self.cdc.fadem.get_dac, self.cdc.primrel.get_dac],
        setters=[self.cdc.fadem.set_dac, self.cdc.primrel.set_dac],
        min_val=-0x7FFF,
        max_val=0x7FFF,
        unit='DAC',
        description='This property returns the raw value corresponding to the preset output of the specified DAC channel.'
    )
