"""Module for the CalsysDcmAdcCal property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors


class CalsysDcmAdcCal(fgc_property.Property):
  """Setting this property with no parameters will trigger a calibration of the ADC offset and gain. The ADC offset calibration is done using channel 7 of the nulling meter board ADC, which is permanently connected to the voltage corresponding to the zero of the ADC. As for the gain, the initial accuracy of the internal VREF of the ADC is enough that a calibration is not required.
  The result of the offset calibration shall be stored in CALSYS.DCM.ADC.RAW_ZERO.

  Set:
    Sets the ADC calibration. The value has to be empty.

  Get:
    Gets the ADC calibration. CALIBRATED if the ADC is calibrated, UNCALIBRATED otherwise.

  NCRP Errors:
    - **23 bad parameter**: The value is not empty when setting the property.

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.DCM.ADC.CAL',
        access='rwp',
        val_type=bool,
        setters=[self.do_calibrate],
        getters=[self.is_calibrated],
        description='This property indicates the calibration value of the ADC',
        default=False
    )
  # we override the set method

  def do_calibrate(self, _):
    """This method is called when the property is set.

    This method will calibrate the ADC offset and gain. The ADC offset calibration is done using channel 7 of the nulling meter board ADC, which is permanently connected to the voltage corresponding to the zero of the ADC. As for the gain, the initial accuracy of the internal VREF of the ADC is enough that a calibration is not required.

    The result of the offset calibration shall be stored in CALSYS.DCM.ADC.RAW_ZERO.

    """
    # TADC calibration consists only of the zero calibration
    zero = self.cdc.nullmeter.get_zero()
    self.cdc.nullmeter.set_zero(zero)
    # we set the zero in the CALSYS.DCM.ADC.RAW_ZERO property
    self.cdc.logger.info(
        f"setting the zero in the CALSYS.DCM.ADC.RAW_ZERO property: {zero}")
    self.cdc.properties['CALSYS.DCM.ADC.RAW_ZERO']._set([zero])
    # we set the property to True
    self.cdc.logger.info("setting the CALSYS.DCM.ADC.CAL property to True")
    self.value[0] = True

  def is_calibrated(self):
    """This method is called when the property is read.

    This method will return CALIBRATED if the ADC is calibrated, UNCALIBRATED otherwise.
    """
    if self.value[0]:
      return "CALIBRATED"
    else:
      return "UNCALIBRATED"

  def set(self, command):
    """This method is called when the property is set.

    Args:
      command: the command that is sent to the property

    Raises:
      BadParameter: if the command has a payload that is not an empty string

    """
    if command.payload == ['']:
      command.payload = [True]
    else:
      raise errors.BadParameter
    super().set(command)
