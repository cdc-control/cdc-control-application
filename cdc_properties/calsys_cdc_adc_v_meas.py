"""Module for the CalsysCdcAdcVMeas property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysCdcAdcVMeas(fgc_property.Property):
  """This property returns the value of the voltage measured by the current calibrator. Accessing this property triggers a reading of the ADC channel which measures the output voltage of the CDC and returns a value in Volt. This measurement includes the voltage across the secondary windings of the CDC and across the current sense resistor. 

  Set:
    This property cannot be set.

  Get:
    When this property is read, it returns the value of the voltage measured by the current calibrator in Volt.

  NCRP Errors:
    - **11 set not perm**: this property cannot be set
  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.ADC.V_MEAS',
        access='r',
        val_type=float,
        getters=[self.cdc.nullmeter.get_V_compliance],
        unit='V',
        description='Accessing this property triggers a reading of the ADC channel which measures the output voltage of the CDC and returns a value in Volt. This measurement includes the voltage across the secondary windings of the CDC and across the current sense resistor. SET not permitted'
    )
