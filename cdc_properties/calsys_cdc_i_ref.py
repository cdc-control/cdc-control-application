"""Module for the CalsysCdcIRef property of the CDC."""

import time
from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors


class CalsysCdcIRef(fgc_property.Property):
  """This property gets and sets the current reference of the current calibrator.

  This property is only available when the CDC is in the ACTIVE state. The value of this property is the current reference in amperes. The value is a float. 

  The current is output in steps to avoid upsetting the core of the ZFD. Too large steps mean that the flux in the core cannot be maintained close to zero and the core will be magnetized.

  The step size varies depending on the range. For the 1A range the step size is 50 mA, for the 2.5A range the step size is 125 mA, for the 5A range the step size is 250 mA and for the 10A range the step size is 500 mA. 


  Set:
    Sets the Iref current for the current calibrator by manipulating the primary relays and the 1T DAC. The value has to be within the range of the current range. 

  Get:
    Gets the I ref current for the current calibrator.

  NCRP Errors:
    - **34 bad state**: The CDC is not in the ACTIVE state.
    - **34 bad state**: The CDC is in the OFF range (set CALSYS.CDC.OUTPUT to a valid range first)
    - **28 out of limits**: The value is out of limits for the current range. 
    - **19 bad float**: The value is not a float. 

  """

  def __init__(self, cdc):
    self.cdc = cdc
    self.ranges = {
        'RANGE_1A': {'fullscale': 1, 'turns': 40, 'max_step': 0.05},
        'RANGE_2A5': {'fullscale': 2.5, 'turns': 16, 'max_step': 0.125},
        'RANGE_5A': {'fullscale': 5, 'turns': 8, 'max_step': 0.25},
        'RANGE_10A': {'fullscale': 10, 'turns': 4, 'max_step': 0.5},
    }
    super().__init__(
        name='CALSYS.CDC.I_REF',
        access='rw',
        val_type=float,
        setters=[self.set_iref],
        unit='A',
        description='This property gets and sets the current reference of the current calibrator.'
    )

  def set_iref(self, value):
    # check that we are in ACTIVE state
    if self.cdc.state != self.cdc.CDCStates.ACTIVE:
      self.cdc.logger.error(
          f'Cannot set I_REF when state is {self.cdc.state}')
      raise errors.BadState

    # TODO: check for alarms

    # check that the value is in the range
    cdc_range = self.cdc.properties['CALSYS.CDC.OUTPUT'].value[0]

    # check that we are in a valid range (not OFF)

    if cdc_range == 'OFF':
      self.cdc.logger.error(f'Cannot set I_REF when range is {cdc_range}')
      raise errors.BadState

    try:
      value = float(value)
    except ValueError as exc:
      self.cdc.logger.error(
          f'Cannot set I_REF to {value} A, float conversion failed')
      raise errors.BadFloat from exc

      # check that the value is in the range
    if abs(value) > self.ranges[cdc_range]['fullscale']:
      self.cdc.logger.error(
          f'Cannot set IREF to {value} A when range is {cdc_range}')
      raise errors.OutOfLimits

    # get the current Iref value
    current_iref = self.value[0]

    if self.cdc.stepping:
      # Calculate the difference and steps required to reach the desired value
      iref_difference = abs(value - current_iref)
      max_step = self.ranges[cdc_range]['max_step']

      # Implement step increment
      while iref_difference > max_step:
        self.cdc.logger.debug(
            f'Stepping required. Current IREF: {current_iref} A')
        current_iref += max_step if value > current_iref else -max_step
        self._set_iref_value(current_iref)
        iref_difference = abs(value - current_iref)
        # wait for the current to settle
        time.sleep(0.5)

    self._set_iref_value(value)

  def _set_iref_value(self, value):
    ten_milliamp_units = value * 100
    # then, normalize it for the range we are in
    # * The folliwng, in the case of the PBCx5, is wrong, as the range
    # * is saved as 5A but it actually is 1A.
    # * But this does us a good service, effectively imposing a
    # * current to be set that is 1/5 of the requested value, which
    # * is what we want!
    cdc_range = self.cdc.properties['CALSYS.CDC.OUTPUT'].value[0]
    ten_milliamp_turns = ten_milliamp_units * self.ranges[cdc_range]['turns']

    integer_part = int(ten_milliamp_turns)
    decimal_part = ten_milliamp_turns - integer_part

    self._set_primary_relays(integer_part)
    self._set_1tdac(decimal_part)

    self.value[0] = value

  def _set_primary_relays(self, value):
    """We calculate which relays should be on based on the current value. Since the primary relays can be calculated directly from a 12 bit number by the PrimaryRelays class, we just call that"""
    # set the orientation depending on the sign
    if value < 0:
      negative = True
    else:
      negative = False

    self.cdc.primrel.set_negative(negative)
    self.cdc.logger.debug(f'Setting primary relays to {value:012b}')
    self.cdc.primrel.set_primary_relays(abs(value))

  def _set_1tdac(self, value):
    # since the dac is calibrated in the primrel, we can just set the value directly, scaling it to 0x8000

    # turn on the 1t dac relay
    self.cdc.primrel.set_1t_dac_relay(True)
    # set the sign of the current
    self.cdc.primrel.set_1t_dac_negative(value < 0)
    value = abs(value)
    self.cdc.primrel.set_dac(int(value*0x7FFF))
    return 0
