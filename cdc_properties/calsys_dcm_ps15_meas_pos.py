"""Module for the CalsysDcmPs15MeasPos property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysDcmPs15MeasPos(fgc_property.Property):
  """This property returns the value of the +15V power supply in Volt.

  Set:
    Set not permitted.

  Get:
    Gets the value of the +15V power supply in Volt.

  NCRP Errors:
    - **11 set not perm**: The property is read only.

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.DCM.PS15.MEASPOS',
        access='r',
        val_type=float,
        getters=[lambda: self.cdc.sysmon.read_calibrated_voltage('+15V')],
        unit='V',
        description='Accessing this property triggers a measurement of the positive 15V power supply and returns the value in Volt.'
    )
