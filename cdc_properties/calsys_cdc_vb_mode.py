"""Module for the CalsysCdcVBMode property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysCdcVBMode(fgc_property.Property):
  """This property returns the status of the VB connection.

  Set:
    Set not permitted.

  Get:
    Gets the status of the VB connection. The value is an integer: 0 if the VB is not connected, 1 if the VB is connected.

  NCRP Errors:
    - **11 set not perm**: The property is read only.

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.VB_MODE',
        access='r',
        getters=[self.is_vb_connected],
        val_type=bool,
        description='Getting this property will tell you if a VB is connected to the CDC (VB_MODE =1) or not (VB_MODE =0).'
    )

  def is_vb_connected(self):
    """we return 1 if the VB is connected, 0 otherwise"""
    if self.cdc.primrel.get_vb_status() == self.cdc.primrel.VBStatus.VB_DISCONNECTED:
      return "0"
    else:
      return "1"
