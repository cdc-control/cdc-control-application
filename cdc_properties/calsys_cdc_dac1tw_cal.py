"""Module for the CalsysCdcDac1TwCal property of the CDC."""

import time
from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors


class CalsysCdcDac1TwCal(fgc_property.Property):
  """This property contains the calibration state of the DAC 1TW system. Setting this property with no parameters will trigger a calibration of the DAC. 

  The DAC calibration is not permitted in VB mode and it requires the CDC to be ON and at zero current with no offset present. 

  The calibration of the zero is done first followed by the calibration of the values corresponding to 10mA which is measured by doing a back to back between the DAC winding with the DAC at full scale and the “calibration” winding with the 10mA reference.

  The calibration sets the zero and full scale values in the CALSYS.CDC.DAC1TW.ZERO and CALSYS.CDC.DAC1TW.FS properties. 

  Set:
    Triggers the 1TW DAC calibration

  Get:
    When this property is read, it returns the calibration state of the DAC 1TW system. The possible values are CALIBRATED and UNCALIBRATED.

  NCRP Errors:
    - **79 null failed**: the nulling failed, it was not possible to bring the current to zero by manipulating the DAC. The DAC was restored to its previous state.
    - **34 bad state**: the CDC must be in the ACTIVE state to calibrate the DAC
    - **34 bad state**: CALSYS.CDC.I_REF must be set to 0A to calibrate the DAC
    - **34 bad state**: the VB must be disconnected to calibrate the DAC
    - **23 bad parameter**: if the command has a payload that is not an empty string

  """

  def __init__(self, cdc):
    self.cdc = cdc
    # super is used to hold the bool, while derived class converts it to string
    super().__init__(
        name='CALSYS.CDC.DAC1TW.CAL',
        access='rw',
        val_type=bool,
        setters=[self.set_and_calibrate],
        getters=[self.is_calibrated],
        description='This property contains the calibration state of the DAC 1TW system. Setting this property with no parameters will trigger a calibration of the DAC. The DAC calibration is not permitted in VB mode and it requires the CDC to be ON and at zero current with no offset present. The calibration of the zero is done first followed by the calibration of the values corresponding to 10mA which is measured by doing a back to back between the DAC winding with the DAC at full scale and the “calibration” winding with the 10mA reference.'
    )
    self.ranges_map = {
        'RANGE_1A': 1.0,
        'RANGE_2A5': 2.5,
        'RANGE_5A': 5.0,
        'RANGE_10A': 10.0
    }

  def _nulling(self, zero: int = 0):
    """Function for bringing the dac to zero by reading the nullmeter. It is used in the calibration of the DAC 1 turn winding. The function is based on the PID algorithm.

    Args:
      zero (int, optional): The initial value of the DAC. Defaults to 0.

    Raises:
      errors.NullFailed: Raised if the nulling fails after 1000 attempts


    """
    nr = 1000
    ki = 1000
    kp = 500
    # PID process to null the dac by reading nullmeter.get_I_null()
    nulling_attempts = 0
    # Threshold for nulling completion
    nulling_threshold = 0.05
    consecutive_below_threshold = 0
    sample_time = 0.01  # 10 ms
    integral = 0.0  # Initial integral value
    alpha = 0.2  # Smoothing factor for the low-pass filter
    filtered_I_null = 0  # Initial filtered value of I_null

    # Get the current range
    range_value = self.cdc.properties['CALSYS.CDC.OUTPUT'].value[0]

    relative_gain = self.ranges_map[range_value]
    while nulling_attempts < nr:
      # Get the current I_null
      raw_I_null = -self.cdc.nullmeter.get_I_null() / relative_gain
      # Apply low-pass filter
      filtered_I_null = alpha * raw_I_null + (1 - alpha) * filtered_I_null
      # Calculate the error
      error = filtered_I_null
      # Calculate the integral
      integral += error * sample_time * ki
      # saturate the integral, to avoid windup
      clamp_min = -2 ** 15  # Lower bound for clamping
      # Upper bound for clamping (2 ** 15 - 1 to include the maximum value)
      clamp_max = 2 ** 15 - 1
      if integral < clamp_min:
        integral = clamp_min
      elif integral > clamp_max:
        integral = clamp_max
      proportional = error * kp
      # Calculate the output
      output = proportional + integral
      # Saturate the output
      output = max(min(output, 2 ** 15), -2 ** 15)
      # remove the zero offset
      output = int(output-zero)
      # Set the output
      if output > 0x7fff:
        output = 0x7fff
      elif output < -0x7FFF:
        output = -0x7fff
      self.cdc.primrel.set_dac(output)
      # Print the output, making sure that all numbers keep the same width for easier reading
      print(
          f'{filtered_I_null: 01.5f} {error: 01.3f} {proportional: 05.3f} { integral: 05.3f} {output: 05.3f}')
      time.sleep(sample_time)
      if abs(error) < nulling_threshold:
        consecutive_below_threshold += 1
      else:
        consecutive_below_threshold = 0
      if consecutive_below_threshold >= 10:
        # Nulling completed after consecutive iterations below threshold
        break
      nulling_attempts += 1
    if nulling_attempts == nr:
      # Nulling failed after maximum attempts
      self.cdc.logger.error(
          f'Nulling failed with parameters: kp={kp}, ki={ki}, nr={nr}, residual={error}')
      raise errors.NullFailed
    else:
      # Nulling succeeded
      self.cdc.logger.info('Nulling succeeded')
      return int(output)

  def _validate_state(self):
    """ This function validates that the CDC is in the correct state to calibrate the DAC. It raises a BadState error if the CDC is not in the correct state.

    Raises:
      errors.BadState: If the CDC is not in the ACTIVE state, if the VB is connected, or if the I_REF is not set to 0A    

    """
    # check that the CDC is in the correct state
    if self.cdc.state != self.cdc.CDCStates.ACTIVE:
      self.cdc.logger.error(
          f'Cannot calibrate DAC1.TW.CAL while CDC is in state {self.cdc.state}')
      raise errors.BadState
    # check that there is no VB
    if self.cdc.primrel.get_vb_status() != self.cdc.primrel.VBStatus.VB_DISCONNECTED:
      self.cdc.logger.error(
          'Cannot calibrate DAC1.TW.CAL while VB is in use')
      raise errors.BadState

    if self.cdc.properties['CALSYS.CDC.I_REF'].value[0] != 0:
      self.cdc.logger.error(
          'Cannot calibrate DAC1.TW.CAL while I_REF is not zero')
      raise errors.BadState

  def set_and_calibrate(self, _):
    """This function is called when the property is set. It triggers the calibration of the DAC 1TW system.

    Args:
      _: This is the value that is sent to the property. It is not used in this function.

    Raises:
      errors.NullFailed: Raised if the nulling fails after 1000 attempts
    """
    self._validate_state()
    # Store the current calibration state
    old_zero = self.cdc.properties['CALSYS.CDC.DAC1TW.ZERO'].value[0]
    old_full_scale = self.cdc.properties['CALSYS.CDC.DAC1TW.FS'].value[0]

    try:
      # reset the calibration
      self.cdc.primrel.set_1t_dac_zero(0)
      self.cdc.primrel.set_1t_dac_fs(0x8000)

      # set the 1T DAC to midrange
      self.cdc.primrel.set_dac(0)
      self.cdc.primrel.set_primary_relays(0)

      # set the 1T DAC POL relay to 1
      self.cdc.primrel.set_1t_dac_negative(False)
      self.cdc.primrel.set_negative(True)

      self.cdc.primrel.set_1t_dac_relay(True)

      zero = self._nulling()

      # set the zero in the CALSYS.CDC.DAC1TW.ZERO property, using the ._set() method to trigger persistent memory write
      # pylint: disable=protected-access
      self.cdc.properties['CALSYS.CDC.DAC1TW.ZERO']._set([zero])

      # now we do the same, but we turn on the 1T relay first

      self.cdc.primrel.set_dac(0x7000)
      self.cdc.primrel.set_primary_relays(1)

      # now null in this new configuration, to get the full scale value
      full_scale = self._nulling(zero)

      # reset everything to the initial state
      self.cdc.primrel.set_dac(0)
      self.cdc.primrel.set_primary_relays(0)
      self.cdc.primrel.set_negative(False)
      self.cdc.primrel.set_1t_dac_relay(False)
      self.cdc.primrel.set_1t_dac_negative(False)

      # set the full scale in the CALSYS.CDC.DAC1TW.FS property
      # pylint: disable=protected-access
      self.cdc.properties['CALSYS.CDC.DAC1TW.FS']._set(
          [full_scale])

      # apply the calibration
      self.cdc.primrel.set_1t_dac_zero(zero)
      self.cdc.primrel.set_1t_dac_fs(full_scale)

      # we should be reading
      self.value[0] = True
    except errors.Error as exc:
      # if something went wrong, restore the old calibration
      self.cdc.logger.error(
          f'Calibration failed: {exc}, restoring old calibration')
      self.cdc.primrel.set_1t_dac_zero(old_zero)
      self.cdc.primrel.set_1t_dac_fs(old_full_scale)
      self.cdc.primrel.set_dac(0)
      self.cdc.primrel.set_primary_relays(0)
      raise exc

  def _set_dac(self, control_signal: int):
    """This function sets the DAC to the specified value. It saturates the output at 16 bits.

    Args:
      control_signal (int): The value to set the DAC to (between -0x7FFF and 0x7FFF)

    """

    # the dac is in the fadem, and has to be set as follows:

    # saturate the output at 15 bits
    if control_signal > 0x7FFF:
      control_signal = 0x7FFF
    elif control_signal < -0x8000:
      control_signal = -0x8000

    self.cdc.fadem.set_dac(int(control_signal))

  def is_calibrated(self):
    """This function returns the state of the DAC calibration. If the DAC has been calibrated, it returns CALIBRATED. If the DAC has not been calibrated, it returns UNCALIBRATED.    
    """
    if self.value[0]:
      return "CALIBRATED"
    else:
      return "UNCALIBRATED"

  def set(self, command):
    """This function is called when the property is set. It triggers the calibration of the DAC 1TW system"""
    if command.payload == ['']:
      command.payload = [True]
    else:
      raise errors.BadParameter
    super().set(command)
