"""Module for the CalsysCdcDacVolt property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors


class CalsysCdcDacVolt(fgc_property.Property):
  """This property returns the voltage corresponding to the preset output of the specified DAC channel. It has two channels: FADEM dac and PRIMREL dac. The first is used to do the nulling in the CDC, the second sets the current in the 1 turn winding. Both DACs are 16 bit signed integers (0V = 0x8000)

  Set:
    This property cannot be set.

  Get:
    When this property is read, it returns the voltage corresponding to the preset output of the specified DAC channel, in Volts. Dac output is between -2.5V and 2.5V for both channels.

  NCRP Errors:
    - **11 set not perm**: this property cannot be set  
    - **20 bad array idx**: the array specifies a channel that does not exist, valid channels are 0 and 1
    - **21 bad array len**: the array is not the correct length ([idx], [start,], [start, stop], [start, stop, step] are all valid) 

  """

  def __init__(self, cdc):
    self.cdc = cdc
    # create getters and setters
    getters = [self.get_ch0_volt, self.get_ch1_volt]
    super().__init__(
        name='CALSYS.CDC.DAC.VOLT',
        access='r',
        length=2,
        getters=getters,
        val_type=float,
        unit='V',
        description='This property returns the voltage corresponding to the preset output of the specified DAC channel.'
    )

  def get_ch0_volt(self):
    """Returns the volts being output from the fadem DAC (channel 0)"""
    # this provides a 16 bit number that has to be rescaled to +-2.5V
    voltage = self.cdc.fadem.get_dac() * 5.0 / 65536.0 - 2.5
    return voltage

  def get_ch1_volt(self):
    """Returns the volts being output from the primrel DAC (channel 1)"""
    voltage = self.cdc.primrel.get_dac() * 5.0 / 65536.0 - 2.5
    return voltage
