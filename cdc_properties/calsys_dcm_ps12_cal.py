"""Module for the CalsysDcmPs12Cal property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysDcmPs12Cal(fgc_property.Property):
  """This property contains the calibration gain of the 12V power supply. Setting it with the measured voltage on the power supply rail, triggers a calibration. Getting the value returns the calibration status

  Set:
    Sets the calibration gain of the power supply rail. The value is the measured voltage of the power supply rail. Setting the property to 0 will cancel the calibration.

  Get:
    Gets the calibration gain of the 12V power supply. CALIBRATED if the gain is not 0, UNCALIBRATED otherwise.

  NCRP Errors:
    - **19 bad float**: The value is not a float when setting the property.

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.DCM.PS12.CAL',
        access='rwp',
        setters=[self.calibrate_voltage],
        getters=[self.is_calibrated],
        val_type=float,
        description='This property contains the calibration gain of the 12V power supply. Setting it triggers a calibration, getting the value returns the calibration status'
    )

  def calibrate_voltage(self, value):
    """This method calibrates the 12V power supply. It takes the value as the target voltage and computes the gain to apply to the raw value to get the target voltage"""
    # first we get the voltage from the system monitor
    current_voltage = self.cdc.sysmon.read_raw_voltage('12V')
    # then we compute the gain. This gain should be the multiplicative factor to multiply the raw to get the value
    gain = value / current_voltage
    # store the gain in the property
    self.value[0] = gain

  def is_calibrated(self):
    """we return CALIBRATED if the gain is not 0, UNCALIBRATED otherwise"""
    if self.value[0] != 0:
      return "CALIBRATED"
    else:
      return "UNCALIBRATED"
