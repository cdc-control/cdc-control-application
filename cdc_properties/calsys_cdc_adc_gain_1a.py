"""Module for the CalsysCdcAdcGain1A property of the CDC."""


from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors


class CalsysCdcAdcGain1A(fgc_property.Property):
  """This class implements the CALSYS.CDC.ADC.GAIN_1A property of the CDC. It represents the gain of the ADC channel for a I_REF of 1A. Setting this expert property with no parameter and the CDC output set to 1A launches a gain calibration for the CDC output current readout. It stores the gain persistently. CALSYS.CDC.I_REF must be set to 1A before calibrating the gain.

  Set:
    When this property is set, it is set without a value. If it is without a value, we assume that the user wants to calibrate the ADC. If a value is provided, we raise a BadParameter error.

  Get:
    When this property is read, it returns CALIBRATED if the gain has been calibrated, or UNCALIBRATED if it has not been calibrated.

  NCRP Errors:
    - **34 bad state**: the CDC must be in the ACTIVE state to calibrate the gain
    - **34 bad state**: CALSYS.CDC.I_REF must be set to 1A to calibrate the gain
    - **23 bad parameter**: if the command has a payload that is not an empty string

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.ADC.GAIN_1A',
        access='rwp',
        val_type=float,
        length=1,
        getters=[self.is_calibrated],
        setters=[self.calibrate_gain],
        unit='V/A',
        description='Setting this expert property with no parameter and the CDC output set to 1A launches a gain calibration for the CDC output current readout. When read, it retrieves the value of the calibrated gain.',
        min_val=0,
        max_val=1.6,
        default=0
    )

  def calibrate_gain(self, _):
    """
    This function calibrates the gain of the ADC channel, which is the CDC output current. It sets the gain to 1/current_readout, where current_readout is the current read from the nullmeter when the CDC output is set to 1A.

    Args:
      _: This is the value that is sent to the property. It is not used in this function.

    Returns:
      None
    """

    self._validate_state()

    # read from the nullmeter and set the gain
    current_readout = self.cdc.nullmeter.get_I_monitor()

    # we know that whatever value current_readout is, this represents 1A
    # so we can set the gain to 1/current_readout

    self.value[0] = 1/current_readout
    return

  def _validate_state(self):
    """
    This function validates that the CDC is in the correct state to calibrate the gain. It raises a BadState error if the CDC is not in the correct state.

    Args:

    Raises:
      BadState: If CALSYS.CDC.I_REF is not set to 1A, or if the CDC is not in the ACTIVE state.
    """
    # check that we are in the right state
    if self.cdc.state != self.cdc.CDCStates.ACTIVE:
      self.cdc.logger.error(
          f'Cannot calibrate ADC.GAIN_1A while CDC is in state {self.cdc.state}, must be ACTIVE')
      raise errors.BadState
    # check that I_ref is set to 1A
    if self.cdc.properties['CALSYS.CDC.I_REF'].value[0] != 1.0:
      self.cdc.logger.error(
          f'Cannot calibrate ADC.GAIN_1A while I_REF is not 1A, must be 1A, currently {self.cdc.properties["CALSYS.CDC.I_REF"].value[0]}')
      raise errors.BadState

  def is_calibrated(self):
    """
    This function returns the state of the gain calibration. If the gain has been calibrated, it returns CALIBRATED. If the gain has not been calibrated, it returns UNCALIBRATED.

    Returns:
      str: CALIBRATED or UNCALIBRATED
    """

    if self.value[0] != 0:
      return "CALIBRATED"
    else:
      return "UNCALIBRATED"

  def set(self, command):
    """When this property is set, it is set without a value. If it is without a value, we assume that the user wants to calibrate the ADC.

    Args:
      command (fgc_ncrp_parser.fgc_ncrp.command.Command): The command that is being sent to the property.

    Raises:
      BadParameter: If the command has a payload that is not an empty string.
    """
    if command.payload == ['']:
      command.payload = [True]
    else:
      raise errors.BadParameter

    super().set(command)
