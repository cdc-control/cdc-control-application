"""Module for the CalsysDcmPs12Meas property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysDcmPs12Meas(fgc_property.Property):
  """This property returns the value of the 12V power supply in Volt.

  Set:
    Set not permitted.

  Get:
    Gets the value of the 12V power supply in Volt.

  NCRP Errors:
    - **11 set not perm**: The property is read only.

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.DCM.PS12.MEAS',
        access='r',
        val_type=float,
        getters=[lambda: self.cdc.sysmon.read_calibrated_voltage('12V')],
        unit='V',
        description='Accessing this property triggers a measurement of the 12V power supply and returns the value in Volt.'
    )
