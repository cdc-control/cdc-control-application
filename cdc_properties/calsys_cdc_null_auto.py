"""Module for the CalsysCdcNullAuto property of the CDC."""

import time

from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors


class CalsysCdcNullAuto(fgc_property.Property):
  """This property triggers the automatic nulling of the current calibrator.

  This property is only available when the CDC is in the ACTIVE state. Setting this property will trigger a CDC offset nulling action. The CDC offset nulling can only be done with the CDC on at zero current and when not in VB mode. 

  Set:
    Setting this property will trigger a CDC offset nulling action. The CDC offset nulling can only be done with the CDC on at zero current and when not in VB mode. This operation may take some seconds

  Get:
    Not permitted

  NCRP Errors:
    - **34 bad state**: The CDC is not in the ACTIVE state.
    - **34 bad state**: The CDC is in the OFF range (set CALSYS.CDC.OUTPUT to a valid range first)
    - **34 bad state**: The CDC is in the VB mode (disconnect VB first)
    - **34 bad state**: The I ref of the CDC is not zero (set CALSYS.CDC.I_REF to zero first)
    - **79 null failed**: The nulling failed. This can happen if the nulling parameters are not appropriate (such as CALSYS.CDC.NULL.KP, CALSYS.CDC.NULL.KI, or if the output is open)

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.NULL.AUTO',
        access='w',
        val_type=bool,
        setters=[self.set_null],
        description='Setting this property will trigger a CDC offset nulling action. The CDC offset nulling can only be done with the CDC on at zero current and when not in VB mode. This operation may TAKE up to 5s. GET not permitted'
    )
    self.ranges_map = {
        'RANGE_1A': 1.0,
        'RANGE_2A5': 2.5,
        'RANGE_5A': 5.0,
        'RANGE_10A': 10.0
    }

  def set_null(self, _):
    """This function triggers the automatic nulling of the current calibrator.

    Raises:
      errors.BadState: if the CDC is not in the correct state

    """
    current_range = self.cdc.properties['CALSYS.CDC.OUTPUT'].value[0]
    # check that the CDC is in the correct state
    self._validate_state(current_range)
    try:
      self.nulling(current_range)
    except errors.NullFailed as exc:
      self.cdc.logger.error('Nulling failed')
      self.cdc.state = self.cdc.CDCStates.ACTIVE
      raise exc
    self.cdc.state = self.cdc.CDCStates.ACTIVE

  def _validate_state(self, current_range):
    """This function validates that the CDC is in the correct state to null.

    Args:
      current_range: the current range of the CDC

    Raises:
      errors.BadState: if the CDC is not in the correct state
    """
    # check that the CDC is in the correct state
    if self.cdc.state != self.cdc.CDCStates.ACTIVE:
      self.cdc.logger.error(
          f'Cannot null while CDC is in state {self.cdc.state}, must be in ACTIVE state')
      raise errors.BadState
    # check that the CDC is not in the OFF range
    if current_range == 'OFF':
      self.cdc.logger.error(
          'Cannot null while CDC is in OFF range')
      raise errors.BadState
    # check that there is no VB
    if self.cdc.primrel.get_vb_status() != self.cdc.primrel.VBStatus.VB_DISCONNECTED:
      self.cdc.logger.error(
          'Cannot null while VB is in use')
      raise errors.BadState
    # check that I_REF is zero
    if self.cdc.properties['CALSYS.CDC.I_REF'].value[0] != 0:
      self.cdc.logger.error(
          'Cannot null while I_REF is not zero')
      raise errors.BadState

  def nulling(self, range_value):
    """This function performs the nulling of the current calibrator.

    Args:
      range_value: the current range of the CDC

    Raises:
      errors.NullFailed: if the nulling fails

    """
    self.cdc.state = self.cdc.CDCStates.NULLING
    self.cdc.logger.debug('Nulling started')
    # Get PID constants
    # pylint: disable=protected-access
    # pylint: disable=invalid-name
    kp = self.cdc.properties['CALSYS.CDC.NULL.KP'].value[0]
    ki = self.cdc.properties['CALSYS.CDC.NULL.KI'].value[0]
    nr = self.cdc.properties['CALSYS.CDC.NULL.NR'].value[0]

    self.cdc.logger.debug(
        f'nulling parameters: kp={kp}, ki={ki}, nr={nr}')

    nulling_attempts = 0
    # Threshold for nulling completion
    nulling_threshold = 0.05

    consecutive_below_threshold = 0

    sample_time = 0.01  # 10 ms

    integral = 0
    alpha = 0.2  # Smoothing factor for the low-pass filter
    filtered_I_null = 0  # Initial filtered value of I_null

    if self.cdc.primrel.get_pbc_x5_status():
      # if PBCx5 is connected
      # internally we are in the 1A range, but the user sees the 5A range
      relative_gain = self.ranges_map['RANGE_1A']
    else:
      relative_gain = self.ranges_map[range_value]

    while nulling_attempts < nr:
      # Get the current I_null
      raw_I_null = self.cdc.nullmeter.get_I_null(
      ) / relative_gain
      # Apply low-pass filter
      filtered_I_null = alpha * raw_I_null + (1 - alpha) * filtered_I_null
      # Calculate the error
      error = filtered_I_null
      # Calculate the integral
      integral += error * sample_time * ki
      # saturate the integral, to avoid windup
      clamp_min = -2 ** 15  # Lower bound for clamping
      # Upper bound for clamping (2 ** 15 - 1 to include the maximum value)
      clamp_max = 2 ** 15 - 1

      if integral < clamp_min:
        integral = clamp_min
      elif integral > clamp_max:
        integral = clamp_max

      proportional = error * kp
      # Calculate the output
      output = proportional + integral
      # Saturate the output, if the output is saturated turn on the LED
      if output > clamp_max:
        output = clamp_max
        self.cdc.set_null_saturation_led(1)
      elif output < clamp_min:
        output = clamp_min
        self.cdc.set_null_saturation_led(1)
      else:
        self.cdc.set_null_saturation_led(0)

      # Set the output
      self._set_dac(int(output))
      # Print the output, making sure that all numbers keep the same width for easier reading
      self.cdc.logger.debug(
          f'{filtered_I_null: 01.5f} {error: 01.3f} {proportional: 05.3f} { integral: 05.3f} {output: 05.3f}')

      time.sleep(sample_time)

      if abs(error) < nulling_threshold:
        consecutive_below_threshold += 1
      else:
        consecutive_below_threshold = 0

      if consecutive_below_threshold >= 10:
        # Nulling completed after consecutive iterations below threshold
        break

      nulling_attempts += 1

    if nulling_attempts == nr:
      # Nulling failed after maximum attempts
      self.cdc.logger.error(
          f'Nulling failed with parameters: kp={kp}, ki={ki}, nr={nr}, residual={error}')
      # reset the relays and range
      raise errors.NullFailed
    else:
      # Nulling successful
      self.cdc.logger.info(
          f'Nulling successful after {nulling_attempts} attempts')

  def _set_dac(self, control_signal: int):
    """
    This function sets the control signal of the CDC.

    Args:
      control_signal: the control signal to set. This is a 16 bit signed integer. If the value is outside of the allowed range, it is saturated.

    """

    # saturate the output at 15 bits
    if control_signal > 0x7FFF:
      control_signal = 0x7FFF
    elif control_signal < -0x8000:
      control_signal = -0x8000

    self.cdc.fadem.set_dac(int(control_signal))
