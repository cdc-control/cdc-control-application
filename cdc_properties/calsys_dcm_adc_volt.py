"""Module for the CalsysDcmAdcVolt property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysDcmAdcVolt(fgc_property.Property):
  """This property returns the value of the ADC in Volt. This is the voltage read at the input of the ADC, so it is in a range of 0-4.096V. The ADC is 16 bits, so the resolution is 4.096/2^16 = 62.5uV.

  The ADC is used in a signed configuration, so a value of 0x8000 corresponds to 2.048V.

  Accessing this property triggers an ADC reading, for the specified channel and returns the value in Volt.

  The channels are mapped as follows:
  - **0: 10V_cal**, external 10V calibration. Not used. Maps to ADC channel 0.
  - **1: I_null**, nulling current. Maps to ADC channel 1.
  - **2: V_compliance**, compliance voltage of the CDC. Maps to ADC channel 2.
  - **3: I_monitor**, self monitoring current. Maps to ADC channel 3.
  - **4: PBC_temp**, temperature of the PBC module. Maps to ADC channel 4.
  - **5: zero**, count corresponding to the zero of the ADC. Maps to ADC channel 7.

  Set:
    Set not permitted.

  Get:
    Gets the value of the ADC channels in volts.

  NCRP Errors:
    - **11 set not perm**: The property is read only.
    - **20 bad array idx**: the array specifies a channel that does not exist, valid channels are 0 to 5
    - **21 bad array len**: the array is not the correct length ([idx], [start,], [start, stop], [start, stop, step] are all valid)

  """

  def __init__(self, cdc):
    self.cdc = cdc
    getters = [
        lambda: self.cdc.nullmeter.get_voltage('10V_cal'),
        lambda: self.cdc.nullmeter.get_voltage('I_null'),
        lambda: self.cdc.nullmeter.get_voltage('V_compliance'),
        lambda: self.cdc.nullmeter.get_voltage('I_monitor'),
        lambda: self.cdc.nullmeter.get_voltage('PBC_temp'),
        lambda: self.cdc.nullmeter.get_voltage('zero')
    ]
    super().__init__(
        name='CALSYS.DCM.ADC.VOLT',
        access='r',
        length=6,
        getters=getters,
        val_type=float,
        unit='V',
        description='Accessing this property triggers an ADC reading, for the specified channel and returns the value in Volt.'
    )
