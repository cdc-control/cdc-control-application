"""Module for the CalsysDcmAdcRaw property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysDcmAdcRaw(fgc_property.Property):
  """This property returns the raw value of the ADC channels.
  The channels are mapped as follows:
  - 0: 10V_cal, external 10V calibration. Not used. Maps to ADC channel 0.
  - 1: I_null, nulling current. Maps to ADC channel 1.
  - 2: V_compliance, compliance voltage of the CDC. Maps to ADC channel 2.
  - 3: I_monitor, self monitoring current. Maps to ADC channel 3.
  - 4: PBC_temp, temperature of the PBC module. Maps to ADC channel 4.
  - 5: zero, count corresponding to the zero of the ADC. Maps to ADC channel 7.

  Set:
    Set not permitted.

  Get:
    Gets the raw value of the ADC channels. 

  NCRP Errors:
    - **11 set not perm**: The property is read only.
    - **20 bad array idx**: the array specifies a channel that does not exist, valid channels are 0 to 5
    - **21 bad array len**: the array is not the correct length ([idx], [start,], [start, stop], [start, stop, step] are all valid)

  """

  def __init__(self, cdc):
    self.cdc = cdc
    channels = self.cdc.nullmeter.channels
    getters = [
        lambda: self.cdc.nullmeter.read_raw_channel(channels['10V_cal']),
        lambda: self.cdc.nullmeter.read_raw_channel(channels['I_null']),
        lambda: self.cdc.nullmeter.read_raw_channel(channels['V_compliance']),
        lambda: self.cdc.nullmeter.read_raw_channel(channels['I_monitor']),
        lambda: self.cdc.nullmeter.read_raw_channel(channels['PBC_temp']),
        lambda: self.cdc.nullmeter.read_raw_channel(channels['zero'])
    ]
    super().__init__(
        name='CALSYS.DCM.ADC.RAW',
        access='r',
        val_type=int,
        length=6,
        getters=getters,
        unit='ADC counts',
        description='Accessing this property triggers an ADC reading for the specified channel and returns the value in raw reading.'
    )
