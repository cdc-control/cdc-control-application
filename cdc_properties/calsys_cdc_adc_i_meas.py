"""Module for the CalsysCdcAdcIMeas property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysCdcAdcIMeas(fgc_property.Property):
  """This property returns the value of the output current measured by the current calibrator.

  Set:
    This property cannot be set.

  Get:
    When this property is read, it returns the value of the output current measured by the current calibrator in Amp.

  NCRP errors:
    - **11 set not perm**: this property cannot be set    
  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.ADC.I_MEAS',
        access='r',
        val_type=float,
        getters=[self.get_calibrated_i_monitor],
        unit='A',
        description='Accessing this property triggers a reading of the ADC channel which measures the output current of the CDC and returns a value in Amp. SET not permitted'
    )

  def get_calibrated_i_monitor(self):
    """this is read from the nullmeter. If CALSYS.CDC.ADC.GAIN_1A is not calibrated (value=0), we assume gain = 1

    Returns:
      float: the calibrated current measured by the current calibrator in Amp
    """

    calibration = self.cdc.properties['CALSYS.CDC.ADC.GAIN_1A'].value[0]
    if calibration == 0:
      return self.cdc.nullmeter.get_I_monitor()
    else:
      return self.cdc.nullmeter.get_I_monitor() * calibration
