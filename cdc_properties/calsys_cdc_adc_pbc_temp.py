"""Module for the CalsysCdcAdcPbcTemp property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysCdcAdcPbcTemp(fgc_property.Property):
  """This property returns the value of the temperature of the PBC in Celsius.

  Set:
    This property cannot be set.  

  Get:
    When this property is read, it returns the value of the temperature of the PBC in Celsius.

  NCRP Errors:
    - **11 set not perm**: this property cannot be set

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.ADC.PBC_TEMP',
        access='r',
        val_type=float,
        getters=[self.cdc.nullmeter.get_PBC_temp],
        unit='C',
        description='Accessing this property triggers a reading of the ADC channel which measures the temperature of the PBC and returns a value in Celsius. SET not permitted'
    )
