"""Module for the CalsysDcmPs05Meas property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class CalsysDcmPs05Meas(fgc_property.Property):
  """This property returns the value of the 5V power supply in Volt.

  Set:
    Set not permitted.

  Get:
    Gets the value of the 5V power supply in Volt.

  NCRP Errors:
    - **11 set not perm**: The property is read only.

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.DCM.PS05.MEAS',
        access='r',
        getters=[lambda: self.cdc.sysmon.read_calibrated_voltage('5V')],
        val_type=float,
        unit='V',
        description='Accessing this property triggers a measurement of the 5V power supply and returns the value in Volt.'
    )
