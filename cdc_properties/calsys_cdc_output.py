"""Module for the CalsysCdcOutput property of the CDC."""

import time

from fgc_ncrp_parser.fgc_ncrp import fgc_property
from fgc_ncrp_parser.fgc_ncrp import errors
import drivers.primrel as primrel_module


class CalsysCdcOutput(fgc_property.Property):
  """This property gets and sets the range the current calibrator is in. It accepts and returns a string as follows: OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A

  This property is only available when the CDC is in the IDLE or ACTIVE state. The value of this property is the range the current calibrator is in. The value is a string.

  Set:
    Sets the range of the current calibrator by manipulating the secondary relays. The value has to be one of the following: OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A

  Get:
    Gets the range of the current calibrator.

  NCRP Errors:
    - **23 bad parameter**: The range is not valid. Valid ranges are: OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A
    - **34 bad state**: The CDC is not in the IDLE or ACTIVE state.
    - **34 bad state**: The CDC is in a range different than OFF, and the new range is not OFF.
    - **34 bad state**: The CDC I ref is not zero (set CALSYS.CDC.I_REF to zero first)
    - **34 bad state**: The VB and the PBCx5 are connected together, this is not supported
    - **34 bad state**: The VB is connected and the new range is not RANGE_5A or OFF
    - **34 bad state**: The PBCx5 is connected and the new range is not RANGE_5A or OFF
    - **79 null failed**: The nulling failed. This can happen if the nulling parameters are not appropriate (such as CALSYS.CDC.NULL.KP, CALSYS.CDC.NULL.KI, or if the output is open)
  """

  def __init__(self, cdc):
    self.cdc = cdc
    self.ranges_map = {
        'OFF': {'fadem': self.cdc.fadem.Ranges.OFF,
                'primrel': primrel_module.SecondaryRelays.OFF},
        'RANGE_1A': {'fadem': self.cdc.fadem.Ranges.RANGE_1A,
                     'primrel': primrel_module.SecondaryRelays.REL_SEC_40T},
        'RANGE_2A5': {'fadem': self.cdc.fadem.Ranges.RANGE_2A5,
                      'primrel': primrel_module.SecondaryRelays.REL_SEC_16T},
        'RANGE_5A': {'fadem': self.cdc.fadem.Ranges.RANGE_5A,
                     'primrel': primrel_module.SecondaryRelays.REL_SEC_8T},
        'RANGE_10A': {'fadem': self.cdc.fadem.Ranges.RANGE_10A,
                      'primrel': primrel_module.SecondaryRelays.REL_SEC_4T}
    }
    super().__init__(
        name='CALSYS.CDC.OUTPUT',
        access='rw',
        val_type=str,
        setters=[self.set_output],
        description='This property gets and sets the range the current calibrator is in.',
        default='OFF'
    )

  def set_output(self, value):
    """This function sets the range of the current calibrator.

    Args:
      value: the range to set. Valid ranges are: OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A

    Raises:
      errors.BadParameter: if the range is not valid
      errors.BadState: if the CDC is not in the correct state
      errors.NullFailed: if the nulling failed
    """

    self._validate_value(value)
    self._set_value(value)

  def _validate_value(self, value):
    """This function validates that the range is valid and that the CDC is in the correct state to change the range.

    Args:
      value: the range to set. Valid ranges are: OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A

    Raises:
      errors.BadParameter: if the range is not valid
      errors.BadState: if the CDC is not in the correct state
    """

    # check that the range exists
    if value not in self.ranges_map:
      self.cdc.logger.error(f'Invalid range {value}')
      raise errors.BadParameter
    # check that the CDC is in IDLE or ACTIVE state, to change the range
    if self.cdc.state not in [self.cdc.CDCStates.IDLE, self.cdc.CDCStates.ACTIVE]:
      self.cdc.logger.error(
          f'Cannot change range while in state {self.cdc.state}, must be IDLE or ACTIVE')
      raise errors.BadState
    # from all ranges different than OFF, can only go to OFF
    if self.value[0] != 'OFF' and value != 'OFF':
      self.cdc.logger.error(
          f'Cannot change range while in range {self.value[0]}, must be OFF first')
      raise errors.BadState
    # check that there is no current being generated, before changing the range
    if self.cdc.properties['CALSYS.CDC.I_REF'].value[0] != 0.0:
      self.cdc.logger.error('Cannot change range while generating current')
      raise errors.BadState

    # if both VB and PBCx5 are connected, return BadState
    if self.cdc.primrel.get_vb_status() != self.cdc.primrel.VBStatus.VB_DISCONNECTED and self.cdc.primrel.get_pbc_x5_status() and value != 'OFF':
      self.cdc.logger.error('VB and PBCx5 cannot be connected together')
      raise errors.BadState

    # if the VB is connected, only RANGE_5A is allowed
    if self.cdc.primrel.get_vb_status() != self.cdc.primrel.VBStatus.VB_DISCONNECTED and value not in ['RANGE_5A', 'OFF']:
      self.cdc.logger.error('VB connected, only RANGE_5A is allowed')
      raise errors.BadState

    # check if PBCx5 is connected. If so, only one range is allowed
    pbc_x5_connected = self.cdc.primrel.get_pbc_x5_status()
    if pbc_x5_connected and value not in ['RANGE_5A', 'OFF']:
      self.cdc.logger.error('PBCx5 connected, only RANGE_5A is allowed')
      raise errors.BadParameter

  def _set_value(self, value):
    """This function sets the range of the current calibrator.

    Args:
      value: the range to set. Valid ranges are: OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A

    Raises:
      errors.NullFailed: if the nulling failed
    """

    if value == 'OFF':
      # if vb is in use, we clamp it
      if self.cdc.primrel.get_vb_status() != self.cdc.primrel.VBStatus.VB_DISCONNECTED:
        self.cdc.primrel.set_vb_clamp(True)
      # set relays to off
      self._set_range(value)
      # go to idle and return
      self.cdc.state = self.cdc.CDCStates.IDLE
      self.cdc.primrel.set_active(False)
      self.value[0] = value
      return

    # check that no alarms are active
    # TODO check alarms
    # activate the primrel board
    self.cdc.primrel.set_active(True)

    # target range is the value given, except if the PBCx5 is connected, in which case it's RANGE_1A instead of RANGE_5A
    target_range = value
    if self.cdc.primrel.get_pbc_x5_status() and value == 'RANGE_5A':
      target_range = 'RANGE_1A'
    self._set_range(target_range)

    # remove the clamps
    self.cdc.fadem.set_clamp1(False)
    self.cdc.fadem.set_clamp2(False)

    self.cdc.logger.debug(f'VB status: {self.cdc.primrel.get_vb_status()}')
    # check if VB is connected. If so, we don't do the nulling
    if self.cdc.primrel.get_vb_status() != self.cdc.primrel.VBStatus.VB_DISCONNECTED:
      self.cdc.logger.debug('VB connected, skipping nulling')
      # here we also unclamp the VB
      self.cdc.primrel.set_vb_clamp(False)
    else:
      self.cdc.logger.debug('VB disconnected, starting nulling')
      try:
        self.cdc.properties['CALSYS.CDC.NULL.AUTO'].nulling(value)
      except errors.NullFailed:
        self.cdc.state = self.cdc.CDCStates.IDLE
        self.cdc.logger.error('Nulling failed')
        self._set_range('OFF')
        raise

    self.cdc.state = self.cdc.CDCStates.ACTIVE
    self.value[0] = value
    self.cdc.logger.debug('OUTPUT set')

  def _set_range(self, value):
    """This function sets the range of the current calibrator in the fadem and the primrel board.

    Args:
      value: the range to set. Valid ranges are: OFF, RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A
    """

    # clamp the fadem
    self.cdc.fadem.set_clamp1(True)
    self.cdc.fadem.set_clamp2(True)
    # wait for the fadem to settle
    time.sleep(0.1)

    self.cdc.fadem.set_range(self.ranges_map[value]['fadem'])
    self.cdc.primrel.set_range(value)
    # wait for the fadem to settle
    time.sleep(0.1)
