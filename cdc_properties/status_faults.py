"""Module for the StatusFaults property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class StatusFaults(fgc_property.Property):
  """This property indicates the current faults of the device. 

  Set:
    Set not permitted.

  Get:
    Gets the current faults of the device. The value is a set of strings, each string representing a fault. The possible faults are:
    - **PBC_TMP**: The temperature of the PBC is off limits.
    - **PBC_CAL**: The calibration of the PBC is not valid.
    - **PBC_CMPL**: The PBC compliance voltage is off limits.
    - **PBC_CHR**: The PBC is not charging.
    - **VB_PWR**: The voltage booster is off but connected.
    - **VB_TMP**: The temperature of the voltage booster is off limits.
    - **PA_TMP**: The temperature of the power amplifier is off limits.
    - **MOD_TMP**: The temperature of the modulator is off limits.

  NCRP Errors:
    - **11 set not perm**: The property is read only.

  """
  faults = [
      'PBC_TMP',
      'PBC_CAL',
      'PBC_CMPL',
      'PBC_CHR',
      'VB_PWR',
      'VB_TMP',
      'PA_TMP',
      'MOD_TMP'
  ]

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='STATUS.FAULTS',
        access='r',
        getters=[lambda: ' '.join(self.value[0])],
        val_type=set,
        description='This property indicates the current faults of the device'
    )

  def set_fault(self, value):
    """Method to set the faults. 

    Args:
      value (str): The fault to set. Must be one of the following: 'PBC_TMP', 'PBC_CAL', 'PBC_CMPL', 'PBC_CHR', 'VB_PWR', 'VB_TMP', 'PA_TMP', 'MOD_TMP'.

    Raises:
      ValueError: If the argument is not a valid fault.

    """
    if value not in self.faults:
      raise ValueError('Argument must be a valid fault')
    self.value[0].add(value)

  def clear_fault(self, value):
    """Method to clear the faults.

    Args:
      value (str): The fault to clear. Must be one of the following: 'PBC_TMP', 'PBC_CAL', 'PBC_CMPL', 'PBC_CHR', 'VB_PWR', 'VB_TMP', 'PA_TMP', 'MOD_TMP'.

    Raises:
      ValueError: If the argument is not a valid fault.

    """

    if value not in self.faults:
      raise ValueError('Argument must be a valid fault')
    self.value[0].discard(value)
