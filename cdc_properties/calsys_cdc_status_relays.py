"""Module for the CalsysCdcStatusRelays property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property
import drivers.primrel as primrel_module


class CalsysCdcStatusRelays(fgc_property.Property):
  """This property indicates the status of the relays of the CDC.

  This property is only available when the CDC is in the IDLE or ACTIVE state. The value of this property is a string containing the status of the relays of the CDC. The string contains the following information:
  - The status of the polarity relays (INV2048T, INV1024T, INV512T, INV256T, INV128T, INV64T, INV32T, INV16T, INV8T, INV4T, INV2T, INV1T, INV1TCAL, INV1TDAC)
  - The status of the ON/OFF relays (ON2048T, ON1024T, ON512T, ON256T, ON128T, ON64T, ON32T, ON16T, ON8T, ON4T, ON2T, ON1T, ON1TCAL, ON1TDAC)
  - The status of the secondary relays (ONS4T, ONS8T, ONS16T, ONS40T)

  Set:
    Set not permitted.

  Get:
    Gets the status of the relays of the CDC.

  NCRP Errors:
    - **11 set not perm**: The property is read only.

  """

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='CALSYS.CDC.STATUS.RELAYS',
        access='r',
        val_type=int,
        length=1,
        unit='',
        getters=[self.make_relay_string],
        description='This property indicates the status of the relays of the CDC.'
    )

  def make_relay_string(self):
    """This function returns a string containing the status of the relays of the CDC.

    Args:
      None

    Returns:
      A string containing the status of the relays of the CDC.

    """

    # read relays from the primary relay board
    pol_relays = self.cdc.primrel.get_pol_relays().get_active_relay_names()
    onoff_relays = self.cdc.primrel.get_on_off_relays().get_active_relay_names()
    active_secondary_relay = self.cdc.primrel.get_secondary_relays()
    # active_secondary_relay will contain one of the following: SecondaryRelays.REL_SEC_4T, SecondaryRelays.REL_SEC_8T, SecondaryRelays.REL_SEC_16T, SecondaryRelays.REL_SEC_40T or OFF
    # depending on which relay is active, we will add the corresponding relays to the string: ON4T, ON8T, ON16T, ON40T or nothing

    if active_secondary_relay == primrel_module.SecondaryRelays.REL_SEC_4T:
      secondary_relays = 'ONS4T'
    elif active_secondary_relay == primrel_module.SecondaryRelays.REL_SEC_8T:
      secondary_relays = 'ONS8T'
    elif active_secondary_relay == primrel_module.SecondaryRelays.REL_SEC_16T:
      secondary_relays = 'ONS16T'
    elif active_secondary_relay == primrel_module.SecondaryRelays.REL_SEC_40T:
      secondary_relays = 'ONS40T'
    else:
      secondary_relays = ''
    # return the string with the relays

    return pol_relays + ' ' + onoff_relays + ' ' + secondary_relays
