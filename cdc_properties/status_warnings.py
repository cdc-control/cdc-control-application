"""Module for the StatusWarnings property of the CDC."""

from fgc_ncrp_parser.fgc_ncrp import fgc_property


class StatusWarnings(fgc_property.Property):
  """This property indicates the current warnings of the device

  Set:
    Set not permitted.

  Get:
    Gets the current warnings of the device. The value is a set of strings.

  NCRP Errors:
    - **11 set not perm**: The property is read only.

  """
  warnings = [
      'PBC_TMP',
      'CLP_OFF',
      'CLP_ON'
  ]

  def __init__(self, cdc):
    self.cdc = cdc
    super().__init__(
        name='STATUS.WARNINGS',
        access='r',
        getters=[lambda: ' '.join(self.value[0])],
        val_type=set,
        description='This property indicates the current warnings of the device'
    )

  def set_warning(self, value):
    """Method to set the warnings.

    Args:
      value (str): The warning to set. Must be one of the following: 'PBC_TMP', 'CLP_OFF', 'CLP_ON'.

    Raises:
      ValueError: If the argument is not a valid warning.
    """
    if value not in self.warnings:
      raise ValueError('Argument must be a Warnings object')
    self.value[0].add(value)

  def clear_warning(self, value):
    """Method to clear the warnings.

    Args:
      value (str): The warning to clear. Must be one of the following: 'PBC_TMP', 'CLP_OFF', 'CLP_ON'.

    Raises:
      ValueError: If the argument is not a valid warning.
    """
    if value not in self.warnings:
      raise ValueError('Argument must be a Warnings object')
    self.value[0].discard(value)
