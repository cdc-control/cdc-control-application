"""Driver for the FADEM"""

from enum import Enum

from drivers import fadem_consts as consts
from drivers import spi_controller


class Fadem(spi_controller.SPIController):
  """Class for the FADEM"""
  # enum class for the state of the fadem
  class Ranges(Enum):
    """ranges for the CDC"""
    RANGE_10A = 0b111
    RANGE_5A = 0b101
    RANGE_2A5 = 0b011
    RANGE_1A = 0b001
    OFF = 0b000

  class States(Enum):
    """states for the FADEM"""
    FADEM_RAMPDOWN = 0b100
    FADEM_NORMAL = 0b011
    FADEM_DEGAUSS = 0b010
    FADEM_IDLE = 0b001
    FADEM_INIT = 0b000

  def __init__(self, spi):
    self.state_cache = self.States.FADEM_INIT
    super().__init__(spi)

  def reset(self):
    """write the default to all registers. since they are all 16 bits, we address just the MSB but the write will write to both"""
    self._writeWord(consts.CMD_MSB_ADDR, 0)
    self.set_normal_gain(39500)
    self.set_degauss_gain(65535)
    self.set_dem1_shift(625)
    self._writeWord(consts.DEM2_SHIFT_MSB_ADDR, 0)
    self._writeWord(consts.DAC_CH1_MSB_ADDR, 0)
    self._writeWord(consts.DAC_CH2_MSB_ADDR, 0)

  def get_range(self):
    """Get the current range of the FADEM. This may raise a ValueError if the range is invalid or unknown. Exception should be caught by the caller."""
    response = self._readWord(consts.STATUS_LSB_ADDR)
    response &= (0b111 << consts.STATUS_LSB_RANGE_EN_OFFSET)
    response >>= consts.STATUS_LSB_RANGE_EN_OFFSET
    # if lsb is 0, return OFF
    if not response & 1:
      return self.Ranges.OFF
    else:
      return self.Ranges(response)

  def set_range(self, new_range: Ranges):
    """Set the range of the FADEM"""
    # we expect range to be a self.Ranges enum value
    if not isinstance(new_range, self.Ranges):
      raise ValueError("Invalid range")
    response = self._readWord(consts.CMD_LSB_ADDR)
    response &= ~(0b111 << consts.CMD_LSB_RANGE_EN_OFFSET)
    response |= (new_range.value << consts.CMD_LSB_RANGE_EN_OFFSET)
    self._writeWord(consts.CMD_LSB_ADDR, response)

  def get_state(self):
    """Get the current state of the FADEM. This may raise a ValueError if the state is invalid or unknown. Exception should be caught by the caller."""
    response = self._readWord(consts.STATUS_MSB_ADDR)
    response &= (0b111 << consts.STATUS_LSB_STATE0_OFFSET)
    response >>= consts.STATUS_LSB_STATE0_OFFSET
    state = self.States(response)
    self.state_cache = state
    return state

  def is_active(self):
    """Check if the FADEM is active"""
    return bool(self._getBit(consts.STATUS_MSB_ADDR, consts.STATUS_MSB_ACTIVE_OFFSET))

  def set_active(self, value: bool = True):
    """Set the active status of the FADEM"""
    self._setBit(consts.CMD_MSB_ADDR, consts.CMD_MSB_ACTIVATE_OFFSET, value)

  def get_clamp1(self):
    """Get the current clamp1 status of the FADEM"""
    return bool(self._getBit(consts.STATUS_LSB_ADDR, consts.STATUS_LSB_CLAMP1_OFFSET))

  def set_clamp1(self, value: bool = True):
    """Set the clamp1 status of the FADEM"""
    self._setBit(consts.CMD_LSB_ADDR, consts.CMD_LSB_CLAMP1_OFFSET, value)

  def get_clamp2(self):
    """Get the current clamp2 status of the FADEM"""
    return bool(self._getBit(consts.STATUS_LSB_ADDR, consts.STATUS_LSB_CLAMP2_OFFSET))

  def set_clamp2(self, value: bool = True):
    """Set the clamp2 status of the FADEM"""
    self._setBit(consts.CMD_LSB_ADDR, consts.CMD_LSB_CLAMP2_OFFSET, value)

  def set_clamps(self, value: bool = True):
    """Set both clamps of the FADEM"""
    self.set_clamp1(value)
    self.set_clamp2(value)

  def get_clamps(self):
    """Get the current clamps status of the FADEM. Returns True if both clamps are active, False otherwise."""
    return self.get_clamp1() and self.get_clamp2()

  def set_normal_gain(self, gain):
    """Set the normal gain value of the FADEM"""
    self._writeWord(consts.NORMAL_GAIN_MSB_ADDR, gain)

  def set_degauss_gain(self, gain):
    """Set the degauss gain value of the FADEM"""
    self._writeWord(consts.DEGAUSS_GAIN_MSB_ADDR, gain)

  def set_dem1_shift(self, shift):
    """Set the demodulator 1 shift of the FADEM"""
    self._writeWord(consts.DEM1_SHIFT_MSB_ADDR, shift)

  def set_dem2_shift(self, shift):
    """Set the demodulator 2 shift of the FADEM"""
    self._writeWord(consts.DEM2_SHIFT_MSB_ADDR, shift)

  def set_dac(self, value):
    """Set DAC channels to the value symmetrically around 0x8000"""
    # check that value is in range 0x7FFF to -0x8000
    if value < -0x8000 or value > 0x7FFF:
      raise ValueError("Invalid DAC value")

    self._set_dac_ch1_out(0x8000-value)
    self._set_dac_ch2_out(0x8000+value)

  def get_dac(self):
    """Gets the difference between DAC channels"""
    return (self._get_dac_ch2_out() - self._get_dac_ch1_out()) / 2

  def _set_dac_ch1_out(self, value):
    """Set DAC channel 1 output value"""
    self._writeWord(consts.DAC_CH1_MSB_ADDR, value)

  def _set_dac_ch2_out(self, value):
    """Set DAC channel 2 output value"""
    self._writeWord(consts.DAC_CH2_MSB_ADDR, value)

  def get_normal_gain(self):
    """Get normal gain value"""
    return self._readWord(consts.NORMAL_GAIN_MSB_ADDR)

  def get_degauss_gain(self):
    """Get degauss gain value"""
    return self._readWord(consts.DEGAUSS_GAIN_MSB_ADDR)

  def get_dem1_shift(self):
    """Get demodulator 1 shift value"""
    return self._readWord(consts.DEM1_SHIFT_MSB_ADDR)

  def get_dem2_shift(self):
    """Get demodulator 2 shift value"""
    return self._readWord(consts.DEM2_SHIFT_MSB_ADDR)

  def _get_dac_ch1_out(self):
    """Get DAC channel 1 output value"""
    return self._readWord(consts.DAC_CH1_MSB_ADDR)

  def _get_dac_ch2_out(self):
    """Get DAC channel 2 output value"""
    return self._readWord(consts.DAC_CH2_MSB_ADDR)

  def get_status_reg(self):
    """Get the status register value"""
    return self._readWord(consts.STATUS_MSB_ADDR)

  def get_cmd_reg(self):
    """Get the command register value"""
    return self._readWord(consts.CMD_MSB_ADDR)
