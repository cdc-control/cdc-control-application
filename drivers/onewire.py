"""
File from: https://gitlab.cern.ch/cce/cce/-/blob/master/common/vhd/interfaces/one_wire/python/onewire.py

Convenience methods for controlling 1-Wire module."""

import drivers.onewire_regs_const as const
import time

MAX_DEVICES = 32
READOUT_TIMEOUT = 10  # in seconds
REGS_PER_DEVICE = 4  # no. of 32-bit registers per Onewire device

# Constants to select the mux output bus
MUX_AUXB = 0
MUX_MAINB = 1
MUX_MAINA = 2
MUX_AUXA = 3
MUX_MAGNET = 6
MUX_LOCAL = 7


class onewire:
  """
  Provides access to all module functionality and registers.

  Also implements custom methods for most used functionality.
  """

  def __init__(self, bus, offset):
    """Class constructor.

    :param bus: Communication object providing read() and write() methods
    :param offset: IPs base address in memory map
    """
    self._bus = bus
    self._offset = offset

  def read(self, addr):
    """Read one of module registers.

    :param addr: Register relative address
    :return: Data read from register
    """
    assert addr <= const.ONEWIRE_REGS_SIZE
    return self._bus.read(self._offset + addr)

  def write(self, addr, data):
    """Write to one of module registers.

    :param addr: Register relative address
    :param data: Register data to be written
    """
    assert addr <= const.ONEWIRE_REGS_SIZE
    self._bus.write(self._offset + addr, data)

  def start_readout(self):
    """Start 1-Wire readout.

    :raises Exception: if there any errors detected in status register
    :raises Exception: if the readout takes too long
    :returns Number of Onewire devices detected in the readout
    """
    self.write(const.ADDR_ONEWIRE_REGS_CONTROL,
               const.ONEWIRE_REGS_CONTROL_TRIG)
    timeout = time.time() + READOUT_TIMEOUT
    while True:
      val = self.read(const.ADDR_ONEWIRE_REGS_STATUS)
      if val & const.ONEWIRE_REGS_STATUS_BUSY:  # busy-flag is raised
        if time.time() > timeout:
          raise Exception("Timeout in Onewire readout")
      else:
        # too many-flag is raised
        if val & const.ONEWIRE_REGS_STATUS_DEV_TOO_MANY:
          raise Exception(
              "Too many devices detected in the 1-Wire chain.")
        else:
          ow_count = (val & const.ONEWIRE_REGS_STATUS_COUNT) \
              >> const.ONEWIRE_REGS_STATUS_COUNT_OFFSET
          return ow_count

  def set_mux_output(self, output, enable=True):
    """Set the MUX output channel and enable.

    These have to be set at the same time,
    because the mux-register is write-only.

    :param output: index of the output chosen, constants are provided at
                   the beginning of the file
    :param enable: True to enable mux output, False to disable
    """
    enable = 1 if enable else 0
    reg = (output << const.ONEWIRE_REGS_MUX_CTRL_OFFSET) | \
          (enable << const.ONEWIRE_REGS_MUX_EN_OFFSET)
    self.write(const.ADDR_ONEWIRE_REGS_MUX, reg)

  def reset(self):
    """Reset the Onewire module."""
    self.write(const.ADDR_ONEWIRE_REGS_CONTROL,
               const.ONEWIRE_REGS_CONTROL_RST)

  def get_id(self, index):
    """Read a device ID.

    :param index: The index of the slave, as stored in memory
    :returns The 64-bit ID, as stored in the register
    """
    assert index < MAX_DEVICES
    id0 = self.read(const.ADDR_ONEWIRE_REGS_MEM_READOUT +
                    const.ONEWIRE_REGS_MEM_READOUT_SIZE
                    * index * REGS_PER_DEVICE)
    id1 = self.read(const.ADDR_ONEWIRE_REGS_MEM_READOUT +
                    const.ONEWIRE_REGS_MEM_READOUT_SIZE
                    * (index * REGS_PER_DEVICE + 1))
    id_tot = (id1 << 32) | id0
    return id_tot

  def get_no_of_devices(self):
    """Read the number of devices from status-register.

    Does not perform a readout, like start_readout().
    Requires that at least one readout has been done.

    :returns The number of devices, as stored in status-register
    """
    status_reg = self.read(const.ADDR_ONEWIRE_REGS_STATUS)
    ow_count = (status_reg & const.ONEWIRE_REGS_STATUS_COUNT) \
        >> const.ONEWIRE_REGS_STATUS_COUNT_OFFSET
    return ow_count

  def get_index(self, id):
    """Get the position where the data of a device is stored in the memory.

    :param id: the 64-bit ID of the desired Onewire device
    :raises Exception, if the ID cannot be found in memory
    :returns The position in memory
    """
    no_of_devices = self.get_no_of_devices()
    for i in range(0, no_of_devices):
      id_temp = self.get_id(i)
      if id_temp == id:
        return i
    raise Exception("Could not find a device with the given ID!")

  def get_device_data_by_index(self, index):
    """Get the ID and scratchpad data in an array.

    :param index: The index of the slave, as stored in memory
    :returns Dictionary, where "ID" stores the ID,
             "Temperature" the temperature in Celsius,
             "Scratchpad" the scratchpad bytes in an array
    """
    id = self.get_id(index)
    temperature = self.get_temperature_celsius(index)
    sp = 8*[0]
    addr = const.ADDR_ONEWIRE_REGS_MEM_READOUT
    size = const.ONEWIRE_REGS_MEM_READOUT_SIZE
    regs = REGS_PER_DEVICE

    sp[0] = (self.read(addr + size * (index*regs+2)) & 0xff) >> 0
    sp[1] = (self.read(addr + size * (index*regs+2)) & 0xff00) >> 8
    sp[2] = (self.read(addr + size * (index*regs+2)) & 0xff0000) >> 16
    sp[3] = (self.read(addr + size * (index*regs+2)) & 0xff000000) >> 24
    sp[4] = (self.read(addr + size * (index*regs+3)) & 0xff) >> 0
    sp[5] = (self.read(addr + size * (index*regs+3)) & 0xff00) >> 8
    sp[6] = (self.read(addr + size * (index*regs+3)) & 0xff0000) >> 16
    sp[7] = (self.read(addr + size * (index*regs+3)) & 0xff000000) >> 24
    return {"ID": id, "Temperature": temperature, "Scratchpad": sp}

  def get_device_data_by_id(self, id):
    """Get the ID and scratchpad data in an array.

    :param id: ID of the slave
    :returns Dictionary, where "ID" stores the ID,
             "Temperature" the temperature in Celsius,
             "Scratchpad" the scratchpad bytes in an array
    """
    index = self.get_index(id)
    return self.get_device_data_by_index(index)

  def get_temperature_raw(self, index):
    """Get the raw temperature.

    :param index: The index of the slave, as stored in memory
    :returns The temperature in raw format
    """
    assert index < MAX_DEVICES
    reg_data = self.read(const.ADDR_ONEWIRE_REGS_MEM_READOUT +
                         const.ONEWIRE_REGS_MEM_READOUT_SIZE
                         * (index * REGS_PER_DEVICE + 2))
    temp = reg_data & const.ONEWIRE_REGS_MEM_READOUT_DATA_LOW
    return temp

  def get_temperature_celsius(self, index, accuracy=12):
    """Get the temperature in Celsius.

    :param index: The index of the slave, as stored in memory
    :param accuracy: The accuracy, with which the sensor has
                     been programmed. 12 bits by default.
    :returns The temperature in degrees Celsius
    """
    assert index < MAX_DEVICES
    assert 9 <= accuracy <= 12
    raw_temp = self.get_temperature_raw(index)
    # Check if sign bit (bits 11 to 15) is set
    if (raw_temp & (1 << 11)):
      # Conversion from 2's complement to signed
      raw_temp -= 2**16
    # Remove undefined bits
    raw_temp = raw_temp >> 12-accuracy
    # Conversion from raw to Celsius
    acc_multiplier = 0.5/2**(accuracy-9)
    temp_celsius = acc_multiplier*raw_temp
    return temp_celsius
