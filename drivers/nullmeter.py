"""This class is basically just a wrapper for the ad7689 driver, it is used to read the values from the ADC and convert them to physical values."""
import drivers.ad7689 as ad7689


class Channel:
  def __init__(self, index, channel_type):
    self.index = index
    self.channel_type = channel_type


class Nullmeter:
  """
    Reads are scaled according to this scheme:

    There are 5 channels in the Nulling meter board:
     - CH0 calibration
     - CH1 nulling current
     - CH2 compliance voltage
     - CH3 PBC temperature
     - CH4 output current

    All the channels are referred to COM and have a bipolar input, except for CH4 which is unipolar as it's the PBC temperature.
    This is achieved by shifting the common point to VREF/2 on all channels except CH4.
    So, in all except CH4, there's 2.048V applied to the COM input. Each channel ADC input voltage is given by a subtraction of the voltage in the CH line and the COM: (CHx - COM).
    In the case of all channels except CH4, an input of 0V on the ADC channel will correspond to a raw value of -32768, a value of 2.048 will yield  a raw value of zero and a value of 4.096 will yield a raw value of 32768.
    Knowing this, the correspondence between raw value and voltage or current to be measured (the value that we want to present to the user) for the different channels is the following:

    # **CH0**   
    | 10V input -> 4V at ADC input  -> 32000 raw value         (scale factor raw to Volt: 1/3200)
    | -10V input -> 0V at ADC input -> -32000 raw value       (scale factor raw to Volt: 1/3200)

    # **CH1**        
    | 100uA -> 100mV across 1kR resistor -> 4V at ADC input    -> 32000 raw value        (scale factor raw to uA: 1/320)
    | -100uA -> -100mV across 1kR resistor -> 0V at ADC input    -> -32000 raw value   (scale factor raw to uA: 1/320)

    # **CH2** compliance voltage
    | 15V input -> 4V at ADC input  -> 32000 raw value         (scale factor raw to Volt: 1/2133)
    | -15V input -> 0V at ADC input -> -32000 raw value       (scale factor raw to Volt: 1/2133)

    # **CH3** PBC temperature
    | 100 C -> 1V at ADC input  -> 8000 raw value  (scale factor raw to C: 1/80)
    | 0C -> 0V at ADC input  -> 8000 raw value        (scale factor raw to C: 1/80)

    # **CH4** output current
    | 1A -> 100mV across 0.1R resistor -> 0.4V at ADC input    -> 3200 raw value            (scale factor raw to A: 1/3200)
    | -1A -> -100mV across 0.1R resistor -> 0V at ADC input    -> -3200 raw value           (scale factor raw to A: 1/3200)
        
  """
  def __init__(self, spi):
    """Initializes the Nullmeter driver.
    """
    
    config = {
        'INCC': ad7689.consts.INCC_BIPOLAR_COM,
        'BW': ad7689.consts.BW_QUARTER,
        'REF': ad7689.consts.REF_EXTERNAL_AND_TEMP_SENSOR
    }
    self.adc = ad7689.AD7689(spi, config)
    self.channels = {
        '10V_cal': Channel(0, 'V'),
        'I_null': Channel(1, 'I'),
        'V_compliance': Channel(2, 'V'),
        'I_monitor': Channel(3, 'I'),
        'PBC_temp': Channel(4, 'T'),
        'zero': Channel(7, 'V')
    }
    self.zero = 0

  def read_raw_channel(self, channel):
    return self.adc.read(channel.index) - self.zero

  def get_10V_cal(self):
    """Returns the 10V calibration value."""
    reading = self.read_raw_channel(self.channels['10V_cal'])
    voltage = -reading / 3200.0
    return voltage

  def get_I_null(self):
    """Returns the null current in uAmps."""
    reading = self.read_raw_channel(self.channels['I_null'])
    current = -reading / 320.0
    return current

  def get_V_compliance(self):
    """Returns the compliance voltage."""
    reading = self.read_raw_channel(self.channels['V_compliance'])
    voltage = -reading / 2133.0
    return voltage

  def get_I_monitor(self):
    """Returns the monitor current."""
    reading = self.read_raw_channel(self.channels['I_monitor'])
    current = -reading / 3200.0
    return current

  def get_PBC_temp(self):
    """Returns the temperature of the PBC."""
    reading = self.read_raw_channel(self.channels['PBC_temp'])
    temp = reading / 80.0
    return temp

  def get_zero(self):
    """Returns the zero value."""
    zero = self.adc.read(7)
    return zero

  def set_zero(self, zero):
    """Sets the zero value. We leave the upper level to call this function after a get_zero() call."""
    self.zero = zero

  def get_voltage(self, channel):
    """Returns the voltage of a channel."""
    reading = self.read_raw_channel(self.channels[channel])

    # the voltage is 4.096V/2^bits
    voltage = reading * 4.096 / 2 ** self.adc.bits
    return voltage

  def print_channels(self):
    """Prints the various results of the channels."""
    print('10V_cal:', round(self.get_10V_cal(), 2))
    print('I_null:', round(self.get_I_null(), 2))
    print('V_compliance:', round(self.get_V_compliance(), 2))
    print('I_monitor:', round(self.get_I_monitor(), 2))
    print('PBC_temp:', round(self.get_PBC_temp(), 2))
    print('zero:', round(self.get_zero(), 2))

  def get_all_raw(self):
    """Returns all channels in raw format."""
    return [self.read_raw_channel(self.channels[channel]) for channel in self.channels]

  def get_all_channels(self):
    """Returns all channels."""
    return [
        self.get_10V_cal(),
        self.get_I_null(),
        self.get_V_compliance(),
        self.get_I_monitor(),
        self.get_PBC_temp(),
        self.get_zero()
    ]
