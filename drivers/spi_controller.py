# this file is for the fadem constants


class SPIController:
  # pass an instance of spi that is already open
  def __init__(self, spi):
    self.spi = spi

  def _readWord(self, addr):
    # this is a 16 bit transfer.
    # Bit 15: 1=write, 0=read
    #! Bit 14: "update": not implemented in the FADEM
    # Bits 13-8: address
    # Bits 7-0: data
    # we need to form the word
    #! we send MSB first
    addr &= 0x3F  # make sure the address is only 6 bits
    if addr % 2 != 0:
      addr -= 1
    # now we can send the word
    response = self.spi.xfer2([addr, 0, addr + 1, 0])
    # we need to combine the two bytes
    return (response[1] << 8) | response[3]

  def _writeWord(self, addr, value):
    "Write a word to the FADEM. addr is always the base address (MSB). value is the 16 bit value to write."
    # this is a 16 bit transfer.
    # Bit 15: 1=write, 0=read
    #! Bit 14: "update": not implemented in the FADEM
    # Bits 13-8: address
    # Bits 7-0: data
    # we need to form the word
    #! we send MSB first
    addr &= 0x3F  # make sure the address is only 6 bits
    if addr % 2 != 0:
      addr -= 1
    value &= 0xFFFF  # make sure the value is only 16 bits
    addr |= (1 << 7)  # set bit 7 to 1 for write
    self.spi.writebytes([addr, value >> 8, addr + 1, value & 0xFF])

  def _getBit(self, addr, offset):
    # if addr is even, it is the addr of the MSB, so we need to add 8 to the offset. if it is odd, it is the addr of the LSB and we need to subtract 1 from the addr, read the word, and then get the bit.
    if addr % 2 == 0:
      response = self._readWord(addr)
      return (response >> (offset + 8)) & 1
    else:
      response = self._readWord(addr - 1)
      return (response >> offset) & 1

  def _setBit(self, addr, offset, value=1):
    # if addr is even, it is the addr of the MSB, so we need to add 8 to the offset. if it is odd, it is the addr of the LSB and we need to subtract 1 from the addr, read the word, and then get the bit.
    if addr % 2 == 0:
      response = self._readWord(addr)
      if value:
        response |= (1 << (offset + 8))
      else:
        response &= ~(1 << (offset + 8))
      self._writeWord(addr, response)
    else:
      response = self._readWord(addr - 1)
      if value:
        response |= (1 << offset)
      else:
        response &= ~(1 << offset)
      self._writeWord(addr - 1, response)
