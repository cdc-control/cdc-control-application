"""This contains the class that implements the primrel device of the CERN CDC controller"""

from enum import Enum

from drivers import primrel_consts as consts
from drivers import spi_controller


class States(Enum):
  """states for the primary relay board"""
  GET_GPIO = 0b101    # PRIMREL is reading the relays, next state will be SET_GPIO
  SET_GPIO = 0b100    # PRIMREL is setting the relays, next state will be ACTIVE
  # PRIMREL ready to receive a command and set relays. Moves either to SET_GPIO or to INIT again.
  ACTIVE = 0b011
  # setting pull ups. Loads the register GPPU_VAL16. Moves to ACTIVE when finished configuring direction.
  CFG_GPPU = 0b010
  CFG_IODIR = 0b001   # setting the direction for all MCP27s17s on the primary relay board. Loads the register IODIR_VAL. Please notice that there is a state machine in the PRIMRELAY board to write to all the MCB27S17. Always moves to CFG_GPPU when finished configuring direction.
  INIT_MCP = 0b000    # transition phase just to wait that everything starts up. Waits that ACTIVATE is 1 and some time has passed and moves to CFG_IODIR


class SecondaryRelays(Enum):
  """secondary relays"""
  REL_SEC_4T = 0b1000  # 10 A range
  REL_SEC_8T = 0b0100  # 5 A range
  REL_SEC_16T = 0b0010  # 2.5 A range
  REL_SEC_40T = 0b0001  # 1 A range
  OFF = 0b0000


class Relays:
  """Class for representing the relays"""

  RELAY_NAMES = [
      "2048T", "1024T", "512T", "256T", "128T", "64T", "32T",
      "16T", "8T", "4T", "2T", "1T", "1TCAL", "1TDAC"
  ]

  def __init__(self, msb=None, lsb=None):
    if msb is None and lsb is None:
      self._relays = set()
    elif msb is not None and lsb is None:
      msb &= 0xFFFF  # make sure we only have 16 bits
      self._relays = self._bits_to_relay_names((msb >> 8) & 0xFF, msb & 0xFF)
    elif msb is not None and lsb is not None:
      self._relays = self._bits_to_relay_names(msb, lsb)
    else:
      raise ValueError("Invalid arguments")

  def get_set(self):
    """returns the relays set"""
    return self._relays.copy()

  def get_bytes(self):
    """returns the relays as a tuple of two bytes"""
    bits = self._relay_names_to_bits(self._relays)
    return (bits >> 8) & 0xFF, bits & 0xFF

  def get_16bit_integer(self):
    """returns the relays as one 16 bit integer"""
    return self._relay_names_to_bits(self._relays)

  # add operator == for comparing two Relays objects
  def __eq__(self, other):
    if isinstance(other, Relays):
      return self.get_set() == other.get_set()
    return False

  # add operator != for comparing two Relays objects
  def __ne__(self, other):
    return not self.__eq__(other)

  def _bits_to_relay_names(self, msb, lsb):
    """Converts the two bytes to a set of active relay names"""
    bits = (msb << 8) | lsb
    return set(name for i, name in enumerate(self.RELAY_NAMES) if bits & (1 << (13-i)))

  def _relay_names_to_bits(self, relay_names):
    """Converts a set of active relay names to a 16 bit integer"""
    bits = 0
    for i, name in enumerate(self.RELAY_NAMES):
      if name in relay_names:
        bits |= (1 << (13-i))
    return bits


class OnOffRelays(Relays):
  """Class for representing OnOff relays"""

  def get_active_relay_names(self):
    """returns the active relays as a list of relay names"""
    return ' '.join(['ON' + relay for relay in self.RELAY_NAMES if relay in self._relays])


class PolRelays(Relays):
  """Class for representing Pol relays"""

  def get_active_relay_names(self):
    """returns the active relays as a list of relay names"""
    return ' '.join(['INV' + relay for relay in self.RELAY_NAMES if relay in self._relays])


class Primrel(spi_controller.SPIController):
  """Class for controlling the PRIMREL board"""

  def __init__(self, spi):
    super().__init__(spi)
    self.ranges = {
        'OFF': SecondaryRelays.OFF,
        'RANGE_1A': SecondaryRelays.REL_SEC_40T,
        'RANGE_2A5': SecondaryRelays.REL_SEC_16T,
        'RANGE_5A': SecondaryRelays.REL_SEC_8T,
        'RANGE_10A': SecondaryRelays.REL_SEC_4T
    }
    self.dac_1t_zero = 0
    self.dac_1t_fs = 0x8000
    self.on_off_relays_cache = OnOffRelays()
    self.pol_relays_cache = PolRelays()
    self.on_off_1tdac_relay_cache = False
    self.on_off_1tcal_relay_cache = False
    self.pol_1tdac_relay_cache = False
    self.pol_1tcal_relay_cache = False
    # we assume that the board is in INIT_MCP state when we start
    self.state_cache = States.INIT_MCP
    self.vb_status_cache = self.VBStatus.VB_OFF

  class VBStatus(Enum):
    """VB status enum, can be VB_OK = 0b00, VB_OFF = 0b01, VB_TEMP_FAULT = 0b10, VB_DISCONNECTED = 0b11"""
    VB_DISCONNECTED = 0b11
    VB_OFF = 0b01
    VB_OK = 0b00
    VB_TEMP_FAULT = 0b10

  def reset(self):
    """write the default to all registers. since they are all 16 bits, we address just the MSB but the write will write to both"""
    self._writeWord(consts.CMD_MSB_ADDR, 0)
    self._writeWord(consts.REL_POL_CMD_MSB_ADDR, 0)
    self._writeWord(consts.REL_ONOFF_CMD_MSB_ADDR, 0)
    self._writeWord(consts.EXT_OUT_CMD_MSB_ADDR, 0)
    self._writeWord(consts.ONETURN_DAC_CH1_OUT_MSB_ADDR, 0x8000)
    self._writeWord(consts.ONETURN_DAC_CH2_OUT_MSB_ADDR, 0x8000)

  def is_active(self):
    """Returns True if the device is active."""
    return bool(self._getBit(consts.STATUS_MSB_ADDR, consts.STATUS_MSB_ACTIVE_OFFSET))

  def set_active(self, value: bool = True):
    """Sets the device's active state to the given boolean value."""
    self._setBit(consts.CMD_MSB_ADDR, consts.CMD_MSB_ACTIVATE_OFFSET, value)

  def get_state(self):
    """Returns the current state of the device."""
    response = self._readWord(consts.STATUS_LSB_ADDR)
    response &= (0b111 << consts.STATUS_LSB_STATE_OFFSET)
    response >>= consts.STATUS_LSB_STATE_OFFSET
    self.state_cache = States(response)
    return self.state_cache

  def set_1t_dac_zero(self, zero=0):
    """Sets the DAC 1T zero value."""
    self.dac_1t_zero = zero

  def set_1t_dac_fs(self, full_scale=0x8000):
    """Sets the DAC 1T full scale value."""
    self.dac_1t_fs = full_scale

  def _set_dac_ch1(self, value):
    """Sets the DAC channel 1 to the given value."""
    self._writeWord(consts.ONETURN_DAC_CH1_OUT_MSB_ADDR, value)

  def _set_dac_ch2(self, value):
    """Sets the DAC channel 2 to the given value."""
    self._writeWord(consts.ONETURN_DAC_CH2_OUT_MSB_ADDR, value)

  def _get_dac_ch1(self):
    """Returns the current value of DAC channel 1."""
    return self._readWord(consts.ONETURN_DAC_CH1_OUT_MSB_ADDR)

  def _get_dac_ch2(self):
    """Returns the current value of DAC channel 2."""
    return self._readWord(consts.ONETURN_DAC_CH2_OUT_MSB_ADDR)

  def set_dac(self, value):
    """Set DAC channels to the value symmetrically around 0x8000"""
    # check that value is in range 0x7FFF to -0x8000
    if value < -0x8000 or value > 0x7FFF:
      raise ValueError("Invalid DAC value")

    # subtract the zero
    value -= self.dac_1t_zero

    # scale to the full scale
    value = int(value * self.dac_1t_fs / 0x8000)

    self._set_dac_ch1(0x8000-value)
    self._set_dac_ch2(0x8000+value)

  def get_dac(self):
    """Gets the difference between DAC channels"""
    return (self._get_dac_ch2() - self._get_dac_ch1()) / 2

  def get_rel_ext_status(self):
    """Returns True if the external relay is activated."""
    return bool(self._getBit(consts.EXT_OUT_CMD_LSB_ADDR, consts.EXT_OUT_CMD_LSB_REL_EXT_OFFSET))

  def set_rel_ext_status(self, value: bool = True):
    """Sets the external relay to the given boolean value."""
    self._setBit(consts.EXT_OUT_CMD_LSB_ADDR,
                 consts.EXT_OUT_CMD_LSB_REL_EXT_OFFSET, value)

  def get_vb_clamp(self):
    """Returns True if the VB clamp is activated."""
    return bool(self._getBit(consts.EXT_OUT_CMD_LSB_ADDR, consts.EXT_OUT_CMD_LSB_CMD_VB_CLAMP_OFFSET))

  def set_vb_clamp(self, value: bool = True):
    """Sets the VB clamp to the given boolean value."""
    # not documented yet: the VB clamp is active low
    self._setBit(consts.EXT_OUT_CMD_LSB_ADDR,
                 consts.EXT_OUT_CMD_LSB_CMD_VB_CLAMP_OFFSET, not value)

  def set_range(self, value):
    """Sets the range to the given value."""
    # we need to convert between the range values and the secondary relay values
    # the range values are RANGE_1A, RANGE_2A5, RANGE_5A, RANGE_10A, the corresponding relay values are REL_SEC_40T, REL_SEC_16T, REL_SEC_8T, REL_SEC_4T
    if value not in self.ranges:
      raise ValueError("Invalid range value")
    self._set_secondary_relays(self.ranges[value])

  def set_primary_relays(self, relays: int):
    """Sets the primary relays without affecting the 1TDAC and 1TCAL relays."""
    # value must be between 0xFFF and 0x000
    if relays < 0 or relays > 0xFFF:
      raise ValueError("Invalid relays value")

    # get the current value of the relays, to preserve the 1TDAC and 1TCAL relays
    response = self._readWord(consts.REL_ONOFF_CMD_MSB_ADDR)
    response &= 0x0003
    response |= (relays << 2)
    self._writeWord(consts.REL_ONOFF_CMD_MSB_ADDR, response)

  def get_primary_relays(self):
    """Returns the current value of the primary relays."""
    response = self._readWord(consts.REL_ONOFF_CMD_MSB_ADDR)
    response &= 0x0FFC
    response >>= 2
    return response

  def set_negative(self, isnegative: bool):
    """Direction = True for negative, False for positive."""
    # get the current value of the relays
    response = self._readWord(consts.REL_POL_CMD_MSB_ADDR)
    response &= 0x0003
    if isnegative:
      response |= 0xFFFC
    self._writeWord(consts.REL_POL_CMD_MSB_ADDR, response)

  def set_1t_dac_relay(self, value: bool = True):
    """Sets the 1TDAC relay to the given boolean value."""
    self._setBit(consts.REL_ONOFF_CMD_LSB_ADDR,
                 consts.REL_ONOFF_CMD_LSB_1TDAC_OFFSET, value)
    # readout to check and update the cached value
    relay_readback = self.get_1t_dac_relay()
    print(f'Primrel 1tdac relay readback match: {relay_readback == value}')
    return relay_readback == value

  def get_1t_dac_relay(self):
    """Returns the current value of the 1TDAC relay."""
    relay = bool(self._getBit(consts.REL_ONOFF_CMD_LSB_ADDR,
                 consts.REL_ONOFF_CMD_LSB_1TDAC_OFFSET))
    self.on_off_1tcal_relay_cache = relay
    return relay

  def set_1t_cal_relay(self, value: bool = True):
    """Sets the 1TCAL relay to the given boolean value."""
    self._setBit(consts.REL_ONOFF_CMD_LSB_ADDR,
                 consts.REL_ONOFF_CMD_LSB_1TCAL_OFFSET, value)
    # readout to check and update the cached value
    relay_readback = self.get_1t_cal_relay()
    print(f'Primrel 1tcal relay readback match: {relay_readback == value}')
    return relay_readback == value

  def get_1t_cal_relay(self):
    """Returns the current value of the 1TCAL relay."""
    relay = bool(self._getBit(consts.REL_ONOFF_CMD_LSB_ADDR,
                 consts.REL_ONOFF_CMD_LSB_1TCAL_OFFSET))
    self.on_off_1tcal_relay_cache = relay
    return relay

  def set_1t_dac_negative(self, value: bool = True):
    """Sets the 1TDAC polarity to the given boolean value."""
    self._setBit(consts.REL_POL_CMD_LSB_ADDR,
                 consts.REL_POL_CMD_LSB_1TDAC_OFFSET, not value)
    relay_readback = self.get_1t_dac_negative()
    print(f'Primrel 1tdac polarity readback match: {relay_readback == value}')
    return relay_readback == value

  def get_1t_dac_negative(self):
    """Returns the current value of the 1TDAC polarity."""
    relay = not bool(self._getBit(consts.REL_POL_CMD_LSB_ADDR,
                     consts.REL_POL_CMD_LSB_1TDAC_OFFSET))
    self.pol_1tdac_relay_cache = relay
    return relay

  def set_1t_cal_negative(self, value: bool = True):
    """Sets the 1TCAL polarity to the given boolean value."""
    self._setBit(consts.REL_POL_CMD_LSB_ADDR,
                 consts.REL_POL_CMD_LSB_1TCAL_OFFSET, value)
    relay_readback = self.get_1t_cal_negative()
    print(f'Primrel 1tcal polarity readback match: {relay_readback == value}')
    return relay_readback == value

  def get_1t_cal_negative(self):
    """Returns the current value of the 1TCAL polarity."""
    relay = bool(self._getBit(consts.REL_POL_CMD_LSB_ADDR,
                              consts.REL_POL_CMD_LSB_1TCAL_OFFSET))
    self.pol_1tcal_relay_cache = relay
    return relay

  def _set_secondary_relays(self, relays: SecondaryRelays):
    """Set secondary relays."""
    response = self._readWord(consts.EXT_OUT_CMD_LSB_ADDR)
    response &= ~(0b1111 << consts.EXT_OUT_CMD_LSB_REL_SEC_40T_OFFSET)
    response |= (relays.value << consts.EXT_OUT_CMD_LSB_REL_SEC_40T_OFFSET)
    self._writeWord(consts.EXT_OUT_CMD_LSB_ADDR, response)

  def get_mod_amp_status(self):
    """Get Mod Amp status."""
    return bool(self._getBit(consts.EXT_IN_MSB_ADDR, consts.EXT_IN_MSB_ST_MODAMP_OFFSET)
                )

  def get_pbc_x5_status(self):
    """Get PBCx5 status."""
    return bool(self._getBit(consts.EXT_IN_MSB_ADDR, consts.EXI_IN_MSB_ST_PBC_X5_OFFSET))

  def get_pbc_temp_warning(self):
    """Get PBC temperature warning status."""
    # warning is if both temp1 and temp2 are low
    # this could be implemented more efficiently by reading the whole register and masking
    return not (self.get_pbc_temp1() or self.get_pbc_temp2())

  def get_pbc_temp_fault(self):
    """Get PBC temperature fault status."""
    # fault is if temp1 is high and temp2 is low
    # this could be implemented more efficiently by reading the whole register and masking
    return self.get_pbc_temp1() and not self.get_pbc_temp2()

  def get_pcb_temp_ok(self):
    """Get PBC temperature ok status."""
    # ok is if temp1 is low and temp2 is high
    # this could be implemented more efficiently by reading the whole register and masking
    return not self.get_pbc_temp1() and self.get_pbc_temp2()

  def get_pbc_temp2(self):
    """Get PBC temperature 2 status."""
    return bool(self._getBit(consts.EXT_IN_MSB_ADDR, consts.EXT_IN_MSB_ST_PBC_TEMP2_OFFSET))

  def get_pbc_temp1(self):
    """Get PBC temperature 1 status."""
    return bool(self._getBit(consts.EXT_IN_MSB_ADDR, consts.EXT_IN_MSB_ST_PBC_TEMP1_OFFSET))

  def get_pbc_compliance(self):
    """Get PBC compliance status."""
    return not bool(self._getBit(consts.EXT_IN_MSB_ADDR, consts.EXT_IN_MSB_ST_PBC_CMPL_OFFSET))

  def get_pbc_cal(self):
    """Get PBC CAL status."""
    return not bool(self._getBit(consts.EXT_IN_MSB_ADDR, consts.EXT_IN_MSB_ST_PBC_CAL_OK_OFFSET))

  def get_pbc_charge(self):
    """Get PBC charge status."""
    return not bool(self._getBit(consts.EXT_IN_MSB_ADDR, consts.EXT_IN_MSB_ST_PBC_CHR_OK_OFFSET))

  def get_pbc_power_on(self):
    """Get PBC power on status."""
    return not bool(self._getBit(consts.EXT_IN_LSB_ADDR, consts.EXT_IN_LSB_ST_PBC_PWR_ON_OFFSET))

  def get_secondary_relays(self):
    """Get secondary relays."""
    response = self._readWord(consts.EXT_IN_LSB_ADDR)
    response &= (0b1111 << consts.EXT_IN_LSB_SEC_ST_40T_OFFSET)
    response >>= consts.EXT_IN_LSB_SEC_ST_40T_OFFSET
    # invert the bits
    response ^= 0b1111
    return SecondaryRelays(response)

  def get_vb_status(self):
    """Get VB status."""
    response = self._readWord(consts.EXT_IN_LSB_ADDR)
    response &= (0b11 << consts.EXT_IN_LSB_ST_VB_ACTIVE_OFFSET)
    response >>= consts.EXT_IN_LSB_ST_VB_ACTIVE_OFFSET
    status = self.VBStatus(response)
    self.vb_status_cache = status
    return status

  def get_pwr_amp_status(self):
    """Get power amp status."""
    # TODO check if this is correct, or if the bit is inverted
    return bool(self._getBit(consts.EXT_IN_LSB_ADDR, consts.EXT_IN_LSB_ST_PAMP_OFFSET))

  def get_on_off_relays(self):
    """Get on/off relays.

    #! before reading the relays, we need to write to the status register, in order to update the IO expanders. This is not documented in the primrel documentation, but it is in the primrel verilog code. I'm not sure whether this is a bug or a feature.

    """
    self._writeWord(consts.STATUS_MSB_ADDR, 0x1234)
    # the relays are inverted in the read, so invert them back.
    response = self._readWord(consts.REL_ONOFF_STATUS_MSB_ADDR)
    # invert the bits
    response ^= 0x3FFF
    # mask off the two msb that are unused
    response &= 0x3FFF
    relays = OnOffRelays(response)
    self.on_off_relays_cache = relays
    return relays

  def get_pol_relays(self):
    """Get polarity relays.

    #! before reading the relays, we need to write to the status register, in order to update the IO expanders. This is not documented in the primrel documentation, but it is in the primrel verilog code. I'm not sure whether this is a bug or a feature.

    """
    self._writeWord(consts.STATUS_MSB_ADDR, 0x1234)

    response = self._readWord(consts.REL_POL_STATUS_MSB_ADDR)
    response &= 0x3FFF
    # the polarity relays are active high
    relays = PolRelays(response)
    self.pol_relays_cache = relays
    return relays

  def set_on_off_relays(self, on_off_relays):
    """Set on/off relays."""
    word = on_off_relays.get_16bit_integer()
    self._writeWord(consts.REL_ONOFF_CMD_MSB_ADDR, word)
    # readback
    readback = self.get_on_off_relays()
    return readback == self.on_off_relays_cache

  def set_pol_relays(self, pol_relays):
    """Set polarity relays."""
    word = pol_relays.get_16bit_integer()
    self._writeWord(consts.REL_POL_CMD_MSB_ADDR, word)
    # readback
    readback = self.get_pol_relays()
    return readback == self.pol_relays_cache
