
import drivers.ad7689_consts as consts


class AD7689:
  def __init__(self, spi, config=None):
    """Initializes the AD7689 driver."""
    self.spi = spi
    self.config = config or consts.DEFAULT_CONFIG
    self.bits = 16

  def read(self, channel):
    """Reads the given channel and returns the value.

    Args:
      channel: Channel to read. Must be between 0 and 7.
    """
    config = self._generate_config(channel)

    # read two meaningless readings
    _ = self.spi.xfer2([config >> 8, config & 0xFF])
    _ = self.spi.xfer2([0x00, 0x00])
    response = self.spi.xfer2([0x00, 0x00])

    if self.config['INCC'] in [consts.INCC_BIPOLAR_COM, consts.INCC_BIPOLAR_DIFFERENTIAL]:
      signedness = True
    else:
      signedness = False

    value = int.from_bytes(response, byteorder='big', signed=signedness)
    return value

  def read_all(self):
    """Reads all channels and returns the values as a list."""
    values = []
    for channel in range(8):
      value = self.read(channel)
      values.append(value)
    return values

  def read_temperature(self):
    """Reads the temperature channel and returns the value. REF should be set adequately to activate the temperature sensor."""

    config = ((consts.CFG_WRITE << consts.CFG_SHIFT) |
              (consts.INCC_TEMP_SENSOR << consts.INCC_SHIFT) |
              (0 << consts.INx_SHIFT) |
              (self.config['BW'] << consts.BW_SHIFT) |
              (self.config['REF'] << consts.REF_SHIFT) |
              (consts.SEQ_DISABLED << consts.SEQ_SHIFT) |
              (consts.NO_READBACK << consts.READBACK_SHIFT)) << 2
    # read two meaningless readings
    _ = self.spi.xfer2([config >> 8, config & 0xFF])
    _ = self.spi.xfer2([0x00, 0x00])
    response = self.spi.xfer2([0x00, 0x00])

    value = int.from_bytes(response, byteorder='big')
    temp = self.count_to_temperature(value, 4.096)
    return temp

  def count_to_temperature(self, count, reference_voltage):
    """Converts the given count to a temperature value using the given reference voltage."""
    # Calculate the voltage per count
    voltage_per_count = reference_voltage / 2**16
    # Convert the count to voltage
    voltage = count * voltage_per_count
    # Calculate the temperature difference from the reference voltage (283mV at 25C)
    temperature_diff = (voltage - 0.283) / 0.001
    # Calculate the temperature
    temperature = 25 + temperature_diff
    return temperature

  def _generate_config(self, channel):
    """Generates the configuration word for the given channel."""
    config = (consts.CFG_WRITE << consts.CFG_SHIFT) | \
             (self.config['INCC'] << consts.INCC_SHIFT) | \
             (channel << consts.INx_SHIFT) | \
             (self.config['BW'] << consts.BW_SHIFT) | \
             (self.config['REF'] << consts.REF_SHIFT) | \
             (consts.SEQ_DISABLED << consts.SEQ_SHIFT) | \
             (consts.NO_READBACK << consts.READBACK_SHIFT)

    return config << 2
