"""
File from: https://gitlab.cern.ch/cce/fgc4/-/blob/master/common/python/hw/devmem.py

Module class for accessing hardware through /dev/mem or similar interface.

Should also work with UIO drivers
"""

import os
import mmap


class devmem:
  """Memory-mapped communication class for /dev/mem or UIO interfaces.

  Provides basic init and read/write methods.
  Access is aligned to word boundaries.
  """

  wsize = 4  # word size in bytes

  def __init__(self, base: int, length: int, fname: str = '/dev/mem'):
    """Class constructor.

    :param base: Base HW address. Should be aligned to page boundary
    :param length: Length of memory mapped space in bytes
    :param fname: Path to the mmap device file
    """
    assert base >= 0
    assert length >= 1
    if base != (base & ~(mmap.PAGESIZE - 1)):
      raise AssertionError("Base address must be page aligned")

    self.base = base
    self.length = length
    self.fname = fname

    self.f = os.open(self.fname, os.O_RDWR | os.O_SYNC)
    mem = mmap.mmap(self.f, self.length, mmap.MAP_SHARED,
                    mmap.PROT_READ | mmap.PROT_WRITE, offset=self.base)
    # Memory view allows a natural, C-like operation on memory-mapped files
    # However, default access is byte-based even if we use slicing, so we
    # need to cast it to a width corresponding to the real bus width
    self.mem = memoryview(mem).cast('I')

  def write(self, addr: int, data: int) -> None:
    """Write data to hardware.

    :param addr: HW address
    :param data: HW data
    """
    assert addr >= 0

    self.mem[addr // self.wsize] = data

  def read(self, addr: int) -> int:
    """Read data from hardware.

    :param addr: HW address
    :return: Value read from hardware bus
    """
    assert addr >= 0

    return int(self.mem[addr // self.wsize])
