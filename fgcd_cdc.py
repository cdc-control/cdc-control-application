"This is the class that implements the property model for the CDC"
# The properties to be implemented are:
# simple is a property that is simple instance of a the base class Property, i.e. just a variable
# complex is a derived class that implements actions following set or get
#
# DEVICE.VERSION.MAIN_PROG (simple)
# STATUS.FAULTS (complex)
# STATUS.WARNINGS (complex)
# CALSYS.DCM.ADC.CAL (complex)
# CALSYS.DCM.ADC.RAW (complex,array)
# CALSYS.DCM.ADC.VOLT (complex,array)-
# CALSYS.DCM.ADC.RAW_ZERO (persistent, simple)
# CALSYS.DCM.PS05.CAL (persistent, simple)
# CALSYS.DCM.PS05.MEAS (complex)
# CALSYS.DCM.PS12.CAL (persistent, simple)
# CALSYS.DCM.PS12.MEAS (complex)
# CALSYS.DCM.PS15.CALPOS (persistent, simple)
# CALSYS.DCM.PS15.MEASPOS (complex)
# CALSYS.DCM.PS15.CALNEG (persistent, simple)
# CALSYS.DCM.PS15.MEASNEG (complex)
# CALSYS.CDC.STATUS.RELAYS (complex)
# CALSYS.CDC.OUTPUT (complex)
# CALSYS.CDC.I_REF (complex)
# CALSYS.CDC.DAC1TW.CAL (persistent, complex)
# CALSYS.CDC.DAC1TW.ZERO (persistent, simple)
# CALSYS.CDC.DAC1TW.FS (persistent, simple)
# CALSYS.CDC.NULL.KP (persistent, simple)
# CALSYS.CDC.NULL.KI (persistent, simple)
# CALSYS.CDC.NULL.NR (simple)
# CALSYS.CDC.NULL.AUTO (complex)
# CALSYS.CDC.ADC.GAIN_1A (persistent, complex)
# CALSYS.CDC.ADC.I_MEAS (complex)
# CALSYS.CDC.ADC.V_MEAS (complex)
# CALSYS.CDC.ADC.I_NULL (complex)
# CALSYS.CDC.ADC.PBC_TEMP (complex)
# CALSYS.CDC.DAC.RAW (complex, array)
# CALSYS.CDC.DAC.VOLT (complex, array)
# CALSYS.CDC.VB_MODE (complex)

# The class should do type and range checking on the values that are set and return the correct error code if the value is out of range or the wrong type.
# The class should also implement the get and set methods for the properties.

# the class contains a lock that is used to protect the class variables from being accessed by multiple threads at the same time. The lock is acquired before the class variables are accessed and released after the class variables are accessed.


import enum
import threading
import time
import os

import spidev
# import fgc_ncrp_parser.fgc_ncrp.errors as errors
import fgc_ncrp_parser.fgc_ncrp.fgc_property as ncrp_property
import fgc_ncrp_parser.fgc_ncrp.errors as errors
import cdc_properties
import fgcd
from drivers import fadem as fadem_module
from drivers import primrel as primrel_module
from drivers import nullmeter as nullmeter_module

import cdc_sysmon


class CDC(fgcd.Device):
  """Class for the CDC

  This class implements the property model for the CDC. It contains the properties and the methods to set and get the properties.

  Attributes:
    state (CDCStates): The state of the CDC. This is a private attribute, it should not be accessed directly. Use the get_state() and set_state() methods instead. The state can be one of the following values: INIT, IDLE, NULLING, ACTIVE, ERROR. The enum is defined in the CDCStates class.

    stepping (bool): This attribute is used to set the stepping setting of the CDC. If stepping is True, the CDC will step through the nulling procedure, otherwise it will run the nulling procedure in one go.

    sysmon (SystemMonitor): This attribute is the SystemMonitor object that is used to monitor the CDC.

    sysmon_thread (threading.Thread): This attribute is the thread that runs the SystemMonitor.

    spi_fadem (spidev.SpiDev): This attribute is the SPI object that is used to communicate with the FADEM.

    spi_primrel (spidev.SpiDev): This attribute is the SPI object that is used to communicate with the PRIMREL.

    spi_nullmeter (spidev.SpiDev): This attribute is the SPI object that is used to communicate with the NULLMETER.

    fadem (Fadem): This attribute is the Fadem object that is used to communicate with the FADEM.

    primrel (Primrel): This attribute is the Primrel object that is used to communicate with the PRIMREL.

    nullmeter (Nullmeter): This attribute is the Nullmeter object that is used to communicate with the NULLMETER.

    logger (logging.Logger): This attribute is the logger object that is used to log messages.

    properties (dict): This attribute is a dictionary that contains the properties of the CDC. The keys of the dictionary are the property names and the values are the property objects.

  """

  class CDCStates(enum.Enum):
    """"Enumeration of the states of the CDC.

    Attributes:
      INIT: The CDC is in the INIT state. In this state, the CDC is not ready to be used. The CDC is in this state after a power on or a reset. The CDC can only be initialized from this state. It is not possible to set or get properties in this state.

      IDLE: The CDC is in the IDLE state. In this state, the CDC is ready to be used. The CDC is in this state after a successful initialization. It is possible to set and get properties in this state.

      NULLING: The CDC is in the NULLING state. In this state, the CDC is nulling. It is not possible to set or get properties in this state.

      ACTIVE: The CDC is in the ACTIVE state. In this state, the CDC is active and in a valid range. It is possible to set and get properties in this state.

      ERROR: The CDC is in the ERROR state. In this state, the CDC is not in a valid range. The CDC will not recover from this state. It is still possible to set and get properties in this state. Please check the logs for the error message.
    """
    INIT = 0
    IDLE = 1
    NULLING = 2
    ACTIVE = 3
    ERROR = 4

  def __init__(self, logger, config):
    """Constructor of the CDC class.

    Args:
      logger (logging.Logger): The logger object that is used to log messages.

      config (dict): The configuration dictionary that is read from the configuration file. 

    Raises:
      OSError: If the SPI devices cannot be opened.
    """

    self.state = self.CDCStates.INIT
    self.logger = logger
    # create the fadem and primrel objects
    # create the two SPI objects for the FADEM and PRIMREL
    self.spi_fadem = spidev.SpiDev()
    self.spi_primrel = spidev.SpiDev()
    self.spi_nullmeter = spidev.SpiDev()
    try:
      self.spi_fadem_number = int(config['fadem']['spi_number'])
      self.spi_fadem_device = int(config['fadem']['spi_device'])
      self.spi_primrel_number = int(config['primrel']['spi_number'])
      self.spi_primrel_device = int(config['primrel']['spi_device'])
      self.spi_nullmeter_number = int(config['nullmeter']['spi_number'])
      self.spi_nullmeter_device = int(config['nullmeter']['spi_device'])
    except Exception as exc:
      logger.error('Could not read SPI configuration')
      raise exc

    # open the SPI devices
    try:
      self.spi_fadem.open(self.spi_fadem_number, self.spi_fadem_device)
      self.spi_fadem.mode = 1
      self.spi_primrel.open(self.spi_primrel_number, self.spi_primrel_device)
      self.spi_primrel.mode = 1
      self.spi_nullmeter.open(self.spi_nullmeter_number,
                              self.spi_nullmeter_device)
    except Exception as exc:
      logger.error('Could not open SPI devices')
      raise OSError('Could not open SPI devices') from exc
    # set the SPI mode
    self.fadem = fadem_module.Fadem(self.spi_fadem)
    self.primrel = primrel_module.Primrel(self.spi_primrel)
    self.nullmeter = nullmeter_module.Nullmeter(self.spi_nullmeter)

    # set the stepping setting (true or false)
    self.logger.debug(f"Stepping: {config['CDC']['stepping']}")
    self.stepping = config['CDC']['stepping'].upper() == 'TRUE'

    # create the sysmon object
    self.sysmon = cdc_sysmon.SystemMonitor(self, self.logger, config)

    self.sysmon_thread = threading.Thread(target=self.sysmon.run, daemon=True)

    # properties
    properties = {
        # the general property class is used for properties that are just a variable. A derived class is used for more complex properties.
        'DEVICE.VERSION.MAIN_PROG': ncrp_property.Property('DEVICE.VERSION.MAIN_PROG', 'r', int, description='This property indicates the current version of the software', min_val=0, default=0),
        'STATUS.FAULTS': cdc_properties.StatusFaults(self),
        'STATUS.WARNINGS': cdc_properties.StatusWarnings(self),
        'CALSYS.DCM.ADC.CAL': cdc_properties.CalsysDcmAdcCal(self),
        'CALSYS.DCM.ADC.RAW': cdc_properties.CalsysDcmAdcRaw(self),
        'CALSYS.DCM.ADC.VOLT': cdc_properties.CalsysDcmAdcVolt(self),
        # did we remove this? 'CALSYS.DCM.ADC.V_REF': ncrp_property.Property('CALSYS.DCM.ADC.V_REF', 'rwp', float, unit='V', description='This expert property contains the value in Volt of the 10V voltage reference used for the calibration of the ADC.', min_val=9.0, max_val=11.0, default=10.0),
        'CALSYS.DCM.ADC.RAW_ZERO': ncrp_property.Property('CALSYS.DCM.ADC.RAW_ZERO', 'rp', int, unit='ADC counts', description='This property contains the raw value resulting from the zero calibration of the ADC', min_val=-32767, max_val=32768, default=0),
        'CALSYS.DCM.PS05.CAL': cdc_properties.CalsysDcmPs05Cal(self),
        'CALSYS.DCM.PS05.MEAS': cdc_properties.CalsysDcmPs05Meas(self),
        'CALSYS.DCM.PS12.CAL': cdc_properties.CalsysDcmPs12Cal(self),
        'CALSYS.DCM.PS12.MEAS': cdc_properties.CalsysDcmPs12Meas(self),
        'CALSYS.DCM.PS15.CALPOS': cdc_properties.CalsysDcmPs15CalPos(self),
        'CALSYS.DCM.PS15.MEASPOS': cdc_properties.CalsysDcmPs15MeasPos(self),
        'CALSYS.DCM.PS15.CALNEG': cdc_properties.CalsysDcmPs15CalNeg(self),
        'CALSYS.DCM.PS15.MEASNEG': cdc_properties.CalsysDcmPs15MeasNeg(self),
        'CALSYS.CDC.STATUS.RELAYS': cdc_properties.CalsysCdcStatusRelays(self),
        'CALSYS.CDC.OUTPUT': cdc_properties.CalsysCdcOutput(self),
        'CALSYS.CDC.I_REF': cdc_properties.CalsysCdcIRef(self),
        'CALSYS.CDC.DAC1TW.CAL': cdc_properties.CalsysCdcDac1TwCal(self),
        'CALSYS.CDC.DAC1TW.ZERO': ncrp_property.Property('CALSYS.CDC.DAC1TW.ZERO', 'rwp', int, setters=[self.primrel.set_1t_dac_zero], unit='DAC counts', description='This property contains the raw value resulting from the zero calibration of the DAC.', min_val=-32764, max_val=32765, default=0),
        'CALSYS.CDC.DAC1TW.FS': ncrp_property.Property('CALSYS.CDC.DAC1TW.FS', 'rwp', float, setters=[self.primrel.set_1t_dac_fs], unit='V', description='This property contains the value, in volt, of the DAC output that generates a 10mA current in the DAC winding.'),
        'CALSYS.CDC.NULL.KP': ncrp_property.Property('CALSYS.CDC.NULL.KP', 'rwp', float, unit='V/A', description='This property contains the value of the proportional gain of the null controller.', default=1000),
        'CALSYS.CDC.NULL.KI': ncrp_property.Property('CALSYS.CDC.NULL.KI', 'rwp', float, unit='V/(A*s)', description='This property contains the value of the integral gain of the null controller.', default=50000),
        'CALSYS.CDC.NULL.NR': ncrp_property.Property('CALSYS.CDC.NULL.NR', 'rw', int, unit='samples', description='This property contains the value of the number of samples used for the null controller.', min_val=0, max_val=100000, default=1700),
        'CALSYS.CDC.NULL.AUTO': cdc_properties.CalsysCdcNullAuto(self),
        'CALSYS.CDC.ADC.GAIN_1A': cdc_properties.CalsysCdcAdcGain1A(self),
        'CALSYS.CDC.ADC.I_MEAS': cdc_properties.CalsysCdcAdcIMeas(self),
        'CALSYS.CDC.ADC.V_MEAS': cdc_properties.CalsysCdcAdcVMeas(self),
        'CALSYS.CDC.ADC.I_NULL': cdc_properties.CalsysCdcAdcINull(self),
        'CALSYS.CDC.ADC.PBC_TEMP': cdc_properties.CalsysCdcAdcPbcTemp(self),
        'CALSYS.CDC.DAC.RAW': cdc_properties.CalsysCdcDacRaw(self),
        'CALSYS.CDC.DAC.VOLT': cdc_properties.CalsysCdcDacVolt(self),
        'CALSYS.CDC.VB_MODE': cdc_properties.CalsysCdcVBMode(self),
    }
    # call the constructor of the base class
    super().__init__(properties, logger)

    # pass the zero to the nullmeter
    self.logger.debug(
        f'CDC: Setting nullmeter zero to {self.properties["CALSYS.DCM.ADC.RAW_ZERO"]._get()}')
    self.nullmeter.set_zero(
        self.properties['CALSYS.DCM.ADC.RAW_ZERO'].value[0])
    self.logger.debug(f'CDC: Nullmeter zero set to {self.nullmeter.zero}')

    # set the 1T dac full scale and zero
    dac_1t_zero = self.properties['CALSYS.CDC.DAC1TW.ZERO'].value[0]
    dac_1t_fs = self.properties['CALSYS.CDC.DAC1TW.FS'].value[0]
    self.logger.debug(
        f'Setting 1T DAC zero to {dac_1t_zero}, FS to {dac_1t_fs}')
    self.primrel.set_1t_dac_zero(dac_1t_zero)
    self.primrel.set_1t_dac_fs(dac_1t_fs)

    self.sysmon_thread.start()

  def hardware_reset(self):
    """This function resets the hardware to a known state, by individually resetting each component."""
    self.logger.info('CDC: Resetting CDC')
    # reset the primrel
    self.logger.debug('CDC: Resetting the primary relay')
    self.primrel.reset()
    # reset the fadem
    self.logger.debug('CDC: Resetting the fadem')
    self.fadem.reset()

  # Unused code
  # def test_pol_relays(self):
  #   """This function tests the polarity relays of the CDC."""
  #   # toggle the polarity relays
  #   self.logger.debug('CDC: Testing the polarity relays')
  #   self.primrel.set_pol_relays(primrel_module.PolRelays(0x3FFF))
  #   # check that the polarity relays are in the correct position, after 10ms
  #   time.sleep(0.1)
  #   polarity_readback = self.primrel.get_pol_relays()
  #   if polarity_readback != primrel_module.PolRelays(0x3FFF):
  #     # we compare the readback with the expected value
  #     # if the readback is different, we toggle the polarity relays again
  #     self.logger.debug(
  #         'CDC: First toggling failed. Toggling the polarity relays again')
  #     self.primrel.set_pol_relays(primrel_module.PolRelays(0x3FFF))
  #     # check that the polarity relays are in the correct position, after 10ms
  #     time.sleep(0.1)
  #     polarity_readback = self.primrel.get_pol_relays()
  #     if polarity_readback != primrel_module.PolRelays(0x3FFF):
  #       # if the readback is still different, we have a problem
  #       self.logger.error(
  #           f'CDC: Polarity relays are not toggled correctly. Readback: {polarity_readback._relays}')
  #       self.state = self.CDCStates.ERROR
  #       raise RuntimeError("Polarity relays are not toggled correctly.")
  #   # reset the relays
  #   self.logger.debug('CDC: Test OK. Resetting the polarity relays')
  #   self.primrel.set_pol_relays(primrel_module.PolRelays(0x0000))

  # def test_on_off_relays(self):
  #   """This function tests the on/off relays of the primary relay."""
  #   # toggle the on/off relays
  #   self.logger.debug('CDC: Testing the on/off relays')
  #   self.primrel.set_on_off_relays(primrel_module.OnOffRelays(0x3FFF))
  #   # check that the on/off relays are in the correct position, after 10ms
  #   time.sleep(0.1)
  #   on_off_readback = self.primrel.get_on_off_relays()
  #   if on_off_readback != primrel_module.OnOffRelays(0x3FFF):
  #     # we compare the readback with the expected value
  #     # if the readback is different, we toggle the on/off relays again
  #     self.logger.debug(
  #         'CDC: First toggling failed. Toggling the on/off relays again')
  #     self.primrel.set_on_off_relays(primrel_module.OnOffRelays(0x3FFF))
  #     # check that the on/off relays are in the correct position, after 10ms
  #     time.sleep(0.1)
  #     on_off_readback = self.primrel.get_on_off_relays()
  #     if on_off_readback != primrel_module.OnOffRelays(0x3FFF):
  #       # if the readback is still different, we have a problem
  #       self.logger.error(
  #           f'CDC: On/off relays are not toggled correctly. Readback: {on_off_readback._relays}')
  #       self.state = self.CDCStates.ERROR
  #       raise RuntimeError("On/off relays are not toggled correctly.")
  #   # reset the relays
  #   self.logger.debug('CDC: Test OK. Resetting the on/off relays')
  #   self.primrel.set_on_off_relays(primrel_module.OnOffRelays(0x0000))

  def hardware_init(self):
    """This is the init method for the CDC, that moves the state machine from the INIT state to the IDLE state."""
    self.logger.info('CDC: Initializing CDC')

    self.setup_leds()
    self.set_boot_led(0)

    if self.get_state() not in [self.CDCStates.INIT, self.CDCStates.ERROR]:
      self.logger.error(
          f'CDC: Cannot initialize CDC from state {self.state}')
      raise RuntimeError(
          f'CDC: Cannot initialize CDC from state {self.state}')

    self.hardware_reset()
    time.sleep(0.1)

    # activate the primrel
    self.logger.debug('CDC: Activating the primary relay')
    # check the state of the fadem, if active, deactivate it
    if self.fadem.is_active():
      self.logger.debug('CDC: Fadem was active, deactivating')
      self.fadem.set_active(False)

    # load the defaults for the fadem
    self.fadem.set_normal_gain(39500)
    self.fadem.set_degauss_gain(65535)
    self.fadem.set_dem1_shift(625)

    # start the primrel testing procedure
    self.primrel.set_active(True)
    # check that the primrel is active
    time.sleep(0.01)
    if not self.primrel.is_active():

      self.logger.error('CDC: Primary relay module is not activated.')
      self.state = self.CDCStates.ERROR
      raise RuntimeError('Primary relay module is not activated.')

    # self.test_pol_relays()
    # self.test_on_off_relays()

    # turn off the primrel
    self.logger.debug('CDC: Turning off the primary relay module')
    self.primrel.set_active(False)
    # activate the FADEM
    self.logger.debug('CDC: Activating the FADEM')
    self.fadem.set_active(True)
    # turn on the two clamps on the fadem
    self.logger.debug('CDC: Turning on the FADEM clamps')
    self.fadem.set_clamp1(True)
    self.fadem.set_clamp2(True)
    # if vb is connected, turn on als the VB clamp
    if self.primrel.get_vb_status() != self.primrel.VBStatus.VB_DISCONNECTED:
      self.logger.debug('CDC: VB is connected, turning on the VB clamp')
      self.primrel.set_vb_clamp(True)
    time.sleep(0.1)
    # poll the FADEM status to be FADEM_NORMAL with a timeout of 5s
    start_time = time.time()
    while self.fadem.get_state() != self.fadem.States.FADEM_NORMAL:
      time.sleep(0.1)
      if time.time() - start_time > 5:
        self.logger.error('CDC: FADEM did not reach FADEM_NORMAL state.')
        self.state = self.CDCStates.ERROR
        raise RuntimeError("FADEM did not reach FADEM_NORMAL state.")

    self.logger.info('CDC: Initialized.')

    self.state = self.CDCStates.IDLE

    # turn on the boot led to confirm that the CDC is initialized and ready
    self.set_boot_led(1)

  def setup_leds(self):
    """This method sets up the leds of the CDC.
    The leds are hardcoded in this method. If the leds change, this method needs to be changed as well.    
    """

    leds = ['1021', '1022', '1023']
    for led in leds:
      gpio_dir = f'/sys/class/gpio/gpio{led}'
      if os.path.exists(gpio_dir):
        # The led is already exported, unexport it first
        self.logger.info(f'Led {led} already exported, unexporting')
        try:
          with open('/sys/class/gpio/unexport', 'w', encoding='utf-8') as f:
            f.write(led)
        except (IOError, OSError) as exc:
          self.logger.error(f'Could not unexport the led {led}. Error: {exc}')
          raise

      # Now, export the led
      try:
        with open('/sys/class/gpio/export', 'w', encoding='utf-8') as f:
          f.write(led)
      except (IOError, OSError) as exc:
        self.logger.error(f'Could not export the led {led}. Error: {exc}')
        raise

      # set the direction of the leds to out
      try:
        with open(f'{gpio_dir}/direction', 'w', encoding='utf-8') as f:
          f.write('out')
      except (IOError, OSError) as exc:
        self.logger.error(
            f'Could not set the direction of the led {led}. Error: {exc}')
        raise

  def set_boot_led(self, value):
    """This method sets the boot led of the CDC.
    """

    try:
      with open('/sys/class/gpio/gpio1021/value', 'w', encoding='utf-8') as value_f:
        value_f.write(str(value))
    except (IOError, OSError) as exc:
      self.logger.error(f'Could not set the boot led. Error: {exc}')
      raise

  def set_null_saturation_led(self, value):
    """this method is called by the main program to set the null saturation led"""
    try:
      with open('/sys/class/gpio/gpio1022/value', 'w', encoding='utf-8') as value_f:
        value_f.write(str(value))
    except (IOError, OSError) as exc:
      self.logger.error(f'Could not set the null saturation led. Error: {exc}')
      raise

  def set_faults_led(self, value):
    """this method is called by the main program to set the faults led"""
    try:
      with open('/sys/class/gpio/gpio1023/value', 'w', encoding='utf-8') as value_f:
        value_f.write(str(value))
    except (IOError, OSError) as exc:
      self.logger.error(f'Could not set the faults led. Error: {exc}')
      raise

  def set_state(self, state: CDCStates):
    "Set the internal state machine status. This function is not to be used by properties, as they already have the lock so they can set the state directly."
    with self.lock:
      self.state = state

  def get_state(self) -> CDCStates:
    "Get the internal state machine status. This function is not to be used by properties, as they already have the lock so they can get the state directly."
    with self.lock:
      return self.state

  def set(self, command):
    """Set the value of a property.
    Args:
      command (Command): The command object that contains the property name and the value to be set.
    """
    if self.state == self.CDCStates.INIT:
      self.logger.error(
          f'CDC: Cannot set property {command.ncrp_property}, CDC is in state {self.state}')

      raise errors.DevNotReady
    elif self.state == self.CDCStates.NULLING:
      self.logger.error(
          f'CDC: Cannot set property {command.ncrp_property}, CDC is in state {self.state}')
      raise errors.Busy

    return super().set(command)

  def get(self, command):
    """Get the value of a property.
    Args:
      command (Command): The command object that contains the property name.
    """

    if self.state == self.CDCStates.INIT:
      self.logger.error(
          f'CDC: Cannot get property {command.ncrp_property}, CDC is in state {self.state}')
      raise errors.DevNotReady
    elif self.state == self.CDCStates.NULLING:
      self.logger.error(
          f'CDC: Cannot get property {command.ncrp_property}, CDC is in state {self.state}')
      raise errors.Busy
    return super().get(command)
