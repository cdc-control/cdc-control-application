.. _fgc-property:

FGC Property specification
==========================

Overview and NCRP Protocol
--------------------------

The FGC properties are defined as part of the FGC protocol, the NCRP
protocol is a subset of the FGC protocol. The NCRP protocol is used to
communicate with the FGC devices. The NCRP interacts with the FGC
devices via a set of properties.

NCRP commands
-------------

The NCRP command class is defined in the ncrp.py file. Refer to :ref:`fgc-ncrp-module-label` . It contains the
following fields:

* **tag**: The tag of the command, this is used to identify the command.
* **verb**: The verb of the command, can either be g or s for get or set.
* **device**: The device that the command is for. It can be an integer or a device name. If it is 0, the gateway is selected. If no device is  specified, the gateway is selected.
* **property**: The property name that the command is for.
* **transaction_id**: The transaction id of the command.
* **user_selector**: The user selector of the command.
* **array: The array of the command. It can be**: index, to-from, to-end, to-from-step, or all if the array is empty.
* **payload**: The payload of the command. If it is a write, it is the value to write. If it is a read, it is optional and it can be options for the get.

The NCRP Command performs basic checks at the initialization to make
sure that the command is valid.

Properties
----------

All properties can be read, written or both. A set and a get method are
implemented for this. The properties are always an array, even if the
property is a single value. The array is used to be able to read and
write multiple values in the same property at once.

The init of a property takes this values:

* | **name**: The name of the property, as a string. This is converted to upper case.
* | **access**: The access of the property, can be r, w or rw.  
  | A property can also be **persistent**, in which case the access is rp, wp or rwp. A persistent property has a config value that is written in  the configs/PROPERTY.NAME file, and a default value that is written in the defaults/PROPERTY.NAME file. The default value is used only if the config value is not present. The persistent value is read at the initialization of the property, and it is written when the property is written. 
* | **val_type**: The type of the property. Since all properties are a list, the type of the values in the list is specified. If no default value is specified, val_type() is used to create a default value.
* | **length**: This defines the lenght of the property array. If not specified, the length is 1.
* | **getters**: The getters of the property. This is a list of functions that are called when the property is read.
  | The list should be as long as the property array. It can be not specified if the access is write only or if the value just needs to be read without any processing. Complex properties may need getters to access the hardware. Each getter should take exactly zero arguments and return exactly one value.
* | **setters**: The setters of the property. This is a list of functions that are called when the property is written. 
  | The list should be as long as the property array. It can be not specified if the access is read only or if the value just needs to be written without any processing. Complex properties may need setters to access the hardware. Each setter should take exactly one argument (the value to be set) and return nothing. 
  | If required, the setter functions can alter  ``self.value[index]``, to alter the value that will be stored. For persistent properties, this will also reflect in the saved value.
* | **min_val**: The minimum value of the property. 
  | If not specified, there is no minimum value. If a scalar is specified, it is used for all the values in the array. If a list is specified, it should be as long as the property array. This is checked when the property is written or when the property is read if the property is persistent. 
* | **max_val**: The maximum value of the property.
  | If not specified, there is no maximum value. If a scalar is specified, it is used for all the values in the array. If a list is specified, it should be as long as the property array. This is checked when the property is written or when the property is read if the property is  persistent.
* | **unit**: The unit of the property. This is a string. If not specified, the unit is empty. This is not used for anything as of now. 
* | **description**: The description of the property. This is a string. If not specified, the description is empty. This is not used  for anything as of now, other than to document the property. 
* | **default**: The default value of the property. If not specified, the default value is the default value of the type. If a scalar is specified, it is used for all the values in the array. If a list is specified, it should be as long as the property array. 
* | **configs_path**: The path to the configs folder. If not specified, the default is "configs/".
* | **default_path**: The path to the defaults folder. If not specified, the default is "defaults/".

The set and get functions should consider the array specified in the
command to operate.

Methods 
-------

The property will expose the following methods:

* **set(self, command)**: The set method. This is called when the property is written. It uses the payload of the command and the array of the command to write the property. 
* **get(self, command)**: The get method. This is called when the property is read. It uses the array of the command to read the property.
* **\_set(self, values, slice = slice(None))**: This is the internal set method. It is called by the set method. It can be used from the outside to skip the writability check, you just need to provide a list of values and a slice. 
* **\_get(self, slice = slice(None))**: This is the internal get method. It is called by the get method. It is just used to skip the readability check, you just need to provide a slice.

The command object is expected to be an instance of the ncrp.Command class. It is expected to have at least an array field and a payload field. 

Array 
-----

The array comes in different forms: 

* **[] or None**: empty: returns or sets all.
* **[index]**: returns or sets just the specified index.
* **[from, to]**: returns or sets from to.
* **[from, to, step]**: returns or sets from to with step.
* **[from, '']**: returns or sets from to the end ('' is the empty string) Meaning that the array can either be a list of in or a list of one int and one string. 

The payload is the value to write, it should be a list of values, one for each element in the array.
