CALSYS.CDC.NULL.KP 
==================

This property contains the proportional gain of the nulling PI controller. 

This value should be set manually and it is stored in the flash of the device.

This property is defined as a "simple" property, as such it is not implemented in a child class, but rather as an instance of the :ref:`FGC Property Module <fgc-property-module-label>`