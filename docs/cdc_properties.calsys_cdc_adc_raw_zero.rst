CALSYS.CDC.ADC.RAW_ZERO
=======================

This property contains the calibration value for the raw zero of the ADC. This is a read-only property, and it is set by triggering an ADC calibration. This property is persistent, and is stored in the flash memory of the device.

This property is defined as a "simple" property, as such it is not implemented in a child class, but rather as an instance of the :ref:`FGC Property Module <fgc-property-module-label>`