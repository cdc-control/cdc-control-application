STATUS.FAULTS
=============

.. automodule:: cdc_properties.status_faults
   :members:
   :undoc-members:
   :show-inheritance:
