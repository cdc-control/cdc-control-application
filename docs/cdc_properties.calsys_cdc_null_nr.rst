CALSYS.CDC.NULL.NR
==================


This property contains the max number of attempts the CDC will try to do the nulling.

This value should be set manually and it reset to 1700 at every power cycle (or according to what is defined in the :ref:`fgcd_cdc` module)

This property is defined as a "simple" property, as such it is not implemented in a child class, but rather as an instance of the :ref:`FGC Property Module <fgc-property-module-label>`