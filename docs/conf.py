# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import sys
import os
project = 'CDC Controller'
copyright = '2023, Valerio Nappi (valerio.nappi@cern.ch)'
author = 'Valerio Nappi (valerio.nappi@cern.ch)'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc',  # to automatically document the code
              'sphinx.ext.napoleon',  # to parse the docstrings
              'sphinx.ext.coverage',  # to collect doc coverage
              'sphinx_rtd_theme',
              ]

html_css_files = [
]

# generate path to source code. We are in source/conf.py so we need to go up one level
sys.path.insert(0, os.path.abspath('..'))

templates_path = ['_templates']
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output


html_theme = "sphinx_rtd_theme"
html_static_path = ['_static']
