DEVICE.VERSION.MAIN_PROG
========================

This property returns the main program version of the device. The version is an integer value. 

This property is defined as a "simple" property, as such it is not implemented in a child class, but rather as an instance of the :ref:`FGC Property Module <fgc-property-module-label>`