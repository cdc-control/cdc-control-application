
.. _cdc-properties:

CDC properties documentation
============================

Here is the documentation for the CDC properties. These properties are implemented according to the :ref:`fgc-property`.

Consult each property's documentation for more information, they contain the usage instructions and all possible errors that can be raised.

.. toctree::
   :maxdepth: 1
   :caption: CDC properties

   cdc_properties.calsys_cdc_adc_gain_1a
   cdc_properties.calsys_cdc_adc_i_meas
   cdc_properties.calsys_cdc_adc_i_null
   cdc_properties.calsys_cdc_adc_pbc_temp
   cdc_properties.calsys_cdc_adc_raw_zero
   cdc_properties.calsys_cdc_adc_v_meas
   cdc_properties.calsys_cdc_dac1tw_cal
   cdc_properties.calsys_cdc_dac1tw_fs
   cdc_properties.calsys_cdc_dac1tw_zero
   cdc_properties.calsys_cdc_dac_raw
   cdc_properties.calsys_cdc_dac_volt
   cdc_properties.calsys_cdc_i_ref
   cdc_properties.calsys_cdc_null_auto
   cdc_properties.calsys_cdc_null_ki
   cdc_properties.calsys_cdc_null_kp
   cdc_properties.calsys_cdc_null_nr
   cdc_properties.calsys_cdc_output
   cdc_properties.calsys_cdc_status_relays
   cdc_properties.calsys_cdc_vb_mode
   cdc_properties.calsys_dcm_adc_cal
   cdc_properties.calsys_dcm_adc_raw
   cdc_properties.calsys_dcm_adc_volt
   cdc_properties.calsys_dcm_ps05_cal
   cdc_properties.calsys_dcm_ps05_meas
   cdc_properties.calsys_dcm_ps12_cal
   cdc_properties.calsys_dcm_ps12_meas
   cdc_properties.calsys_dcm_ps15_cal_neg
   cdc_properties.calsys_dcm_ps15_cal_pos
   cdc_properties.calsys_dcm_ps15_meas_neg
   cdc_properties.calsys_dcm_ps15_meas_pos
   cdc_properties.device_version_main_prog
   cdc_properties.status_faults
   cdc_properties.status_warnings
   
   
