.. CDC Controller documentation master file, created by
   sphinx-quickstart on Mon Sep  4 11:35:38 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CDC Controller's documentation!
==========================================

.. figure:: images/cdc-min.png
   :width: 400
   :alt: CDC instrument
   :align: center

   CDC with the PBCx5 chassis


.. toctree::
   :maxdepth: 1
   
   install
   usage
   linux-system
   cdc_properties
   fgc_ncrp_parser
   fgc_property
   fgc_ncrp_parser.fgc_ncrp
   fgcd_cdc
   fgcd
   fgc_ncrp_server
   fgc_ncrp_handler
   cdc
   drivers
   fgcd_gateway_serial
   cdc_sysmon
   cdc_http
   session_logger