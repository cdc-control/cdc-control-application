.. _fgcd_gateway_serial:

FGC Gateway Serial Device Emulator
==================================

This device is a Gateway device in place because the FGC NCRP needs to refer to a gateway device in order to address the FGC devices. This emulates a "Serial" gateway device because the old version of the CDC was using a serial gateway device.

This device serves no purpose other than making the controller compatible with the FGC NCRP infrastructure already in place.

Every property does return the "Not Implemented" error code.

.. automodule:: fgcd_gateway_serial
   :members:
   :undoc-members:
   :show-inheritance:
