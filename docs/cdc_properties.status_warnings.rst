STATUS.WARNINGS
===============

.. automodule:: cdc_properties.status_warnings
   :members:
   :undoc-members:
   :show-inheritance:
