

.. _cdc_install_guide:

CDC Controller install guide  
============================
This procedure will guide you through the installation of the CDC software for a new Cora board. 

To perform this procedure, specific materials are required:

-  A Cora Z7-07S module board (Cora for short);
-  A MicroSD card, min. 4 GB;
-  A MicroSD card to USB reader, to connect to a computer;
-  A computer with at least one USB port, running windows or linux,
   capable of running Rufus or a similar software for flashing .img
   files onto SD cards;
-  A copy of the latest system image, downloadable from:
   https://gitlab.cern.ch/cdc-control/cdc-petalinux/-/jobs/artifacts/main/browse?job=create-sd-image;
-  The Rufus software, downloadable from: https://rufus.ie/.

.. _cora-board:

Cora Z7-07S module board
------------------------

.. figure:: images/cora.png
   :width: 400
   :alt: Cora board image
   :align: center
   
   MAC address sticker in green. Power and Mode jumpers in yellow.


Procedure
------------

.. _1-flashing-the-sd-card:

1. Flashing the SD card
-----------------------

The first step is to flash the SD card with the system image. To do so,
follow these steps:

1. Connect the SD card to the computer; 
2. Download Rufus from https://rufus.ie/;
3. Download the system image from: https://gitlab.cern.ch/cdc-control/cdc-petalinux/-/jobs/artifacts/main/browse?job=create-sd-image;
4. Extract the system image from the downloaded archive;
5. Open Rufus; 
6. Select the SD card as the device to flash; Make sure you select the correct device, as the SD card will be completely erased; 
7. Select the ``sdcard.img`` file as the image to flash;
8. Click on "Start" to begin the flashing process; 
9. Wait for the process to complete, and close Rufus; 
10. The process is not complete yet, follow the next steps to finish the setup. 

.. _2-setting-the-mac-address:

2. Setting the MAC address
--------------------------

To finish the setup, the MAC address of the Cora must be set. To do so,
follow these steps:

1. Find the ethernet MAC address of the Cora, written on the sticker on
   the top of the board, just on the side of the Zynq chip. It is a
   barcode with the format "001A2B3C4D5E". Check the image at :ref:`cora-board` to locate the sticker;
2. Open the smaller partition of the SD card, named "BOOT";
3. Open the file named "uEnv.txt" with a text editor;
4. Find and edit the line starting with "ethaddr=" by replacing the MAC
   address with the one of the Cora, by spacing each pair of characters
   with a colon, for example: "ethaddr=00:1A:2B:3C:4D:5E";
5. Save the file and close the text editor;

.. _3-cdc-control-application:

3. CDC Control Application
--------------------------

This step is not necessary for the configuration of the system, but
the user may want to familiarize with the settings and change them if
needed. **If not, skip to** :ref:`34-preparing-the-cora-board`.

In the second partition of the SD card there is the application for the
current calibrator.

.. _31-settings-for-the-cdc-control-application:

3.1 Settings for the CDC control application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The settings are located in the file cdc-control-application/config.ini.
The file is in INI format, and can be edited with a text editor. The
config file contains comments explaining the meaning of each setting.

The file will look like this:

.. code:: ini


   [NTP]
   server = 0.ch.pool.ntp.org

   [logger]
   log_dir = ./logs
   log_level = DEBUG

   # ... 

The user can change the settings as needed, and save the file. The
changes will be applied at the next boot of the system.

.. _32-property-values:

3.2 Property values
~~~~~~~~~~~~~~~~~~~

This step is also not necessary for the normal operation of the system,
but the user may want to change the default values of the properties.
There are two sets of files that affect the value of the properties. The
cdc-control-application/configs/ files and the
cdc-control-application/defaults/ files.

.. _321-the-cdc-control-applicationconfigs-files:

3.2.1 The cdc-control-application/configs/ files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These files are the current stored values for the "persistent"
properties. A property is called persistent if its value is stored in
the application partition, and is not reset at boot time. This files are
updated every time a persistent property is changed. The files are named
as the property, such as *CALSYS.DCM.PS05.CAL* or *CALSYS.CDC.NULL.KP*.
The files are in plain text format, so each file just contains the
value.

.. _322-the-cdc-control-applicationdefaults-files:

3.2.2 The cdc-control-application/defaults/ files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These files are used as defaults for the properties. These files are
ignored for persistent properties if the corresponding file in the
configs folder exists. If a default file exists for a persistent
property, the corresponding configs file will be created from the
default file.

The files are named as the property, such as *CALSYS.DCM.PS05.CAL* or
*CALSYS.CDC.NULL.KP*. The files are in plain text format, so each file
just contains the value.

.. _323-the-modules-ini-file:

3.2.3 The modules.ini file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This file contains the list of modules that are installed in the CDC. At the first startup, the user should check the ID of the various modules and update this file accordingly. The file is located in the application partition, in the folder cdc-control-application/. The file is in INI format, and can be edited with a text editor. The file will look like this:

.. code-block:: ini
      
   [onewire_ids]
   nullmeter = 0x9E00000CC885B028
   primrel = 0xA700000CD66AB828
   lvpsu = 0x5200000CD6A09C28
   1t_driver = 0x7700000CD69D2128
   backplane = 0x6800000CD6615128
   fadem = 0xC500000CD634D128
   modamp = 0x2B00000CD699E528
   actfilt = 0x1C00000CD66A7728
   controller = 0x2200000CD66ABF28


.. _33-log-files:

3.3 Log files
~~~~~~~~~~~~~

The log files are located in the cdc-control-application/logs/ folder. A
new file is created at every boot, named as
**session_2018-03-09_13-34-56.log** with date and time of the boot. If
no internet connection is available, the date and time defaults to
2018-03-09 13:34:56. The log files are in plain text format, and can be
opened with a text editor. The log files are rotated at every boot, so
the current log file is always the one with the most recent date and
time.

.. _34-preparing-the-cora-board:

3.4 Preparing the Cora board
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Insert now the SD card in the Cora, but do not power it yet. 

1. Check that the the MODE jumper on the Cora board is **shorting the two pins**; The MODE jumper is located at the corner opposite of the power jack. Check the image at :ref:`cora-board` to locate the jumper;

2. Put the power jumper to USB position, as shown in the image at :ref:`cora-board`;

SD usage
---------

Depending on your usage and log settings, the
cdc-control-application/logs/ folder in the application partition can be
filled with log files over time. With the default logging configuration
it would take several years for the SD to be full. If the SD is full,
the system will be able to boot and function normally, but the logs will
not be saved. To avoid this, the user can either delete the log files
manually.