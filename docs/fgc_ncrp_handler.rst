FGC NCRP message handler
=========================

.. automodule:: fgc_ncrp_handler
   :members:
   :undoc-members:
   :show-inheritance:
