User guide
=================

This user guide is intended to help you get started with the Cern DCCT Calibrator. This is not intended to be a complete manual, but it should help you get started. If you have any questions, please contact the SY-EPC-HPM group.

Network interface
-------------------

The current calibrator offers the user an interface over TCP/IP. The calibrator is configured to use DHCP by default, so it should be possible to connect it to any network and it will automatically get an IP address. 

MAC Address registration
~~~~~~~~~~~~~~~~~~~~~~~~
If you are required to register your MAC address before connecting the instrument to the network, such as if you are under the CERN network, you should do so. The MAC address of the calibrator is set during the :ref:install: procedure. 

If you're not sure of the MAC address, you can remove the SD card of the controller and look in the BOOT partition for the ``uEnv.txt`` file. The MAC address is listed in the ``ethaddr`` variable.

If you need to change the MAC address, you can do so by editing the ``uEnv.txt`` file and changing the ``ethaddr`` variable. You will need to re-install the SD card and reboot the system for the change to take effect. Changing the MAC address is not recommended unless you have a good reason to do so, as the MAC address is used to identify the calibrator on the network and it is tied to the specific Cora board in the calibrator.

Connecting to the calibrator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once the calibrator is connected to the network, you can connect to it using a telnet client. The calibrator will respond on port 1905. You can connect to it using the following command:

.. code-block:: bash

    telnet <ip address> 1905

The calibrator will wait for a valid command to be issued. The structure of the commands is specified by the FGC NCRP command protocol. 

The minimum structure of a command is:

.. code-block::

  ! <verb> <device>:<property> <value or get-options>

| The <verb> field can either be S for SET or G for GET. 
| If the verb is SET, a value must be specified after the property name. If no value is specified, an empty string is assumed.
| If the verb is GET, the value field is optional and it represents the get-options. Currently, no get option is supported by the Cern DCCT Calibrator. If no value is specified, the calibrator will return the current value of the property. 

The <device> field must be always set to 1 for the Cern DCCT Calibrator to respond, as 0 is addressing the virtual gateway (see :ref:`fgcd_gateway_serial` for more information). Omitting the device field will result in the gateway being addressed, which will respond with a "Not implemented" error.

Reference on this can be obtained from:

* FGC Command/Response Protocol (primary source): `<https://wikis.cern.ch/pages/viewpage.action?pageId=70682032#FGCCommand/ResponseProtocol-NetworkCommandResponseProtocol(NCRP)>`_.
* FGC command/response protocol specification (secondary source): `<https://gitlab.cern.ch/mcejp/fgcd2-command-parser/-/blob/master/SPEC.md#ncrp-flavor>`_.

An example of valid command is:

.. code-block:: bash

    ! g 1:DEVICE.VERSION.MAIN_PROG

The current calibrator will respond with the following:

.. code-block:: bash
  
      $ .
      <version-number>
      ;
    
Valid properties are listed at :ref:`cdc-properties`.


Web interface
-------------

The CERN DCCT Calibrator features a web interface that provides a read-only view of the current state of the calibrator. The web interface is available at the following address:

.. code-block:: bash

    http://<ip address>

Or, if a DNS name is available:

.. code-block:: bash

    http://<dns name>.cern.ch

The web interface is implemented in the :ref:`cdc_http` module.

The web interface features six tiles that display the current state of the calibrator. The tiles are:

* **Calibrator state**: This tile displays the current state of various modules of the current calibrator.

  * **CDC State**: This is the state in which the state machine is currently in. Possible states are documented in :ref:`fgcd_cdc`.
  * **Voltage Booster**: This is the state of the voltage booster. Possible states are documented in :ref:`drivers.primrel-module`.
  * **FADEM Status**: This is the status of the FADEM module. Possible states are documented in :ref:`drivers.fadem-module`.
  * **Primrel Status**: This is the status of the Primrel module. Possible states are documented in :ref:`drivers.primrel-module`.

* **Relays status**: The upper center tile shows the status of the relays. It is made of two rows, the upper for the Polarity relays and the lower for the On/Off relays. The relays are represented by a square, which is either green or white depending on the state of the relay. A green square means that the relay is closed, while a white square means that the relay is open. Due to limitations of the Primrel module, the status of the relays is not meaningful if the Primrel module is in the ``INIT_MCP`` state.

* **Module status**: This is the readback of the Dallas 1-Wire bus ID and Temperature chips on the various modules. The Dallas 1-Wire bus ID is a 16-digit hexadecimal number that uniquely identifies the module. If a module is ever replaced, the CERN DCCT Calibrator will automatically detect that a new module has been installed and it will update the Dallas 1-Wire bus ID accordingly. If more than one module is replaced at the same time, the CERN DCCT Calibrator will not be able to assign automatically the new IDs to the respective modules. In this case, the modules.ini file must be updated automatically by the user. 


SSH access
----------------

The CERN DCCT Calibrator features SSH access. The SSH access is not intended for the user to use directly, but it is intended to be used by the SY-EPC-HPM group to perform maintenance and troubleshooting operations on the calibrator.

The SSH access is available at the following address:

.. code-block:: bash

    ssh <ip address>

Or, if a DNS name is available:

.. code-block:: bash

    ssh <dns name>

The username is ``petalinux`` and the password is available as a gitlab secret environment variable. The password is found in the `cdc-petalinux project <https://gitlab.cern.ch/cdc-control/cdc-petalinux>`_, in the CI/CD settings, under the "Variables" section. The variable name is ``PETALINUX_PASSWORD``.

Serial interface
----------------

The serial interface can be used to access the Linux console, which can be used to perform maintenance and troubleshooting operations on the calibrator. 

The Cora board has a serial interface that can be used to access the Linux console. The serial interface is available on the MicroUSB connector on the Cora board. The serial interface is configured to use 115200 baud, 8 data bits, no parity, 1 stop bit, no flow control. The MicroUSB connection creates two serial ports, one for the JTAG interface and one for the Linux console. The Linux console is available on the second serial port.


The username is ``petalinux`` and the password is available as a gitlab secret environment variable. The password is found in the `cdc-petalinux project <https://gitlab.cern.ch/cdc-control/cdc-petalinux>`_, in the CI/CD settings, under the "Variables" section. The variable name is ``PETALINUX_PASSWORD``.


Petalinux distribution
----------------------

The CERN DCCT Calibrator controller runs a Petalinux distribution. 