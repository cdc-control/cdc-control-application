CALSYS.CDC.DAC1TW.ZERO
=======================

This property contains the calibration value for the raw zero of the one turn DAC. This value is used to calibrate the one turn DAC. The value is in raw counts. The value is stored in the flash memory of the device.

This value can either be set by a DAC1TW calibration or manually. 

This property is defined as a "simple" property, as such it is not implemented in a child class, but rather as an instance of the :ref:`FGC Property Module <fgc-property-module-label>`