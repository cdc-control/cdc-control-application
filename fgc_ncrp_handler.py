"""This program handles the client socket that is passed by the invoking thread, and calls the CDC class methods to send the data to the CDC."""


import fgc_ncrp_parser.ncrp_parser as parser

from fgc_ncrp_parser.fgc_ncrp import ncrp


def handler(clientsocket, cdc, gateway, logger, config):
  """This function handles the client socket that is passed by the invoking thread, and calls the CDC class methods to send the data to the CDC.

  Args:

    clientsocket: Reference to the client socket.
    cdc: Reference to the CDC class.
    gateway: Reference to the gateway class.
    logger: Reference to the logger object.
    config: Reference to the config object.

  """
  # get the data from the client
  data = clientsocket.recv(1024)

  # parse the data
  try:
    command = parser.parse(data.decode(), logger)
  except ncrp.errors.Error as exc:
    # print exception error
    logger.error(f"NCRP error occurred during parsing: {exc}")
    response = ncrp.Response('', error=exc)
    return clientsocket.send(str(response).encode())
  except Exception as exc:
    # print unknown exception error
    logger.error(f"Unexpected exception occurred during parsing: {exc}")
    # we suggest the user to check the log to see what happened
    # this may be a misuse of the LogWaiting error, but it is the best we can do
    response = ncrp.Response('', error=ncrp.errors.LogWaiting())
    return clientsocket.send(str(response).encode())

  cdc_id = int(config['CDC']['ID'] or 1)
  cdc_name = config['CDC']['name']

  logger.debug(f"Received command: {command}")
  # check the device the command is addressed to
  if command.device == 0:
    device = gateway
  elif command.device in (cdc_id, cdc_name):
    device = cdc
  else:
    # unknown device
    response = ncrp.Response(command.tag, error=ncrp.errors.UnknownDevice())
    return clientsocket.send(str(response).encode())

  # handle the command
  if command.verb.upper() == "G":
    # get the device property
    try:
      value = device.get(command)
      response = ncrp.Response(command.tag, data=value)
    except ncrp.errors.Error as exc:
      logger.error(f"NCRP error occurred during get: {exc}")
      response = ncrp.Response(command.tag, error=exc)
    except Exception as exc:
      logger.error(f"Unexpected exception occurred during get: {exc}")
      # we suggest the user to check the log to see what happened
      # this may be a misuse of the LogWaiting error, but it is the best we can do
      response = ncrp.Response(command.tag, error=ncrp.errors.LogWaiting())
  elif command.verb.upper() == "S":
    # set the device property
    try:
      resp = device.set(command)
      response = ncrp.Response(command.tag, error=resp)
    except ncrp.errors.Error as exc:
      response = ncrp.Response(command.tag, error=exc)
    except Exception as exc:
      logger.error(f"Unexpected exception occurred during get: {exc}")
      # we suggest the user to check the log to see what happened
      # this may be a misuse of the LogWaiting error, but it is the best we can do
      response = ncrp.Response(command.tag, error=ncrp.errors.LogWaiting())
  else:
    # unknown command (this should never happen)
    response = ncrp.Response(command.tag, error=ncrp.errors.UnknownCmd())

  return clientsocket.send(str(response).encode())
