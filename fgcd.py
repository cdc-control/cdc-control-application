"this is the class that implements the property model a generic FGC Device"


import threading

from fgc_ncrp_parser.fgc_ncrp import errors
import fgc_ncrp_parser.fgc_ncrp.fgc_property as ncrp_property


class Device:
  """class for the generic FGC Device. It implements the property model and the set and get methods for the properties.
  The properties are stored in a dictionary of Property objects.
  The set and get methods search for the property in the dictionary and call the correct set or get method.
  """

  def __init__(self, properties, logger=None):
    """
    Initialize the Device object.

    properties should be a dictionary of properties
    the dictionary should be in the format:
    {
      'PROPERTY.NAME': ncrp_property.Property('PROPERTY.NAME', 'r', int, description='This property indicates the current version of the software', min_val=0, default=0),
      'PROPERTY.NAME': ncrp_property.Property('PROPERTY.NAME', 'r', int, description='This property indicates the current version of the software', min_val=0, default=0),
    }


    """
    if not isinstance(properties, dict):
      raise TypeError('properties must be a dictionary')
    for key, value in properties.items():
      if not isinstance(key, str):
        raise TypeError('key must be a string')
      # each property should be a Property object or a derived class
      if not isinstance(value, ncrp_property.Property):
        raise TypeError('value must be a Property object or a derived class')
    self.properties = properties
    # logger
    self.logger = logger
    # lock
    self.lock = threading.Lock()

  def set(self, command):
    """this method searches for the property in the properties dictionary and calls the correct set method

    if the property is not found, it raises an exception
    if the property is found, it calls the set method and returns the response returned by the set method

    """

    if self.logger:
      self.logger.debug(
          f'set called with property: {command.ncrp_property} and value: {command.payload}')
    with self.lock:
      # convert the property to upper case
      fgc_property = command.ncrp_property.upper()

      # check that the property exists in the properties dictionary
      if fgc_property not in self.properties:
        if self.logger:
          self.logger.error(f'property {fgc_property} not found')
        raise errors.UnknownProperty
      response = self.properties[fgc_property].set(command)
      if self.logger:
        self.logger.debug(f'set returned: {response}')
      return response

  def get(self, command):
    """this method searches for the property in the properties dictionary and calls the correct get method

    if the property is not found, it raises an exception
    if the property is found, it calls the get method and returns the response returned by the get method

    """
    if self.logger:
      self.logger.debug(f'get called with property: {command.ncrp_property}')
    with self.lock:
      # convert the property to upper case
      fgc_property = command.ncrp_property.upper()

      # check that the property exists in the properties dictionary
      if fgc_property not in self.properties:
        if self.logger:
          self.logger.error(f'property {fgc_property} not found')
        raise errors.UnknownProperty
      response = self.properties[fgc_property].get(command)
      if self.logger:
        self.logger.debug(f'get returned: {response}')
      return response
