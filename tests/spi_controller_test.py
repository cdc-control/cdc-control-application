# pylint: disable=missing-docstring
# pylint: disable=protected-access

import mock

from drivers import spi_controller


def test_init():
  # mock instance of an already open spi
  spi = mock.Mock()
  # create an instance of the spi controller
  spi_controller.SPIController(spi)


def test_readWord():
  spi = mock.Mock()
  spi.xfer2.return_value = [0x00, 0x55, 0x00, 0xAA]
  dut = spi_controller.SPIController(spi)
  # call the function
  assert dut._readWord(0x10) == 0x55AA
  # check that the function reads the specified address
  spi.xfer2.assert_called_once_with([0x10, 0x00, 0x11, 0x00])
  # call the function to an odd address
  assert dut._readWord(0x11) == 0x55AA
  # check that the function reads the specified address
  spi.xfer2.assert_called_with([0x10, 0x00, 0x11, 0x00])


def test_writeWord():
  spi = mock.Mock()
  dut = spi_controller.SPIController(spi)
  # check that the function writes the specified address and value
  dut._writeWord(0x10, 0x55AA)
  spi.writebytes.assert_called_once_with([0x90, 0x55, 0x91, 0xAA])


def test_getBit():
  spi = mock.Mock()
  dut = spi_controller.SPIController(spi)
  dut._readWord = mock.Mock(return_value=0x55AA)
  # check that the function returns the specified bit
  for i in range(8):
    assert dut._getBit(0x10, i) == ((0x55AA >> (i+8)) & 1)

  for i in range(8):
    assert dut._getBit(0x11, i) == ((0x55AA >> i) & 1)


def test_setBit():
  spi = mock.Mock()
  dut = spi_controller.SPIController(spi)
  dut._readWord = mock.Mock(return_value=0x00)
  dut._writeWord = mock.Mock()
  # check that the function sets the specified bit, the function will be called twice, once for 0x10 and once for 0x11
  for i in range(8):
    dut._setBit(0x10, i)
    dut._writeWord.assert_called_with(0x10, 1 << (i+8))
    dut._setBit(0x11, i)
    dut._writeWord.assert_called_with(0x10, 1 << i)
