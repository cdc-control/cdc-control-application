import mock
import pytest
from drivers.ad7689 import AD7689
import drivers.ad7689_consts as consts


@pytest.fixture
def spi_mock():
  # Create a mock spi object
  spi = mock.MagicMock()
  return spi


@pytest.fixture
def ad7689(spi_mock):
  # Create a AD7689 instance using the mock spi object
  ad7689 = AD7689(spi_mock)
  return ad7689


def test_read(ad7689, spi_mock):
  # Mock the return value of spi.xfer2
  # Mock 16-bit response, corresponding to a reading of 32768
  spi_mock.xfer2.return_value = [0x80, 0x00]
  result = ad7689.read(0)  # Read from channel 0
  assert result == 32768  # Check that the reading is correct
  spi_mock.xfer2.assert_called()  # Check that spi.xfer2 was called


def test_read_all(ad7689, spi_mock):
  # Mock the return value of spi.xfer2
  # Mock 16-bit response, corresponding to a reading of 32768
  spi_mock.xfer2.return_value = [0x80, 0x00]
  result = ad7689.read_all()  # Read from all channels
  assert result == [32768]*8  # Check that the readings are correct
  # Check that spi.xfer2 was called 24 times (3 times for each channel)
  assert spi_mock.xfer2.call_count == 24


def test_read_temperature_25C(ad7689, spi_mock):
  # For a temperature of 25C, the output voltage of the sensor would be 0.283V
  # This corresponds to a count of roughly 17578 (0.283V / (4.096V / 2^16))
  # Mock 16-bit response, corresponding to a count of 17578
  spi_mock.xfer2.return_value = [0x11, 0xB0]
  result = ad7689.read_temperature()  # Read temperature
  # Check that the temperature is approximately 25C
  assert abs(result - 25) < 0.1
  assert spi_mock.xfer2.call_count == 3  # Check that spi.xfer2 was called 3 times


def test_read_temperature_50C(ad7689, spi_mock):
  # For a temperature of 50C, the output voltage of the sensor would be 308mV
  # This corresponds to a count of roughly 19351 (308mV / (4.096V / 2^16))
  # Mock 16-bit response, corresponding to a count of 19351
  spi_mock.xfer2.return_value = [0x13, 0x40]
  result = ad7689.read_temperature()  # Read temperature
  # Check that the temperature is approximately 50C
  assert abs(result - 50) < 0.1
  assert spi_mock.xfer2.call_count == 3  # Check that spi.xfer2 was called 3 times


def test_read_unsigned(ad7689, spi_mock):
  ad7689.config['INCC'] = consts.INCC_UNIPOLAR_COM
  spi_mock.xfer2.return_value = [0xFF, 0xFF]  # Max unsigned value for 16-bit
  result = ad7689.read(1)  # Read channel 1
  assert result == 65535  # Max unsigned value for 16-bit
  assert spi_mock.xfer2.call_count == 3  # Check that spi.xfer2 was called 3 times


def test_read_all_unsigned(ad7689, spi_mock):
  ad7689.config['INCC'] = consts.INCC_UNIPOLAR_COM
  spi_mock.xfer2.return_value = [0xFF, 0xFF]  # Max unsigned value for 16-bit
  result = ad7689.read_all()  # Read all channels
  # All channels should return max unsigned value for 16-bit
  assert result == [65535] * 8
  # Check that spi.xfer2 was called 24 times
  assert spi_mock.xfer2.call_count == 24
