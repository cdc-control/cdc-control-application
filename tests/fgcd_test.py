# pylint: disable=missing-docstring


import mock
import pytest

import fgcd
from fgc_ncrp_parser.fgc_ncrp import fgc_property


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_init_ok():
  properties = {
      'PROPERTY.KEY': fgc_property.Property('name', 'rw', int)
  }
  fgcd.Device(properties)


def test_properties_not_a_dictionary():
  with pytest.raises(TypeError):
    fgcd.Device('not a dictionary')


def test_key_is_a_number():
  with pytest.raises(TypeError):
    fgcd.Device({1: 'not a dictionary'})


def test_key_is_a_string():
  with pytest.raises(TypeError):
    fgcd.Device({'PROPERTY.KEY': 'not a dictionary'})


def test_set_property_not_in_properties(mock_command):
  properties = {
      'PROPERTY.KEY': fgc_property.Property('name', 'rw', int)
  }
  mock_command.ncrp_property = 'PROPERTY.KEY2'
  mock_command.payload = [1]
  device = fgcd.Device(properties)
  with pytest.raises(fgcd.errors.UnknownProperty):
    device.set(mock_command)


def test_get_property_not_in_properties(mock_command):
  properties = {
      'PROPERTY.KEY': fgc_property.Property('name', 'rw', int)
  }
  device = fgcd.Device(properties)
  mock_command.ncrp_property = 'PROPERTY.KEY2'
  with pytest.raises(fgcd.errors.UnknownProperty):
    device.get(mock_command)


def test_set_threading_lock(mock_command):
  # this test checks that the lock is locked (in the with self.lock statement) and unlocked (after the with self.lock statement)
  properties = {
      'PROPERTY.KEY': fgc_property.Property('name', 'rw', int)
  }
  mock_command.ncrp_property = 'PROPERTY.KEY'
  mock_command.payload = [1]
  device = fgcd.Device(properties)
  device.lock = mock.Mock()
  device.lock.__enter__ = mock.Mock()
  device.lock.__exit__ = mock.Mock()
  device.set(mock_command)
  device.lock.__enter__.assert_called_once()
  device.lock.__exit__.assert_called_once()


def test_get_threading_lock(mock_command):
  # this test checks that the lock is locked (in the with self.lock statement) and unlocked (after the with self.lock statement)
  properties = {
      'PROPERTY.KEY': fgc_property.Property('name', 'rw', int)
  }
  mock_command.ncrp_property = 'PROPERTY.KEY'
  device = fgcd.Device(properties)
  device.lock = mock.Mock()
  device.lock.__enter__ = mock.Mock()
  device.lock.__exit__ = mock.Mock()
  device.get(mock_command)
  device.lock.__enter__.assert_called_once()
  device.lock.__exit__.assert_called_once()
