import os

import pytest
import mock

import threading

import fgcd_cdc
import fgc_ncrp_parser.fgc_ncrp.errors as errors

import drivers.primrel as primrel_module


@pytest.fixture(name='cdc')
def fixture_cdc():
  mock_config = {
      'fadem': {
          'spi_number': "0",
          'spi_device': "0"
      },
      'primrel': {
          'spi_number': "1",
          'spi_device': "0"
      },
      'nullmeter': {
          'spi_number': "2",
          'spi_device': "0"
      },
      'sysmon': {
          'onewire_device_check': "True",
          'onewire_mem_address': "0x43c00000",
          'num_modules': "9",
          'modules_ini': "tests/modules.ini",
          'max_temp': "80",
          'min_temp': "0",
          'voltage_max_alarm': "1.1",
          'voltage_min_alarm': "0.9"
      },
      'onewire_ids': {
          'Nullmeter': "11385099912896426024",
          'PrimRel': "12033618259470891048",
          'LVPSU': "5908722766250548264",
          '1T_driver': "8574853745653653800",
          'Backplane': "7493989835080814888",
          'FADEM': "14195346080605196584",
          'Modamp': "3098476598770918696",
          'Actfilt': "2017612688198891304",
          'Controller': "2449958252426477352"
      },
      'CDC': {
          'stepping': 'True',
      },
      'debug': {
          'sysmon_logging': 'True',
          'nulling_logging': 'True',
      },
      'logger': {
          'log_dir': 'logs',
          'log_level': 'DEBUG',
          'file_log_level': 'DEBUG',
          'console_log_level': 'DEBUG',
          'max_log_files': '10',
          'max_log_size': '1048576'
      }
  }
  with mock.patch('spidev.SpiDev'), mock.patch('cdc_sysmon.devmem'):
    logger = mock.MagicMock()
    return fgcd_cdc.CDC(logger, mock_config)


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.ncrp_property = "CALSYS.DCM.ADC.CAL"
  command.payload = ['']
  command.array = []
  return command


def test_cdc_init(cdc):
  assert cdc


def test_cdc_no_spidev():
  with pytest.raises(OSError):
    fgcd_cdc.CDC(logger=mock.MagicMock(), config={
                 'fadem': {'spi_number': 0, 'spi_device': 0},
                 'primrel': {'spi_number': 1, 'spi_device': 0},
                 'nullmeter': {'spi_number': 2, 'spi_device': 0}
                 })


def test_get_state(cdc):
  assert cdc.get_state() == fgcd_cdc.CDC.CDCStates.INIT


def test_set_state(cdc):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.NULLING)
  assert cdc.get_state() == fgcd_cdc.CDC.CDCStates.NULLING


def test_set_invalid_state(cdc, mock_command):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.NULLING)
  with pytest.raises(errors.Busy):
    cdc.set(mock_command)


def test_set_valid_state(cdc, mock_command):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.IDLE)
  cdc.set(mock_command)
  assert cdc.get(mock_command) == ['CALIBRATED']


def test_get_invalid_state(cdc, mock_command):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.NULLING)
  with pytest.raises(errors.Busy):
    cdc.get(mock_command)


def test_get_valid_state(cdc, mock_command):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.IDLE)
  cdc.set(mock_command)
  assert cdc.get(mock_command) == ['CALIBRATED']


def test_get_set_multiple_threads(cdc, mock_command):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.IDLE)

  def set_get_property():
    for _ in range(100):
      cdc.set(mock_command)
      cdc.get(mock_command)

  # create a list of 10 threads to run the set_get_property function
  threads = [threading.Thread(target=set_get_property) for _ in range(10)]
  for thread in threads:
    thread.start()
  for thread in threads:
    thread.join()

  assert cdc.get(mock_command) == ['CALIBRATED']


def test_set_out_of_range_value(cdc, mock_command):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.IDLE)
  mock_command.payload = [-1]
  mock_command.ncrp_property = "CALSYS.CDC.NULL.NR"
  with pytest.raises(errors.OutOfLimits):
    cdc.set(mock_command)


def test_set_wrong_type(cdc, mock_command):
  cdc.set_state(fgcd_cdc.CDC.CDCStates.IDLE)
  mock_command.payload = ["invalid"]
  mock_command.ncrp_property = "CALSYS.CDC.NULL.NR"
  with pytest.raises(errors.BadInteger):
    cdc.set(mock_command)


@pytest.fixture(name='hw_init_cdc')
def fixture_hw_init_cdc(cdc):
  cdc.primrel = mock.MagicMock()
  cdc.fadem = mock.MagicMock()
  cdc.setup_leds = mock.MagicMock()
  cdc.set_boot_led = mock.MagicMock()
  return cdc


def test_hardware_init(hw_init_cdc):
  # Simulate the normal case where everything works as expected
  hw_init_cdc.primrel.is_active.return_value = True
  hw_init_cdc.fadem.is_active.return_value = False
  hw_init_cdc.fadem.get_state.return_value = hw_init_cdc.fadem.States.FADEM_NORMAL
  hw_init_cdc.primrel.get_pol_relays.return_value = primrel_module.PolRelays(
      0x3FFF)
  hw_init_cdc.primrel.get_on_off_relays.return_value = primrel_module.OnOffRelays(
      0x3FFF)

  hw_init_cdc.hardware_init()

  assert hw_init_cdc.state == hw_init_cdc.CDCStates.IDLE


def test_hardware_init_primrel_fail(hw_init_cdc):
  # Simulate the case where primrel fails to activate
  hw_init_cdc.primrel.is_active.return_value = False
  hw_init_cdc.fadem.is_active.return_value = False
  hw_init_cdc.fadem.get_state.return_value = hw_init_cdc.fadem.States.FADEM_NORMAL
  hw_init_cdc.primrel.get_pol_relays.return_value = primrel_module.PolRelays(
      0x3FFF)

  with pytest.raises(RuntimeError):
    hw_init_cdc.hardware_init()

  assert hw_init_cdc.state == hw_init_cdc.CDCStates.ERROR


def test_hardware_init_fadem_fail(hw_init_cdc):

  # Simulate the case where fadem fails to reach the normal state
  hw_init_cdc.primrel.is_active.return_value = True
  hw_init_cdc.fadem.is_active.return_value = False
  hw_init_cdc.fadem.get_state.return_value = hw_init_cdc.fadem.States.FADEM_ERROR
  hw_init_cdc.primrel.get_pol_relays.return_value = primrel_module.PolRelays(
      0x3FFF)

  with pytest.raises(RuntimeError):
    hw_init_cdc.hardware_init()

  assert hw_init_cdc.state == hw_init_cdc.CDCStates.ERROR


@pytest.fixture(scope="session", autouse=True)
def cleanup():
  yield
  # delete the log files
  for file in os.listdir('logs'):
    if file.endswith(".log"):
      os.remove(os.path.join('logs', file))

  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))
