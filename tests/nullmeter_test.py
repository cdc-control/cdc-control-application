import pytest
import mock
from drivers.nullmeter import Nullmeter


@pytest.fixture
def ad7689_mock():
  # Create a mock ad7689 object
  ad7689 = mock.MagicMock()
  ad7689.bits = 16
  return ad7689


@pytest.fixture
def nullmeter(ad7689_mock):
  # Create a Nullmeter instance using the mock ad7689 object
  spi_mock = mock.MagicMock()
  nm = Nullmeter(spi_mock)
  nm.adc = ad7689_mock
  return nm


def test_get_10V_cal(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 32000  # Mock reading for 10V_cal channel

  # Call the get_10V_cal() method
  result = nullmeter.get_10V_cal()

  # Assert the result
  assert result == -10.0


def test_get_I_null(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 16000  # Mock reading for I_null channel

  # Call the get_I_null() method
  result = nullmeter.get_I_null()

  # Assert the result
  assert result == -50.0


def test_set_zero(nullmeter):
  # Call the set_zero() method
  nullmeter.set_zero(100)

  # Assert that the zero value is updated
  assert nullmeter.zero == 100


def test_get_V_compliance(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 32000  # Mock reading for V_compliance channel

  # Call the get_V_compliance() method
  result = nullmeter.get_V_compliance()

  # Assert the result
  assert result == pytest.approx(-15.0, abs=0.01)


def test_get_I_monitor(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 16000  # Mock reading for I_monitor channel

  # Call the get_I_monitor() method
  result = nullmeter.get_I_monitor()

  # Assert the result
  assert result == -5.0


def test_get_PBC_temp(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 32000  # Mock reading for PBC_temp channel

  # Call the get_PBC_temp() method
  result = nullmeter.get_PBC_temp()

  # Assert the result
  assert result == 400


def test_get_zero(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 50  # Mock reading for zero channel

  # Call the get_zero() method
  result = nullmeter.get_zero()

  # Assert the result
  assert result == 50


def test_get_all_channels(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 32000

  # Call the get_all_channels() method
  result = nullmeter.get_all_channels()

  # Assert the result
  assert result == pytest.approx([-10.0, -100.0, -15.0, -10.0, 400.0, 32000], abs=0.01)


def test_get_10V_cal_with_zero(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 32000  # Mock reading for 10V_cal channel
  nullmeter.set_zero(100)

  # Call the get_10V_cal() method
  result = nullmeter.get_10V_cal()

  # Assert the result
  assert result == pytest.approx(-9.969482421875, 0.0001)


def test_get_I_null_with_zero(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 16000  # Mock reading for I_null channel
  nullmeter.set_zero(100)

  # Call the get_I_null() method
  result = nullmeter.get_I_null()

  # Assert the result
  assert result == pytest.approx(-49.69482422, 0.1)


def test_get_V_compliance_with_zero(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 32000  # Mock reading for V_compliance channel
  nullmeter.set_zero(100)

  # Call the get_V_compliance() method
  result = nullmeter.get_V_compliance()

  # Assert the result
  assert result == pytest.approx(-14.954451819638361, 0.0001)


def test_get_I_monitor_with_zero(nullmeter, ad7689_mock):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 16000  # Mock reading for I_monitor channel
  nullmeter.set_zero(100)

  # Call the get_I_monitor() method
  result = nullmeter.get_I_monitor()

  # Assert the result
  assert result == pytest.approx(-4.96, 0.01)


def test_print_channels(nullmeter, ad7689_mock, capfd):
  # Set up mock ADC readings
  ad7689_mock.read.return_value = 32000

  # Call the print_channels() method
  nullmeter.print_channels()

  # Capture the output
  captured = capfd.readouterr()

  # Assert the output
  expected_output = ("10V_cal: -10.0\n"
                     "I_null: -100.0\n"
                     "V_compliance: -15.0\n"
                     "I_monitor: -10.0\n"
                     "PBC_temp: 400.0\n"
                     "zero: 32000\n")

  assert captured.out == expected_output
