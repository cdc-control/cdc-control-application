# pylint: disable=missing-docstring
# pylint: disable=protected-access

import random
import mock

from drivers import fadem as fadem_module


def test_is_active():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _readWord function
  fadem._readWord = mock.Mock(return_value=0x00)
  # call the function
  assert not fadem.is_active()
  # check that the _readWord function was called with the correct address
  fadem._readWord.assert_called_with(0x00)

  fadem._readWord = mock.Mock(return_value=0xA000)
  # call the function
  assert fadem.is_active()
  # check that the _readWord function was called with the correct address
  fadem._readWord.assert_called_with(0x00)


def test_set_active():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _readWord function
  # generate a random byte for the return value of the _readWord function 0b0xxxxxxx
  fadem._readWord = mock.Mock(return_value=0x5500)
  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  # call the function
  fadem.set_active()
  # check that the _readWord function was called with the correct address
  fadem._readWord.assert_called_with(2)
  # check that the _writeWord function was called with the correct address and value
  fadem._writeWord.assert_called_with(2, 0xD500)
  # now turn off
  fadem._readWord = mock.Mock(return_value=0xD500)
  fadem.set_active(False)
  # check that the _writeWord function was called with the correct address and value
  fadem._writeWord.assert_called_with(2, 0x5500)


def test_get_ranges():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  for test_range in fadem_module.Fadem.Ranges:
    # mock the _readWord function
    fadem._readWord = mock.Mock(
        return_value=test_range.value << 3)
    # call the function
    assert fadem.get_range() == test_range
    # check that the _readWord function was called with the correct address
    fadem._readWord.assert_called_with(1)


def test_set_ranges():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _readWord function
  fadem._readWord = mock.Mock(return_value=0x00)
  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  for test_range in fadem_module.Fadem.Ranges:
    # test 10A range
    fadem.set_range(test_range)
    # check that the _readWord function was called with the correct address
    fadem._readWord.assert_called_with(3)
    # check that the _writeWord function was called with the correct address and value
    fadem._writeWord.assert_called_with(3, test_range.value << 3)
    # call the function


def test_set_invalid_range():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _readWord function
  fadem._readWord = mock.Mock(return_value=0x00)
  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  # test invalid range
  try:
    # disable the pylance GeneralTypeIssues warning, as it is the purpose of the test
    fadem.set_range(0xFF)  # type: ignore
  except ValueError:
    pass
  else:
    assert False


def test_get_invalid_range():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  for test_range in fadem_module.Fadem.Ranges:
    # mock the _readWord function
    fadem._readWord = mock.Mock(return_value=test_range.value << 3)
    # call the function
    assert fadem.get_range() == test_range
    # check that the _readWord function was called with the correct address
    fadem._readWord.assert_called_with(1)


def test_get_normal_gain():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _readWord function
  fadem._readWord = mock.Mock(side_effect=[0x55AA])
  # call the function
  assert fadem.get_normal_gain() == (0x55 << 8) | 0xAA


def test_set_normal_gain():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  # generate a random gain value
  gain = 0x55AA
  # call the function
  fadem.set_normal_gain(gain)
  # assert the arguments for the first call
  fadem._writeWord.assert_any_call(4, gain)


def test_get_state():
    # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  fadem._readWord = mock.Mock(return_value=0x00)
  assert fadem.get_state() == fadem.States.FADEM_INIT
  fadem._readWord = mock.Mock(return_value=0x01)
  assert fadem.get_state() == fadem.States.FADEM_IDLE
  fadem._readWord = mock.Mock(return_value=0x02)
  assert fadem.get_state() == fadem.States.FADEM_DEGAUSS
  fadem._readWord = mock.Mock(return_value=0x03)
  assert fadem.get_state() == fadem.States.FADEM_NORMAL
  fadem._readWord = mock.Mock(return_value=0x04)
  assert fadem.get_state() == fadem.States.FADEM_RAMPDOWN


def test_get_clamp1():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _readWord function
  fadem._readWord = mock.Mock(side_effect=[0x00])
  # call the function
  assert not fadem.get_clamp1()
  # check that the _readWord function was called with the correct address
  fadem._readWord.assert_called_with(0)
  # mock the _readWord function. This is hardcoded to actually confirm that it reads the right bit, as in the offset is correct
  fadem._readWord = mock.Mock(side_effect=[0x40])
  # call the function
  assert fadem.get_clamp1()


def test_get_clamp2():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _readWord function
  fadem._readWord = mock.Mock(side_effect=[0x00])
  # call the function
  assert not fadem.get_clamp2()
  # check that the _readWord function was called with the correct address
  fadem._readWord.assert_called_with(0)
  # mock the _readWord function. This is hardcoded to actually confirm that it reads the right bit, as in the offset is correct
  fadem._readWord = mock.Mock(side_effect=[0x80])
  # call the function
  assert fadem.get_clamp2()


def test_set_clamp1():
  # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the read to return 0
  fadem._readWord = mock.Mock(return_value=0xAA)
  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  # call the function
  fadem.set_clamp1(True)
  fadem._readWord.assert_called_with(2)
  # assert the arguments for the first call
  fadem._writeWord.assert_called_with(2, (0xEA))

  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  fadem._readWord = mock.Mock(return_value=0x55)
  # call the function
  fadem.set_clamp1(False)
  # assert the arguments for the first call
  fadem._writeWord.assert_called_with(2, (0x15))


def test_set_clamp2():
    # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the read to return 0
  fadem._readWord = mock.Mock(return_value=0x55)
  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  # call the function
  fadem.set_clamp2(True)
  # assert the arguments for the first call
  fadem._readWord.assert_called_with(2)
  fadem._writeWord.assert_called_with(2, (0xD5))

  # mock the _writeWord function
  fadem._writeWord = mock.Mock()
  fadem._readWord = mock.Mock(return_value=0xAA)
  # call the function
  fadem.set_clamp2(False)
  # assert the arguments for the first call
  fadem._writeWord.assert_called_with(2, (0x2A))


def test_fadem_functions():
    # create a mock spi object
  spi = mock.Mock()
  # create an instance of Fadem
  fadem = fadem_module.Fadem(spi)
  # mock the _writeWord and _readWord functions
  fadem._writeWord = mock.Mock()
  fadem._readWord = mock.Mock()
  # generate some random values
  gain = 0x55AA
  shift = 0x1234
  value = 0x5678
  # call the functions
  fadem.set_normal_gain(gain)
  fadem.set_degauss_gain(gain)
  fadem.set_dem1_shift(shift)
  fadem.set_dem2_shift(shift)
  fadem._set_dac_ch1_out(value)
  fadem._set_dac_ch2_out(value)
# create a list of functions to test
  read_funct = [
      fadem.get_normal_gain,
      fadem.get_degauss_gain,
      fadem.get_dem1_shift,
      fadem.get_dem2_shift,
      fadem._get_dac_ch1_out,
      fadem._get_dac_ch2_out,
      fadem.get_status_reg,
      fadem.get_cmd_reg,
  ]
  # set the side effect to a random value and test each function in the list
  for func in read_funct:
    random_value = random.randint(0, 0xFFFF)
    fadem._readWord.side_effect = [random_value]
    assert func() == random_value

  # assert the arguments for the set functions
  fadem._writeWord.assert_has_calls([
      mock.call(4, gain),
      mock.call(6, gain),
      mock.call(8, shift),
      mock.call(10, shift),
      mock.call(12, value),
      mock.call(14, value)
  ])
