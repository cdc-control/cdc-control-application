import pytest
from unittest.mock import MagicMock, patch
from cdc_http import highlight_log, generate_status_class, generate_relay_columns, generate_module_info, generate_html, RequestHandler


def test_highlight_log():
  assert highlight_log("This is a DEBUG log") == "log-debug"
  assert highlight_log("This is a INFO log") == "log-info"
  assert highlight_log("This is a WARNING log") == "log-warning"
  assert highlight_log("This is a ERROR log") == "log-error"
  assert highlight_log("This is a log without a specific log level") == ""


def test_generate_status_class():
  classes = {
      "class1": ["status1", "status2"],
      "class2": ["status3", "status4"]
  }
  assert generate_status_class("status1", classes) == "class1"
  assert generate_status_class("status3", classes) == "class2"
  assert generate_status_class("status5", classes) == ""


def test_generate_relay_columns():
  assert generate_relay_columns(["relay1", "relay2"], [
                                "relay2"]) == '<th class="">relay1</th><th class="active">relay2</th>'
  assert generate_relay_columns(["relay1", "relay2"], [
                                "relay1", "relay2"]) == '<th class="active">relay1</th><th class="active">relay2</th>'


def test_generate_module_info():
  assert generate_module_info(
      {"Name": "Module1", "Temperature": 25}) == "<li>Module1 - 25°C</li>"
  assert generate_module_info(
      {"ID": "0x9E00000CC885B028", "Temperature": 30}) == "<li>0x9E00000CC885B028 - 30°C</li>"
  assert generate_module_info({"Temperature": 35}) == "<li> - 35°C</li>"


@pytest.fixture
def mock_cdc_data():
  return {
      "cdc_state": "INIT",
      "vb_status": "VB_OK",
      "fadem_status": "FADEM_NORMAL",
      "primrel_status": "ACTIVE",
      "log": ["DEBUG: log1", "INFO: log2", "ERROR: log3"],
      "module_status": [
          {"Name": "Module1", "Temperature": 25},
          {"ID": "Module2", "Temperature": 30}
      ],
      "pol_relays": ["relay1", "relay2"],
      "onoff_relays": ["relay3", "relay4"],
      "primary_relays": ["relay1", "relay2", "relay3", "relay4"],
      "output_current": 0,
      "range": "RANGE_1A",
      "v_monitor": {
          "5V": 5.0,
          "12V": 12.0,
          "+15V": 15.0,
          "-15V": -15.0,
      }
  }


def test_generate_html(mock_cdc_data):
  html = generate_html(mock_cdc_data)
  print(html)
  assert "CDC Debug Screen" in html
  assert "CDC State <div class=\"status-box error\">INIT</div>" in html
  assert "<li class=\"log-debug\">DEBUG: log1</li>" in html
  assert "<li>Module1 - 25°C</li>" in html
  assert "<th class=\"\">relay1</th>" in html
  assert "<th class=\"active\">relay3</th>" in html
  assert "{:.8f}".format(mock_cdc_data['output_current']) in html
  assert "RANGE_1A" in html
  assert "5.00V" in html
  assert "12.00V" in html
  assert "15.00V" in html
  assert "-15.00V" in html
