# pylint: disable=protected-access
# pylint: disable=missing-docstring


import pytest

from drivers.primrel import Relays
from drivers.primrel import OnOffRelays
from drivers.primrel import PolRelays


def test_init_relays():
  relays = Relays()
  # assert that the set is empty
  assert relays.get_set


def test_init_with_16bit():
  relays = Relays(0xAA55)
  assert relays._relays == {"2048T", "512T", "128T", "16T", "4T", "1T", "1TDAC"}
  relays = Relays(0x55AA)
  assert relays._relays == {"1024T", "256T","64T", "32T", "8T", "2T", "1TCAL"}


def test_init_with_two_8bit():
  relays = Relays(0xAA, 0x55)
  assert relays._relays == {"2048T", "512T", "128T", "16T", "4T", "1T", "1TDAC"}
  relays = Relays(0x55, 0xAA)
  assert relays._relays == {"1024T", "256T","64T", "32T", "8T", "2T", "1TCAL"}


def test_bad_init():
  with pytest.raises(ValueError):
    Relays(lsb=0xAA)


def test_get_set():
  relays = Relays(0xAA55)
  assert relays.get_set() == {"2048T", "512T", "128T", "16T", "4T", "1T", "1TDAC"}


def test_get_bytes():
  relays = Relays(0xAA55)
  assert relays.get_bytes() == (0x2A, 0x55)


def test_get_16bit():
  relays = Relays(0xAA, 0x55)
  assert relays.get_16bit_integer() == 0x2A55


def test_equal():
  relays1 = Relays(0xAA55)
  relays2 = Relays(0xAA, 0x55)
  assert relays1 == relays2


def test_equal_false():
  relays1 = Relays(0xAA55)
  relays2 = Relays(0xAA, 0x54)
  # assert ignoring pylint uneeded not
  assert not relays1 == relays2  # pylint: disable=unneeded-not


def test_different_types():
  relays1 = Relays(0xAA55)
  relays2 = {
      "2048T": True,
      "1024T": False,
      "512T": True,
      "256T": False,
      "128T": True,
      "64T": False,
      "32T": False,
      "16T": True,
      "8T": False,
      "4T": True,
      "2T": False,
      "1T": True,
      "1TCAL": False,
      "1TDAC": True
  }
  # assert ignoring pylint uneeded not
  assert not relays1 == relays2  # pylint: disable=unneeded-not


def test_not_equal():
  relays1 = Relays(0x2A55)
  relays2 = Relays(0x25AA)
  assert relays1 != relays2


def test_not_equal_false():
  relays1 = Relays(0xAA55)
  relays2 = Relays(0xAA, 0x55)
  assert not relays1 != relays2  # pylint: disable=unneeded-not

def test_onoff_string_names():
  relays = OnOffRelays(0xAA55)
  assert relays.get_active_relay_names() == "ON2048T ON512T ON128T ON16T ON4T ON1T ON1TDAC"
  relays = OnOffRelays(0x55AA)
  assert relays.get_active_relay_names() == "ON1024T ON256T ON64T ON32T ON8T ON2T ON1TCAL"

def test_pol_string_names():
  relays = PolRelays(0xAA55)
  assert relays.get_active_relay_names() == "INV2048T INV512T INV128T INV16T INV4T INV1T INV1TDAC"
  relays = PolRelays(0x55AA)
  assert relays.get_active_relay_names() == "INV1024T INV256T INV64T INV32T INV8T INV2T INV1TCAL"