import pytest
import mock
from fgc_ncrp_parser.fgc_ncrp import errors
from fgc_ncrp_parser.fgc_ncrp.fgc_property import NotImplementedProperty
import fgcd_gateway_serial


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command

# Test 1: Test initialization of fgcd_gateway_serial.GatewaySerial object


def test_gateway_serial_initialization():
  gateway = fgcd_gateway_serial.GatewaySerial()
  assert isinstance(gateway.properties['CLIENT'], NotImplementedProperty)

# Test 2: Test the set and get methods for valid property names


def test_set_get_valid_property(mock_command):
  gateway = fgcd_gateway_serial.GatewaySerial()
  mock_command.ncrp_property = 'CLIENT.TOKEN'
  mock_command.payload = [12345]

  # Call set method
  with pytest.raises(errors.NcrpNotImplemented):
    gateway.set(mock_command)

  # Call get method
  with pytest.raises(errors.NcrpNotImplemented):
    gateway.get(mock_command)


# Test 3: Test the set and get methods for invalid property names


def test_set_get_invalid_property(mock_command):
  gateway = fgcd_gateway_serial.GatewaySerial()
  mock_command.ncrp_property = 'INVALID_PROPERTY_NAME'
  mock_command.payload = [12345]

  with pytest.raises(errors.UnknownProperty):
    gateway.set(mock_command)

  with pytest.raises(errors.UnknownProperty):
    gateway.get(mock_command)

# Test 4: Test for the correct error handling when an invalid property type is set


def test_set_invalid_property_type(mock_command):
  gateway = fgcd_gateway_serial.GatewaySerial()
  mock_command.ncrp_property = 'CLIENT.TOKEN'
  # Assuming the property should receive an integer value
  mock_command.payload = 'invalid_value_type'

  # Replace SomeSpecificError with the actual error your set method raises
  with pytest.raises(errors.NcrpNotImplemented):
    gateway.set(mock_command)

# Example: Run the tests using the command: `pytest test_gateway_serial.py`
