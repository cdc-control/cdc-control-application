# pylint: disable=protected-access
# pylint: disable=missing-docstring


import mock

from drivers import primrel


def test_init():
  spi = mock.Mock()
  primrel.Primrel(spi)


def test_is_active():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x00)
  assert not dut.is_active()
  dut._readWord.assert_called_once_with(0x00)
  dut._readWord = mock.Mock(return_value=1 << (2+8))
  assert dut.is_active()
  dut._readWord.assert_called_once_with(0x00)


def test_set_active():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)

  dut._readWord = mock.Mock(return_value=0x00)
  dut._writeWord = mock.Mock()
  dut.set_active(False)
  dut._readWord.assert_called_once_with(0x02)
  dut._writeWord.assert_called_once_with(0x02, 0x00)

  dut._writeWord = mock.Mock()
  dut.set_active(True)
  dut._writeWord.assert_called_once_with(0x02, 1 << 15)


def test_get_state():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x50)
  assert dut.get_state() == primrel.States.INIT_MCP
  dut._readWord.assert_called_once_with(0x01)
  dut._readWord = mock.Mock(return_value=0x51)
  assert dut.get_state() == primrel.States.CFG_IODIR
  dut._readWord = mock.Mock(return_value=0x52)
  assert dut.get_state() == primrel.States.CFG_GPPU
  dut._readWord = mock.Mock(return_value=0x53)
  assert dut.get_state() == primrel.States.ACTIVE
  dut._readWord = mock.Mock(return_value=0x54)
  assert dut.get_state() == primrel.States.SET_GPIO
  dut._readWord = mock.Mock(return_value=0x55)
  assert dut.get_state() == primrel.States.GET_GPIO


def test_set_dac_ch1():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._writeWord = mock.Mock()
  dut._set_dac_ch1(0x1234)
  # expect two calls, to address 20 and 21 with value 0x1234
  dut._writeWord.assert_called_once_with(20, 0x1234)


def test_set_dac_ch2():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._writeWord = mock.Mock()
  dut._set_dac_ch2(0x1234)
  # expect two calls, to address 22 and 23 with values 0x12 and 0x34
  dut._writeWord.assert_called_once_with(22, 0x1234)


def test_get_dac_ch1():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x1234)
  assert dut._get_dac_ch1() == 0x1234
  # expect one call, to address 20
  dut._readWord.assert_called_once_with(0x14)


def test_get_dac_ch2():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x5678)
  assert dut._get_dac_ch2() == 0x5678
  # expect one call, to address 22
  dut._readWord.assert_called_once_with(0x16)


def test_get_rel_ext_status():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)
  assert dut.get_rel_ext_status()
  dut._getBit.assert_called_once_with(11, 5)


def test_set_rel_ext_status():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._setBit = mock.Mock()
  dut.set_rel_ext_status(True)
  dut._setBit.assert_called_once_with(11, 5, True)
  dut._setBit = mock.Mock()
  dut.set_rel_ext_status(False)
  dut._setBit.assert_called_once_with(11, 5, False)


def test_get_vb_clamp():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)
  assert dut.get_vb_clamp()
  dut._getBit.assert_called_once_with(11, 4)


def test_set_vb_clamp():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._setBit = mock.Mock()
  dut.set_vb_clamp(True)
  dut._setBit.assert_called_once_with(11, 4, False)
  dut._setBit = mock.Mock()
  dut.set_vb_clamp(False)
  dut._setBit.assert_called_once_with(11, 4, True)


def test_set_secondary_relays():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x0F)
  dut._writeWord = mock.Mock()
  dut._set_secondary_relays(primrel.SecondaryRelays.OFF)
  dut._writeWord.assert_called_once_with(11, 0x00)

  dut._set_secondary_relays(primrel.SecondaryRelays.REL_SEC_4T)
  dut._writeWord.assert_called_with(11, 0b1000)

  dut._set_secondary_relays(primrel.SecondaryRelays.REL_SEC_8T)
  dut._writeWord.assert_called_with(11, 0b0100)

  dut._set_secondary_relays(primrel.SecondaryRelays.REL_SEC_16T)
  dut._writeWord.assert_called_with(11, 0b0010)

  dut._set_secondary_relays(primrel.SecondaryRelays.REL_SEC_40T)
  dut._writeWord.assert_called_with(11, 0b0001)


def test_get_secondary_relays():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x0F << 3)
  assert dut.get_secondary_relays() == primrel.SecondaryRelays.OFF
  dut._readWord.assert_called_once_with(17)  # this is in the EXT IN register

  dut._readWord = mock.Mock(return_value=0x07 << 3)
  assert dut.get_secondary_relays() == primrel.SecondaryRelays.REL_SEC_4T

  dut._readWord = mock.Mock(return_value=0x0B << 3)
  assert dut.get_secondary_relays() == primrel.SecondaryRelays.REL_SEC_8T

  dut._readWord = mock.Mock(return_value=0x0D << 3)
  assert dut.get_secondary_relays() == primrel.SecondaryRelays.REL_SEC_16T

  dut._readWord = mock.Mock(return_value=0x0E << 3)
  assert dut.get_secondary_relays() == primrel.SecondaryRelays.REL_SEC_40T


def test_get_vb_status():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x00)
  assert dut.get_vb_status() == dut.VBStatus.VB_OK
  dut._readWord.assert_called_once_with(17)  # this is in the EXT IN register

  dut._readWord = mock.Mock(return_value=0x02)
  assert dut.get_vb_status() == dut.VBStatus.VB_OFF

  dut._readWord = mock.Mock(return_value=0x04)
  assert dut.get_vb_status() == dut.VBStatus.VB_TEMP_FAULT

  dut._readWord = mock.Mock(return_value=0x06)
  assert dut.get_vb_status() == dut.VBStatus.VB_DISCONNECTED


def test_get_mod_amp_status():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert dut.get_mod_amp_status()
  dut._getBit.assert_called_once_with(16, 6)

  dut._getBit = mock.Mock(return_value=False)
  assert not dut.get_mod_amp_status()


def test_get_pbc_x5_status():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert dut.get_pbc_x5_status()
  dut._getBit.assert_called_once_with(16, 5)

  dut._getBit = mock.Mock(return_value=False)
  assert not dut.get_pbc_x5_status()


def test_get_pbc_temp2():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert dut.get_pbc_temp2()
  dut._getBit.assert_called_once_with(16, 4)

  dut._getBit = mock.Mock(return_value=False)
  assert not dut.get_pbc_temp2()


def test_get_pbc_temp1():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert dut.get_pbc_temp1()
  dut._getBit.assert_called_once_with(16, 3)

  dut._getBit = mock.Mock(return_value=False)
  assert not dut.get_pbc_temp1()


def test_get_pbc_compliance():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert not dut.get_pbc_compliance()
  dut._getBit.assert_called_once_with(16, 2)

  dut._getBit = mock.Mock(return_value=False)
  assert dut.get_pbc_compliance()


def test_get_pbc_cal():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert not dut.get_pbc_cal()
  dut._getBit.assert_called_once_with(16, 1)

  dut._getBit = mock.Mock(return_value=False)
  assert dut.get_pbc_cal()


def test_get_pbc_charge():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert not dut.get_pbc_charge()
  dut._getBit.assert_called_once_with(16, 0)

  dut._getBit = mock.Mock(return_value=False)
  assert dut.get_pbc_charge()


def test_get_pbc_power_on():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)

  assert not dut.get_pbc_power_on()
  dut._getBit.assert_called_once_with(17, 7)

  dut._getBit = mock.Mock(return_value=False)
  assert dut.get_pbc_power_on()


def test_get_pwr_amp_status():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._getBit = mock.Mock(return_value=True)
  assert dut.get_pwr_amp_status()
  dut._getBit.assert_called_once_with(17, 0)

  dut._getBit = mock.Mock(return_value=False)
  assert not dut.get_pwr_amp_status()


def test_set_pol_relays():
  relays = primrel.Relays(0x2A55)
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._writeWord = mock.Mock()
  # mock also the readback get_pol_relays
  dut.get_pol_relays = mock.Mock(return_value=relays)

  dut.set_pol_relays(relays)
  dut._writeWord.assert_called_once_with(4, 0x2A55)

  relays = primrel.Relays(0x0000)
  dut.set_pol_relays(relays)
  dut._writeWord.assert_called_with(4, 0x0000)


def test_get_pol_relays():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x2A55)
  assert dut.get_pol_relays()._relays == primrel.PolRelays(0x2A55)._relays
  dut._readWord.assert_called_once_with(12)

  dut._readWord = mock.Mock(return_value=0x0000)
  assert dut.get_pol_relays()._relays == primrel.PolRelays(0x0000)._relays
  dut._readWord.assert_called_with(12)


def test_set_on_off_relays():
  relays = primrel.Relays(0x2A55)
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._writeWord = mock.Mock()
  # mock also the readback get_on_off_relays
  dut.get_on_off_relays = mock.Mock(return_value=relays)

  dut.set_on_off_relays(relays)
  dut._writeWord.assert_called_once_with(8, 0x2A55)

  relays = primrel.Relays(0x0000)
  dut.set_on_off_relays(relays)
  dut._writeWord.assert_called_with(8, 0x0000)


def test_get_on_off_relays():
  spi = mock.Mock()

  dut = primrel.Primrel(spi)
  dut._readWord = mock.Mock(return_value=0x2A55)
  assert dut.get_on_off_relays()._relays == primrel.OnOffRelays(0x15AA)._relays
  dut._readWord.assert_called_once_with(14)

  dut._readWord = mock.Mock(return_value=0x3FFF)
  assert dut.get_on_off_relays()._relays == primrel.OnOffRelays(0x0000)._relays
  dut._readWord.assert_called_with(14)
