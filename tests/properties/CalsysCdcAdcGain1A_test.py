# pylint: disable=missing-docstring

import os
import mock

import pytest

from cdc_properties import CalsysCdcAdcGain1A
from fgc_ncrp_parser.fgc_ncrp import errors


@pytest.fixture(name='cdc')
def fixture_cdc():
  # mock the CDC class that has the state, I_REF, and nullmeter properties
    # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))
  cdc_mock = mock.Mock()
  cdc_mock.state = cdc_mock.CDCStates.ACTIVE
  cdc_mock.properties = {
      'CALSYS.CDC.I_REF': mock.Mock(value=[1.0])
  }
  cdc_mock.nullmeter = mock.Mock(get_I_monitor=mock.Mock(return_value=1.0))
  yield cdc_mock

  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = ['']
  command.array = []
  return command


def test_set(cdc, mock_command):
  gain_property = CalsysCdcAdcGain1A(cdc)
  gain_property.set(mock_command)

  mock_command.payload = []
  mock_command.array = []
  # Since get_I_monitor returned 1.0, the gain value should be set to 1.0
  assert gain_property.get(mock_command) == ['CALIBRATED']


def test_set_bad_state(cdc, mock_command):
  cdc.state = cdc.CDCStates.NULLING
  gain_property = CalsysCdcAdcGain1A(cdc)

  with pytest.raises(errors.BadState):
    gain_property.set(mock_command)


def test_set_bad_iref(cdc, mock_command):
  cdc.properties['CALSYS.CDC.I_REF'].value = [2.0]
  gain_property = CalsysCdcAdcGain1A(cdc)

  with pytest.raises(errors.BadState):
    gain_property.set(mock_command)


def test_get(cdc, mock_command):
  gain_property = CalsysCdcAdcGain1A(cdc)
  gain_property.set(mock_command)

  assert gain_property.get(mock_command) == ["CALIBRATED"]


def test_get_uncalibrated(cdc, mock_command):
  gain_property = CalsysCdcAdcGain1A(cdc)

  assert gain_property.get(mock_command) == ["UNCALIBRATED"]
