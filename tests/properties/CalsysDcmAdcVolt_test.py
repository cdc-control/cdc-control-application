import pytest
from cdc_properties import CalsysDcmAdcVolt
from drivers.nullmeter import Nullmeter
import fgc_ncrp_parser.fgc_ncrp.errors as errors
import mock


@pytest.fixture(name='mock_cdc')
def fixture_mock_cdc():
  mock_cdc = mock.Mock()
  mock_cdc.logger = mock.Mock()
  mock_cdc.nullmeter = Nullmeter(mock.Mock())
  mock_cdc.nullmeter.get_10V_cal = mock.Mock(return_value=4.09)
  mock_cdc.nullmeter.get_I_null = mock.Mock(return_value=2.045)
  mock_cdc.nullmeter.get_V_compliance = mock.Mock(return_value=4.096)
  mock_cdc.nullmeter.get_I_monitor = mock.Mock(return_value=2.45)
  mock_cdc.nullmeter.get_PBC_temp = mock.Mock(return_value=4.096)
  mock_cdc.nullmeter.get_zero = mock.Mock(return_value=2.048)
  mock_cdc.nullmeter.read_raw_channel = mock.Mock(
      side_effect=[100, 100, 100, 100, 100, 100])
  return mock_cdc


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_get_without_array(mock_cdc, mock_command):
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [0.00625, 0.00625, 0.00625, 0.00625, 0.00625, 0.00625]


def test_get_with_single_channel(mock_cdc, mock_command):
  mock_command.array = [1]
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [0.00625]


def test_get_with_start_channel(mock_cdc, mock_command):
  mock_command.array = [1, '']
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [0.00625, 0.00625, 0.00625, 0.00625, 0.00625]


def test_get_with_range_of_channels(mock_cdc, mock_command):
  mock_command.array = [1, 3]
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [0.00625, 0.00625, 0.00625]


def test_get_with_from_to_step(mock_cdc, mock_command):
  mock_command.array = [1, 3, 2]
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [0.00625, 0.00625]


def test_get_with_unimplemented_functionality(mock_cdc, mock_command):
  mock_command.array = [1, 2, 3, 4]
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  with pytest.raises(errors.BadArrayLen):
    dut_property.get(mock_command)


def test_get_with_illegal_channel(mock_cdc, mock_command):
  mock_command.array = [10]
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  with pytest.raises(errors.BadArrayIdx):
    dut_property.get(mock_command)


def test_last_before_first_channel(mock_cdc, mock_command):
  mock_command.array = [3, 1]
  dut_property = CalsysDcmAdcVolt(mock_cdc)
  with pytest.raises(errors.BadArrayIdx):
    dut_property.get(mock_command)
