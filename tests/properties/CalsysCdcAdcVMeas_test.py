
import pytest
from cdc_properties import CalsysCdcAdcVMeas
import mock
import os


@pytest.fixture(name='cdc')
def fixture_cdc():
  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))

  # Mock the CDC class that has the nullmeter properties
  cdc = mock.Mock()
  cdc.nullmeter = mock.Mock(get_V_compliance=mock.Mock(return_value=1.0))
  yield cdc

  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_get_voltage(cdc, mock_command):
  voltage_property = CalsysCdcAdcVMeas(cdc)

  # The nullmeter.get_V_compliance() method returns 1.0,
  # so the expected result is 1.0
  assert voltage_property.get(mock_command) == [1.0]
