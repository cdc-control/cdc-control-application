import pytest
from cdc_properties import CalsysCdcAdcIMeas
import mock
import os


@pytest.fixture(name='cdc')
def fixture_cdc():
  # Mock the CDC class that has the state, I_REF, and nullmeter properties
  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))

  cdc = mock.MagicMock()
  cdc.state = cdc.CDCStates.ACTIVE
  cdc.properties = {
      'CALSYS.CDC.I_REF': mock.Mock(get=mock.Mock(return_value=1.0)),
      'CALSYS.CDC.ADC.GAIN_1A': mock.Mock(_get=mock.Mock(return_value=0))
  }
  cdc.nullmeter = mock.Mock(get_I_monitor=mock.Mock(return_value=1.0))
  yield cdc

  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_get_unscaled(cdc, mock_command):
  gain_property = CalsysCdcAdcIMeas(cdc)
  # mock the value[0] of CALSYS.CDC.ADC.GAIN_1A to be 0
  cdc.properties['CALSYS.CDC.ADC.GAIN_1A'].value = [0]

  # The nullmeter.get_I_monitor() method returns 1.0, and the calibration is 0,
  # so the expected result is 1.0
  assert gain_property.get(mock_command) == [1.0]


def test_get_scaled(cdc, mock_command):
  gain_property = CalsysCdcAdcIMeas(cdc)
  # mock the value[0] of CALSYS.CDC.ADC.GAIN_1A to be 0
  cdc.properties['CALSYS.CDC.ADC.GAIN_1A'].value = [2.0]

  # The nullmeter.get_I_monitor() method returns 1.0, and the calibration is 2.0,
  # so the expected result is 2.0
  assert gain_property.get(mock_command) == [2.0]
