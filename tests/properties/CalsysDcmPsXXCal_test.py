import pytest
from unittest import mock
import os

import fgc_ncrp_parser.fgc_ncrp.errors as errors

from cdc_properties import (
    CalsysDcmPs05Cal,
    CalsysDcmPs12Cal,
    CalsysDcmPs15CalPos,
    CalsysDcmPs15CalNeg
)


@pytest.fixture(name='cdc_mock')
def fixture_cdc():
  return mock.Mock()


@pytest.fixture(name='calsys_dcm_ps05_cal')
def fixture_calsys_dcm_ps05_cal(cdc_mock):
  return CalsysDcmPs05Cal(cdc_mock)


@pytest.fixture(name='calsys_dcm_ps12_cal')
def fixture_calsys_dcm_ps12_cal(cdc_mock):
  return CalsysDcmPs12Cal(cdc_mock)


@pytest.fixture(name='calsys_dcm_ps15_cal_pos')
def fixture_calsys_dcm_ps15_cal_pos(cdc_mock):
  return CalsysDcmPs15CalPos(cdc_mock)


@pytest.fixture(name='calsys_dcm_ps15_cal_neg')
def fixture_calsys_dcm_ps15_cal_neg(cdc_mock):
  return CalsysDcmPs15CalNeg(cdc_mock)


@pytest.fixture(autouse=True)
def cleanup_configs_directory():
  yield
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))


@pytest.fixture(name='mock_sysmon_read_raw_voltage')
def fixture_mock_sysmon_read_raw_voltage(cdc_mock):
  def mock_read_raw_voltage(voltage):
    voltage_mapping = {
        '5V': 5.0,
        '12V': 12.0,
        '+15V': 15.0,
        '-15V': -15.0
    }
    return voltage_mapping.get(voltage, 0.0)

  cdc_mock.sysmon.read_raw_voltage.side_effect = mock_read_raw_voltage
  return cdc_mock


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_calsys_dcm_ps05_cal_initialization(calsys_dcm_ps05_cal):
  assert calsys_dcm_ps05_cal.cdc is not None
  assert calsys_dcm_ps05_cal.name == 'CALSYS.DCM.PS05.CAL'
  assert calsys_dcm_ps05_cal.access == 'rwp'
  assert calsys_dcm_ps05_cal.val_type == float
  assert calsys_dcm_ps05_cal.description == 'This property contains the calibration gain of the 5V power supply. Setting it triggers a calibration, getting the value returns the calibration status'


def test_calsys_dcm_ps05_cal_set(mock_sysmon_read_raw_voltage, calsys_dcm_ps05_cal, mock_command):
  # Trigger calibration
  mock_command.payload = [5.1]
  calsys_dcm_ps05_cal.set(mock_command)

  # Assert that the calibration was triggered
  calsys_dcm_ps05_cal.cdc.sysmon.read_raw_voltage.assert_called_once_with('5V')
  assert calsys_dcm_ps05_cal.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps05_cal_get(mock_sysmon_read_raw_voltage, calsys_dcm_ps05_cal, mock_command):
  assert calsys_dcm_ps05_cal.get(mock_command) == ['UNCALIBRATED']
  mock_command.payload = [5.1]
  calsys_dcm_ps05_cal.set(mock_command)
  assert calsys_dcm_ps05_cal.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps05_cal_set_value_not_float(calsys_dcm_ps05_cal, mock_command):
  with pytest.raises(errors.BadFloat):
    mock_command.payload = ['a']
    calsys_dcm_ps05_cal.set(mock_command)


def test_calsys_dcm_ps12_cal_initialization(calsys_dcm_ps12_cal):
  assert calsys_dcm_ps12_cal.cdc is not None
  assert calsys_dcm_ps12_cal.name == 'CALSYS.DCM.PS12.CAL'
  assert calsys_dcm_ps12_cal.access == 'rwp'
  assert calsys_dcm_ps12_cal.val_type == float
  assert calsys_dcm_ps12_cal.description == 'This property contains the calibration gain of the 12V power supply. Setting it triggers a calibration, getting the value returns the calibration status'


def test_calsys_dcm_ps12_cal_set(mock_sysmon_read_raw_voltage, calsys_dcm_ps12_cal, mock_command):
  # Trigger calibration
  mock_command.payload = [12.1]
  calsys_dcm_ps12_cal.set(mock_command)

  # Assert that the calibration was triggered
  calsys_dcm_ps12_cal.cdc.sysmon.read_raw_voltage.assert_called_once_with('12V')
  assert calsys_dcm_ps12_cal.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps12_cal_get(mock_sysmon_read_raw_voltage, calsys_dcm_ps12_cal, mock_command):
  mock_command.payload = [12.1]
  assert calsys_dcm_ps12_cal.get(mock_command) == ['UNCALIBRATED']
  calsys_dcm_ps12_cal.set(mock_command)
  assert calsys_dcm_ps12_cal.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps12_cal_set_value_not_float(calsys_dcm_ps12_cal, mock_command):
  with pytest.raises(errors.BadFloat):
    mock_command.payload = ['a']
    calsys_dcm_ps12_cal.set(mock_command)


def test_calsys_dcm_ps15_cal_pos_initialization(calsys_dcm_ps15_cal_pos):
  assert calsys_dcm_ps15_cal_pos.cdc is not None
  assert calsys_dcm_ps15_cal_pos.name == 'CALSYS.DCM.PS15.CALPOS'
  assert calsys_dcm_ps15_cal_pos.access == 'rwp'
  assert calsys_dcm_ps15_cal_pos.val_type == float
  assert calsys_dcm_ps15_cal_pos.description == 'This property contains the calibration gain of the +15V power supply. Setting it triggers a calibration, getting the value returns the calibration status'


def test_calsys_dcm_ps15_cal_pos_set(mock_sysmon_read_raw_voltage, calsys_dcm_ps15_cal_pos, mock_command):
  # Trigger calibration,
  mock_command.payload = [15.1]
  calsys_dcm_ps15_cal_pos.set(mock_command)

  # Assert that the calibration was triggered
  calsys_dcm_ps15_cal_pos.cdc.sysmon.read_raw_voltage.assert_called_once_with(
      '+15V')
  assert calsys_dcm_ps15_cal_pos.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps15_cal_pos_get(mock_sysmon_read_raw_voltage, calsys_dcm_ps15_cal_pos, mock_command):
  assert calsys_dcm_ps15_cal_pos.get(mock_command) == ['UNCALIBRATED']
  mock_command.payload = [15.1]
  calsys_dcm_ps15_cal_pos.set(mock_command)
  assert calsys_dcm_ps15_cal_pos.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps15_cal_pos_set_value_not_float(calsys_dcm_ps15_cal_pos, mock_command):
  with pytest.raises(errors.BadFloat):
    mock_command.payload = ['a']
    calsys_dcm_ps15_cal_pos.set(mock_command)


def test_calsys_dcm_ps15_cal_neg_initialization(calsys_dcm_ps15_cal_neg):
  assert calsys_dcm_ps15_cal_neg.cdc is not None
  assert calsys_dcm_ps15_cal_neg.name == 'CALSYS.DCM.PS15.CALNEG'
  assert calsys_dcm_ps15_cal_neg.access == 'rwp'
  assert calsys_dcm_ps15_cal_neg.val_type == float
  assert calsys_dcm_ps15_cal_neg.description == 'This property contains the calibration gain of the -15V power supply. Setting it triggers a calibration, getting the value returns the calibration status'


def test_calsys_dcm_ps15_cal_neg_set(mock_sysmon_read_raw_voltage, calsys_dcm_ps15_cal_neg, mock_command):
  # Trigger calibration
  mock_command.payload = [-15.1]
  calsys_dcm_ps15_cal_neg.set(mock_command)

  # Assert that the calibration was triggered
  calsys_dcm_ps15_cal_neg.cdc.sysmon.read_raw_voltage.assert_called_once_with(
      '-15V')
  assert calsys_dcm_ps15_cal_neg.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps15_cal_neg_get(mock_sysmon_read_raw_voltage, calsys_dcm_ps15_cal_neg, mock_command):
  assert calsys_dcm_ps15_cal_neg.get(mock_command) == ['UNCALIBRATED']
  mock_command.payload = [-15.1]
  calsys_dcm_ps15_cal_neg.set(mock_command)
  assert calsys_dcm_ps15_cal_neg.get(mock_command) == ['CALIBRATED']


def test_calsys_dcm_ps15_cal_neg_set_value_not_float(calsys_dcm_ps15_cal_neg, mock_command):
  with pytest.raises(errors.BadFloat):
    mock_command.payload = ['a']
    calsys_dcm_ps15_cal_neg.set(mock_command)
