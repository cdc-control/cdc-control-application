import pytest
import mock
from cdc_properties import CalsysCdcNullAuto
from fgc_ncrp_parser.fgc_ncrp import errors
import time


class MockCDC:
  def __init__(self):
    self.primrel = mock.MagicMock()
    self.nullmeter = mock.MagicMock()
    self.fadem = mock.MagicMock()
    self.logger = mock.MagicMock()
    self.state = 'ACTIVE'
    self.properties = {
        'CALSYS.CDC.OUTPUT': mock.Mock(value=['RANGE_1A']),
        'CALSYS.CDC.I_REF': mock.Mock(value=[0]),
        'CALSYS.CDC.NULL.KP': mock.Mock(value=[0.1]),
        'CALSYS.CDC.NULL.KI': mock.Mock(value=[0.2]),
        'CALSYS.CDC.NULL.NR': mock.Mock(value=[500]),
    }
    self.CDCStates = mock.MagicMock()
    self.CDCStates.ACTIVE = 'ACTIVE'
    self.CDCStates.NULLING = 'NULLING'
    self.primrel.VBStatus = mock.MagicMock()
    self.primrel.VBStatus.VB_DISCONNECTED = mock.MagicMock()
    self.primrel.get_vb_status.return_value = self.primrel.VBStatus.VB_DISCONNECTED
    self.set_null_saturation_led = mock.MagicMock()


@pytest.fixture(name='cdc')
def fixture_cdc():
  return MockCDC()


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


@pytest.fixture(name='auto_null_obj')
def fixture_auto_null_obj(cdc):
  return CalsysCdcNullAuto(cdc)


def test_nulling_succeeds(mock_command, auto_null_obj):
  mock_command.payload = [True]
  auto_null_obj.cdc.nullmeter.get_I_null.return_value = 0.0
  auto_null_obj.set(mock_command)
  auto_null_obj.cdc.logger.info.assert_called_with(
      'Nulling successful after 9 attempts')


def test_nulling_fails(mock_command, auto_null_obj):
  mock_command.payload = [True]
  auto_null_obj.cdc.nullmeter.get_I_null.return_value = 100.0
  with pytest.raises(errors.NullFailed):
    auto_null_obj.set(mock_command)
  auto_null_obj.cdc.logger.error.assert_called()


def test_validate_state_fails_invalid_state(mock_command, auto_null_obj):
  mock_command.payload = [True]
  auto_null_obj.cdc.state = "INIT"
  with pytest.raises(errors.BadState):
    auto_null_obj.set(mock_command)
  auto_null_obj.cdc.logger.error.assert_called()


def test_validate_state_fails_vb_in_use(mock_command, auto_null_obj):
  mock_command.payload = [True]
  auto_null_obj.cdc.primrel.get_vb_status.return_value = 'VB_CONNECTED'
  with pytest.raises(errors.BadState):
    auto_null_obj.set(mock_command)
  auto_null_obj.cdc.logger.error.assert_called()


def test_validate_state_fails_non_zero_iref(mock_command, auto_null_obj):
  mock_command.payload = [True]
  auto_null_obj.cdc.properties['CALSYS.CDC.I_REF'].value = [1]
  with pytest.raises(errors.BadState):
    auto_null_obj.set(mock_command)
  auto_null_obj.cdc.logger.error.assert_called()


def test_set_dac(auto_null_obj, monkeypatch):
  monkeypatch.setattr(time, "sleep", lambda _: None)
  auto_null_obj._set_dac(0x9000)
  auto_null_obj.cdc.fadem.set_dac.assert_called_with(0x7FFF)
