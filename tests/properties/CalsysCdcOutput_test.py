# pylint: disable=redefined-outer-name
# pylint: disable=protected-access
# pylint: disable=missing-docstring


import pytest
import mock
from cdc_properties import CalsysCdcOutput
from fgc_ncrp_parser.fgc_ncrp import errors
import drivers.primrel as primrel


@pytest.fixture(name='cdc_mock')
def fixture_cdc():
  return_mock = mock.MagicMock()
  return_mock.state = return_mock.CDCStates.IDLE
  return_mock.properties = {
      'CALSYS.CDC.NULL.KP': mock.MagicMock(),
      'CALSYS.CDC.NULL.KI': mock.MagicMock(),
      'CALSYS.CDC.NULL.NR': mock.MagicMock(),
      'CALSYS.CDC.I_REF': mock.MagicMock(),
  }
  return_mock.properties['CALSYS.CDC.NULL.KP'].value = [0.001]
  return_mock.properties['CALSYS.CDC.NULL.KI'].value = [0.025]
  return_mock.properties['CALSYS.CDC.NULL.NR'].value = [10]
  return_mock.properties['CALSYS.CDC.I_REF'].value = [0.0]
  return_mock.properties['CALSYS.CDC.NULL.AUTO'] = mock.MagicMock()
  # add the enum class VBStatus to the mock
  return_mock.primrel.VBStatus = primrel.Primrel.VBStatus
  return return_mock


@pytest.fixture
def calsys_cdc_output(cdc_mock):
  # patch the sleep method of time module to do nothing
  with mock.patch('time.sleep'):
    # Create an instance of CalsysCdcOutput for testing
    return_property = CalsysCdcOutput(cdc_mock)
    yield return_property


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_set_valid_range_vb_disconnected_pbcx5_disconnected(calsys_cdc_output, cdc_mock, mock_command):
  cdc_mock.primrel.get_vb_status.return_value = primrel.Primrel.VBStatus.VB_DISCONNECTED
  cdc_mock.primrel.get_pbc_x5_status.return_value = False
  cdc_mock.state = cdc_mock.CDCStates.IDLE

  # set self.cdc.nullmeter.get_I_null() to return 0.00005
  cdc_mock.nullmeter.get_I_null.return_value = 0.00005
  mock_command.payload = ['RANGE_1A']
  calsys_cdc_output.set(mock_command)

  cdc_mock.primrel.set_active.assert_called_with(True)
  cdc_mock.primrel.set_range.assert_called_with('RANGE_1A')
  assert cdc_mock.properties['CALSYS.CDC.NULL.AUTO'].nulling.called
  assert cdc_mock.state == cdc_mock.CDCStates.ACTIVE


def test_set_valid_range_vb_connected_pbcx5_disconnected(calsys_cdc_output, cdc_mock, mock_command):
  cdc_mock.primrel.get_vb_status.return_value = primrel.Primrel.VBStatus.VB_OK
  cdc_mock.primrel.get_pbc_x5_status.return_value = False
  cdc_mock.state = cdc_mock.CDCStates.IDLE

  # set self.cdc.nullmeter.get_I_null() to return 0.00005
  cdc_mock.nullmeter.get_I_null.return_value = 0.00005

  mock_command.payload = ['RANGE_5A']
  calsys_cdc_output.set(mock_command)

  cdc_mock.primrel.set_active.assert_called_with(True)
  cdc_mock.primrel.set_range.assert_called_with('RANGE_5A')
  # assert dacs have not been called, as with the vb connected, the nulling is not performed
  assert not cdc_mock.fadem.set_dac.called

  assert cdc_mock.state == cdc_mock.CDCStates.ACTIVE


def test_set_valid_range_vb_disconnected_pbcx5_connected(calsys_cdc_output, cdc_mock, mock_command):
  cdc_mock.primrel.get_vb_status.return_value = primrel.Primrel.VBStatus.VB_DISCONNECTED
  cdc_mock.primrel.get_pbc_x5_status.return_value = True
  cdc_mock.state = cdc_mock.CDCStates.IDLE

  # set self.cdc.nullmeter.get_I_null() to return 0.00005
  cdc_mock.nullmeter.get_I_null.return_value = 0.00005

  mock_command.payload = ['RANGE_5A']
  calsys_cdc_output.set(mock_command)

  # in the PBCx5 case the user sets the 5A range but actually the set_range is called with 1A range
  cdc_mock.primrel.set_active.assert_called_with(True)
  cdc_mock.primrel.set_range.assert_called_with('RANGE_1A')
  assert cdc_mock.properties['CALSYS.CDC.NULL.AUTO'].nulling.called

  assert cdc_mock.state == cdc_mock.CDCStates.ACTIVE


def test_set_invalid_range(calsys_cdc_output, mock_command):
  with pytest.raises(errors.BadParameter):
    mock_command.payload = ['INVALID_RANGE']
    calsys_cdc_output.set(mock_command)


def test_set_invalid_range_vb_connected(calsys_cdc_output, cdc_mock, mock_command):
  cdc_mock.primrel.get_vb_status.return_value = primrel.Primrel.VBStatus.VB_OK
  cdc_mock.primrel.get_pbc_x5_status.return_value = False
  cdc_mock.state = cdc_mock.CDCStates.IDLE

  mock_command.payload = ['RANGE_1A']
  with pytest.raises(errors.BadState):
    calsys_cdc_output.set(mock_command)


def test_set_invalid_range_pbcx5_connected(calsys_cdc_output, cdc_mock, mock_command):
  cdc_mock.primrel.get_vb_status.return_value = primrel.Primrel.VBStatus.VB_DISCONNECTED
  cdc_mock.primrel.get_pbc_x5_status.return_value = True
  cdc_mock.state = cdc_mock.CDCStates.IDLE

  mock_command.payload = ['RANGE_1A']
  with pytest.raises(errors.BadParameter):
    calsys_cdc_output.set(mock_command)


def test_set_bad_state(calsys_cdc_output, cdc_mock, mock_command):
  cdc_mock.state = cdc_mock.CDCStates.ERROR
  with pytest.raises(errors.BadState):
    mock_command.payload = ['RANGE_1A']
    calsys_cdc_output.set(mock_command)
