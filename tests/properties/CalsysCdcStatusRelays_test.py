import pytest
import mock
import drivers.primrel as primrel_module

from cdc_properties import CalsysCdcStatusRelays


@pytest.fixture(name='mock_cdc')
def fixture_mock_cdc():
  mock_cdc = mock.Mock()

  mock_pol_relays = mock.Mock()
  mock_pol_relays.get_active_relay_names.return_value = 'INV1024T INV512T INV256T'

  mock_onoff_relays = mock.Mock()
  mock_onoff_relays.get_active_relay_names.return_value = 'ON1024T ON512T ON1T'

  mock_cdc.primrel.get_pol_relays.return_value = mock_pol_relays
  mock_cdc.primrel.get_on_off_relays.return_value = mock_onoff_relays

  return mock_cdc


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_calsys_cdc_status_relays(mock_cdc, mock_command):
  # Test for each relay condition
  for relay, expected_result in [
      (primrel_module.SecondaryRelays.REL_SEC_4T,
       ['INV1024T INV512T INV256T ON1024T ON512T ON1T ONS4T']),
      (primrel_module.SecondaryRelays.REL_SEC_8T,
       ['INV1024T INV512T INV256T ON1024T ON512T ON1T ONS8T']),
      (primrel_module.SecondaryRelays.REL_SEC_16T,
       ['INV1024T INV512T INV256T ON1024T ON512T ON1T ONS16T']),
      (primrel_module.SecondaryRelays.REL_SEC_40T,
       ['INV1024T INV512T INV256T ON1024T ON512T ON1T ONS40T']),
      ('OFF', ['INV1024T INV512T INV256T ON1024T ON512T ON1T '])
  ]:
    mock_cdc.primrel.get_secondary_relays.return_value = relay
    calsys_cdc_status_relays = CalsysCdcStatusRelays(mock_cdc)
    assert calsys_cdc_status_relays.get(mock_command) == expected_result
