# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
import pytest
import mock
from cdc_properties import StatusFaults


@pytest.fixture
def status_faults():
  # Create a mock cdc object for testing
  cdc_mock = mock.Mock()
  # Create an instance of StatusFaults for testing
  faults = StatusFaults(cdc_mock)
  yield faults


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_status_faults_initialization(status_faults):
  # Check if the cdc attribute is set correctly
  assert status_faults.cdc is not None

  # Check if the value attribute is an array containing an empty set
  assert status_faults.value[0] == set()


def test_status_faults_set_fault(status_faults):
  # Set a fault and check if it is added to the value set
  status_faults.set_fault('PBC_TMP')
  assert 'PBC_TMP' in status_faults.value[0]


def test_status_faults_clear_fault(status_faults):
  # Set a fault and then clear it
  status_faults.set_fault('PBC_TMP')
  status_faults.clear_fault('PBC_TMP')
  # Check if the fault is removed from the value set
  assert 'PBC_TMP' not in status_faults.value[0]


def test_status_faults_get(status_faults, mock_command):
  # Set multiple faults
  status_faults.set_fault('PBC_TMP')
  status_faults.set_fault('VB_PWR')
  # Get the faults string
  faults_string = status_faults.get(mock_command)[0]
  # Split the faults string into a set of faults
  faults = set(faults_string.split())
  # Check if the obtained set of faults is equal to the expected set of faults
  expected_faults = {'PBC_TMP', 'VB_PWR'}
  assert faults == expected_faults


def test_status_faults_set_fault_exception(status_faults):
  # Test exception when setting a fault with an invalid value
  with pytest.raises(ValueError):
    status_faults.set_fault("INVALID_FAULT")


def test_status_faults_clear_fault_exception(status_faults):
  # Test exception when clearing a fault with an invalid value
  with pytest.raises(ValueError):
    status_faults.clear_fault("INVALID_FAULT")
