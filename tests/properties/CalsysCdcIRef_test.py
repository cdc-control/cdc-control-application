# pylint: disable=redefined-outer-name
# pylint: disable=protected-access

import pytest
import mock
from cdc_properties import CalsysCdcIRef
from fgc_ncrp_parser.fgc_ncrp import errors


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = [1]
  command.array = []
  return command


@pytest.fixture(name='cdc_mock')
def fixture_cdc_mock():
  return_mock = mock.MagicMock()
  return_mock.state = return_mock.CDCStates.ACTIVE
  return_mock.primrel = mock.MagicMock()
  return_mock.primrel.get_pbc_x5_status.return_value = False
  return_mock.properties = {
      'CALSYS.CDC.OUTPUT': mock.MagicMock()
  }
  return_mock.properties['CALSYS.CDC.OUTPUT'].value = ['RANGE_1A']
  return return_mock


@pytest.fixture(name='calsys_cdc_iref')
def fixture_calsys_cdc_iref(cdc_mock):
  # patch the sleep method of time module to do nothing
  with mock.patch('time.sleep'):
    # Create an instance of CalsysCdcIRef for testing
    return_property = CalsysCdcIRef(cdc_mock)
    yield return_property


def test_iref_set(mock_command, calsys_cdc_iref):
  mock_command.payload = [0.95]
  calsys_cdc_iref.set(mock_command)
  assert calsys_cdc_iref.value == [0.95]


def test_zero_set(mock_command, calsys_cdc_iref):
  mock_command.payload = [0.0]
  calsys_cdc_iref.set(mock_command)
  assert calsys_cdc_iref.value == [0.0]
  assert calsys_cdc_iref.cdc.primrel.set_primary_relays.call_args[0][0] == 0b00000000


def test_primary_relays(mock_command, calsys_cdc_iref):
  # for RANGE_1A and IREF 1A we expect 4000 = 0b111110100000 relay setting
  mock_command.payload = [1.0]
  calsys_cdc_iref.set(mock_command)
  assert calsys_cdc_iref.cdc.primrel.set_primary_relays.call_args[0][0] == 0b111110100000


def test_out_of_limits(mock_command, calsys_cdc_iref):
  mock_command.payload = [1.1]
  with pytest.raises(errors.OutOfLimits):
    calsys_cdc_iref.set(mock_command)
