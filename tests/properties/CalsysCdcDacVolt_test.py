import pytest
import mock
from fgc_ncrp_parser.fgc_ncrp import errors
from cdc_properties import CalsysCdcDacVolt


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = [1]
  command.array = []
  return command


@pytest.fixture(name='calsys_cdc_dac_volt_mock')
def fixture_calsys_cdc_dac_volt_mock():
  fadem_mock = mock.Mock()
  fadem_mock.get_dac.return_value = 0x8000

  primrel_mock = mock.Mock()
  primrel_mock.get_dac.return_value = -0x8000

  cdc_mock = mock.Mock()
  cdc_mock.fadem = fadem_mock
  cdc_mock.primrel = primrel_mock

  return CalsysCdcDacVolt(cdc_mock)


def test_get_with_no_array(mock_command, calsys_cdc_dac_volt_mock):
  assert calsys_cdc_dac_volt_mock.get(mock_command) == [0.0, -5.0]


def test_get_with_array_of_one_element(mock_command, calsys_cdc_dac_volt_mock):
  mock_command.array = [0]
  assert calsys_cdc_dac_volt_mock.get(mock_command) == [0.0]

  mock_command.array = [1]
  assert calsys_cdc_dac_volt_mock.get(mock_command) == [-5.0]


def test_get_with_invalid_array_element(mock_command, calsys_cdc_dac_volt_mock):
  mock_command.array = [2]
  with pytest.raises(errors.BadArrayIdx):
    calsys_cdc_dac_volt_mock.get(mock_command)


def test_get_with_two_element_array(mock_command, calsys_cdc_dac_volt_mock):
  mock_command.array = [0, 1]
  assert calsys_cdc_dac_volt_mock.get(mock_command) == [0.0, -5.0]


def test_get_with_invalid_two_element_array(mock_command, calsys_cdc_dac_volt_mock):
  mock_command.array = [2, 3]
  with pytest.raises(errors.BadArrayIdx):
    calsys_cdc_dac_volt_mock.get(mock_command)


def test_get_with_three_element_array(mock_command, calsys_cdc_dac_volt_mock):
  mock_command.array = [0, 1, 1]
  assert calsys_cdc_dac_volt_mock.get(mock_command) == [0.0, -5.0]


def test_get_with_long_array(mock_command, calsys_cdc_dac_volt_mock):
  mock_command.array = [0, 1, 2, 3]
  with pytest.raises(errors.BadArrayLen):
    calsys_cdc_dac_volt_mock.get(mock_command)
