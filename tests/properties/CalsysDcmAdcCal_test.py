# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
import pytest
import mock
from cdc_properties import CalsysDcmAdcCal
import os


@pytest.fixture
def calsys_dcm_adc_cal():
  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))
  # Create a mock cdc object for testing
  cdc_mock = mock.Mock()
  # Create an instance of CalsysDcmAdcCal for testing
  return_property = CalsysDcmAdcCal(cdc_mock)
  yield return_property
  # remove all the files under the configs folder except for the README.md file
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_calsys_dcm_adc_cal_initialization(calsys_dcm_adc_cal):
  # Check if the cdc attribute is set correctly
  assert calsys_dcm_adc_cal.cdc is not None
  # Check if the value attribute is False initially
  assert calsys_dcm_adc_cal.value == [False]


def test_calsys_dcm_adc_cal_set(calsys_dcm_adc_cal, mock_command):
  nullmeter_mock = mock.Mock()
  # Set the nullmeter mock as the return value for cdc.nullmeter
  calsys_dcm_adc_cal.cdc.nullmeter = nullmeter_mock
  # Set the return value of get_zero method on the nullmeter_mock
  nullmeter_mock.get_zero.return_value = 0
  # Create a mock property object for testing

  property_mock = mock.Mock()
  # Set the return value of _set method on the property_mock
  property_mock._set.return_value = None

  properties_dict = {'CALSYS.DCM.ADC.RAW_ZERO': property_mock}

  # Set the properties_dict as the value of cdc.properties
  calsys_dcm_adc_cal.cdc.properties = properties_dict

  # Call the set method
  mock_command.payload = ['']
  calsys_dcm_adc_cal.set(mock_command)
  # Check if the nullmeter.get_zero() method is called
  nullmeter_mock.get_zero.assert_called_once()
  # Check if the nullmeter.set_zero() method is called with the return value of nullmeter.get_zero()
  nullmeter_mock.set_zero.assert_called_once_with(nullmeter_mock.get_zero())
  # Check if the CALSYS.DCM.ADC.RAW_ZERO property is set with the same value as nullmeter.get_zero()
  assert calsys_dcm_adc_cal.cdc.properties['CALSYS.DCM.ADC.RAW_ZERO']._set.called
  # Check if the value attribute is set to True
  assert calsys_dcm_adc_cal.value == [True]


def test_calsys_dcm_adc_cal_get_calibrated(calsys_dcm_adc_cal, mock_command):
  # Set the value attribute to True
  calsys_dcm_adc_cal.value = [True]
  # Check if the get method returns "CALIBRATED"
  assert calsys_dcm_adc_cal.get(mock_command) == ["CALIBRATED"]


def test_calsys_dcm_adc_cal_get_uncalibrated(calsys_dcm_adc_cal, mock_command):
  # Set the value attribute to False
  calsys_dcm_adc_cal.value = [False]
  # Check if the get method returns "UNCALIBRATED"
  assert calsys_dcm_adc_cal.get(mock_command) == ["UNCALIBRATED"]
