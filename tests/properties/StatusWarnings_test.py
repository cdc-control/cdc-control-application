# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
import pytest
import mock
from cdc_properties import StatusWarnings


@pytest.fixture
def status_warnings():
  # Create a mock cdc object for testing
  cdc_mock = mock.Mock()
  # Create an instance of StatusWarnings for testing
  warnings = StatusWarnings(cdc_mock)
  yield warnings


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_status_warnings_initialization(status_warnings):
  # Check if the cdc attribute is set correctly
  assert status_warnings.cdc is not None
  # Check if the value attribute is an empty set
  assert status_warnings.value[0] == set()


def test_status_warnings_set_warning(status_warnings):
  # Set a warning and check if it is added to the value set
  status_warnings.set_warning('PBC_TMP')
  assert 'PBC_TMP' in status_warnings.value[0]


def test_status_warnings_clear_warning(status_warnings):
  # Set a warning and then clear it
  status_warnings.set_warning('PBC_TMP')
  status_warnings.clear_warning('PBC_TMP')
  # Check if the warning is removed from the value set
  assert 'PBC_TMP' not in status_warnings.value[0]


def test_status_warnings_get(status_warnings, mock_command):
  # Set multiple warnings
  status_warnings.set_warning('PBC_TMP')
  status_warnings.set_warning('CLP_OFF')
  # Check if the get method returns the warnings as a string
  warnings_string = status_warnings.get(mock_command)[0]
  # Split the warnings string into a set of warnings
  warnings = set(warnings_string.split())
  # Check if the obtained set of warnings is equal to the expected set of warnings
  expected_warnings = {'PBC_TMP', 'CLP_OFF'}
  assert warnings == expected_warnings


def test_status_warnings_set_warning_exception(status_warnings):
  # Test exception when setting a warning with an invalid value
  with pytest.raises(ValueError):
    status_warnings.set_warning("INVALID_WARNING")


def test_status_warnings_clear_warning_exception(status_warnings):
  # Test exception when clearing a warning with an invalid value
  with pytest.raises(ValueError):
    status_warnings.clear_warning("INVALID_WARNING")
