import pytest
import mock
from cdc_properties import CalsysCdcDac1TwCal
import time

from fgc_ncrp_parser.fgc_ncrp import errors


# The following test cases focus on the primary methods of the class

@pytest.fixture(name='cdc')
def fixture_cdc():
  cdc_mock = mock.MagicMock()
  cdc_mock.primrel.set_dac.return_value = None
  cdc_mock.primrel.set_primary_relays.return_value = None
  cdc_mock.primrel.set_1t_dac_negative.return_value = None
  cdc_mock.primrel.set_negative.return_value = None
  cdc_mock.primrel.set_1t_dac_relay.return_value = None
  cdc_mock.primrel.get_vb_status.return_value = cdc_mock.primrel.VBStatus.VB_DISCONNECTED
  cdc_mock.nullmeter.get_I_null.return_value = 0.0
  cdc_mock.properties = {
      'CALSYS.CDC.I_REF': mock.Mock(value=[0]),
      'CALSYS.CDC.DAC1TW.ZERO': mock.Mock(value=[0]),
      'CALSYS.CDC.DAC1TW.FS': mock.Mock(value=[32768]),
      'CALSYS.CDC.OUTPUT': mock.Mock(value=['RANGE_1A']),
  }
  cdc_mock.state = cdc_mock.CDCStates.ACTIVE
  cdc_mock.fadem.set_dac.return_value = None
  return cdc_mock


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  mock_command = mock.Mock()
  mock_command.payload = ['']
  mock_command.array = []
  return mock_command


@pytest.fixture(name='cal_obj')
def fixture_cal_obj(cdc):
  return CalsysCdcDac1TwCal(cdc)


def test_nulling_succeeds(cal_obj):
  assert cal_obj._nulling() == 0
  cal_obj.cdc.nullmeter.get_I_null.assert_called()
  cal_obj.cdc.primrel.set_dac.assert_called()
  cal_obj.cdc.logger.info.assert_called_with('Nulling succeeded')


def test_nulling_fails(cal_obj):
  cal_obj.cdc.nullmeter.get_I_null.return_value = 1000.0
  with pytest.raises(errors.NullFailed):
    cal_obj._nulling()
  cal_obj.cdc.logger.error.assert_called()


def test_validate_state_succeeds(cal_obj):
  cal_obj._validate_state()  # Should pass without raising any exception


def test_validate_state_fails_invalid_state(cal_obj):
  cal_obj.cdc.state = "INIT"
  with pytest.raises(errors.BadState):
    cal_obj._validate_state()
  cal_obj.cdc.logger.error.assert_called()


def test_set_dac(cal_obj):
  cal_obj._set_dac(0x9000)
  cal_obj.cdc.fadem.set_dac.assert_called_with(0x7FFF)


def test_get(cal_obj, mock_command):
  assert cal_obj.get(mock_command) == ['UNCALIBRATED']


class FirstOrderSystem:
  def __init__(self, a=1, b=1, initial_offset=5000, perturbation=1000):
    self.a = a
    self.b = b
    self.y = initial_offset  # system output with initial offset
    self.perturbation = perturbation  # system input perturbation

  def update(self, u, dt):
    self.y += dt * (self.a * self.y + self.b * u) + \
        dt * self.b * self.perturbation
    return self.y


def test_pid_with_simulation(cal_obj, monkeypatch):
  system = FirstOrderSystem(
      a=0.1, b=0.1, initial_offset=5000, perturbation=1000)
  sample_time = 0.01  # 10 ms
  # override the sleep function to simulate the real time delay
  monkeypatch.setattr(time, "sleep", lambda _: None)
  # setup the get_I_null method to simulate the system's response
  cal_obj.cdc.nullmeter.get_I_null = lambda: system.y * 10
  # intercept the set_dac call to simulate control input to the system
  original_set_dac = cal_obj.cdc.primrel.set_dac

  def set_dac_wrapper(value):
    original_set_dac(value)
    system.update(value, sample_time)

  cal_obj.cdc.primrel.set_dac = set_dac_wrapper

  # test the nulling method with the simulated system
  assert abs(cal_obj._nulling()) == 1000
  cal_obj.cdc.logger.info.assert_called_with('Nulling succeeded')


def test_set_with_simulation(cal_obj, monkeypatch, mock_command):
  system = FirstOrderSystem(
      a=0.1, b=0.1, initial_offset=5000, perturbation=1000)
  sample_time = 0.01  # 10 ms
  # override the sleep function to simulate the real time delay
  monkeypatch.setattr(time, "sleep", lambda _: None)
  # setup the get_I_null method to simulate the system's response
  cal_obj.cdc.nullmeter.get_I_null = lambda: system.y * 10
  # intercept the set_dac call to simulate control input to the system
  original_set_dac = cal_obj.cdc.primrel.set_dac

  def set_dac_wrapper(value):
    original_set_dac(value)
    system.update(value, sample_time)

  cal_obj.cdc.primrel.set_dac = set_dac_wrapper

  # test the nulling method with the simulated system
  cal_obj.set(mock_command)
  # assert that the _set function for the zero has been called with 1000 and the full scale with 2000, with a tolerance of 1
  cal_obj.cdc.properties['CALSYS.CDC.DAC1TW.ZERO']._set.assert_called_with([
      pytest.approx(-1000, 2)])
  cal_obj.cdc.properties['CALSYS.CDC.DAC1TW.FS']._set.assert_called_with([
      pytest.approx(-2000, 2)])

  assert cal_obj.get(mock_command) == ['CALIBRATED']
