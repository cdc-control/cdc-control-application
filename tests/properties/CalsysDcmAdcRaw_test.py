import pytest
import mock
from cdc_properties import CalsysDcmAdcRaw
from drivers.nullmeter import Nullmeter
import fgc_ncrp_parser.fgc_ncrp.errors as errors


@pytest.fixture(name='mock_cdc')
def fixture_mock_cdc():
  mock_cdc = mock.Mock()
  mock_cdc.nullmeter = Nullmeter(mock.Mock())
  mock_cdc.nullmeter.adc = mock.Mock()
  mock_cdc.nullmeter.adc.read = mock.Mock(
      side_effect=[100, 200, 300, 400, 500, 600])
  return mock_cdc


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_get_without_array(mock_cdc, mock_command):
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  result = dut_property.get(mock_command)
  # Update the expected result with your actual expected value
  assert result == [100, 200, 300, 400, 500, 600]


def test_get_with_single_channel(mock_cdc, mock_command):
  mock_command.array = [1]
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [100]


def test_get_with_start_channel(mock_cdc, mock_command):
  mock_command.array = [1, '']
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [100, 200, 300, 400, 500]


def test_get_with_range_of_channels(mock_cdc, mock_command):
  mock_command.array = [1, 3]
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [100, 200, 300]


def test_get_with_to_from_step(mock_cdc, mock_command):
  mock_command.array = [1, 3, 2]
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  result = dut_property.get(mock_command)
  assert result == [100, 200]


def test_get_with_too_log_array(mock_cdc, mock_command):
  mock_command.array = [1, 2, 3, 4]
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  with pytest.raises(errors.BadArrayLen):
    dut_property.get(mock_command)


def test_get_with_illegal_channel(mock_cdc, mock_command):
  mock_command.array = [10]
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  with pytest.raises(errors.BadArrayIdx):
    dut_property.get(mock_command)


def test_last_before_first_channel(mock_cdc, mock_command):
  mock_command.array = [3, 1]
  dut_property = CalsysDcmAdcRaw(mock_cdc)
  with pytest.raises(errors.BadArrayIdx):
    dut_property.get(mock_command)
