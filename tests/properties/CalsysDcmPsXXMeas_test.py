# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
import pytest
from unittest import mock
import os
from cdc_properties import (
    CalsysDcmPs05Meas,
    CalsysDcmPs12Meas,
    CalsysDcmPs15MeasPos,
    CalsysDcmPs15MeasNeg
)


@pytest.fixture(name='cdc_mock')
def fixture_cdc():
  return mock.Mock()


@pytest.fixture(autouse=True)
def cleanup_configs_directory():
  yield
  for file in os.listdir('configs'):
    if file != 'README.md':
      os.remove(os.path.join('configs', file))


@pytest.fixture
def calsys_dcm_ps05_meas(cdc_mock):
  return CalsysDcmPs05Meas(cdc_mock)


@pytest.fixture
def calsys_dcm_ps12_meas(cdc_mock):
  return CalsysDcmPs12Meas(cdc_mock)


@pytest.fixture
def calsys_dcm_ps15_meas_pos(cdc_mock):
  return CalsysDcmPs15MeasPos(cdc_mock)


@pytest.fixture
def calsys_dcm_ps15_meas_neg(cdc_mock):
  return CalsysDcmPs15MeasNeg(cdc_mock)


@pytest.fixture(name='mock_command')
def fixture_mock_command():
  command = mock.Mock()
  command.payload = []
  command.array = []
  return command


def test_calsys_dcm_ps05_meas_initialization(calsys_dcm_ps05_meas):
  assert calsys_dcm_ps05_meas.cdc is not None
  assert calsys_dcm_ps05_meas.name == 'CALSYS.DCM.PS05.MEAS'
  assert calsys_dcm_ps05_meas.access == 'r'
  assert calsys_dcm_ps05_meas.val_type == float
  assert calsys_dcm_ps05_meas.description == 'Accessing this property triggers a measurement of the 5V power supply and returns the value in Volt.'


def test_calsys_dcm_ps05_meas_get(calsys_dcm_ps05_meas, mock_command):
  calsys_dcm_ps05_meas.cdc.sysmon.read_calibrated_voltage.return_value = 5.0
  assert calsys_dcm_ps05_meas.get(mock_command) == [5.0]


def test_calsys_dcm_ps12_meas_initialization(calsys_dcm_ps12_meas):
  assert calsys_dcm_ps12_meas.cdc is not None
  assert calsys_dcm_ps12_meas.name == 'CALSYS.DCM.PS12.MEAS'
  assert calsys_dcm_ps12_meas.access == 'r'
  assert calsys_dcm_ps12_meas.val_type == float
  assert calsys_dcm_ps12_meas.description == 'Accessing this property triggers a measurement of the 12V power supply and returns the value in Volt.'


def test_calsys_dcm_ps12_meas_get(calsys_dcm_ps12_meas, mock_command):
  calsys_dcm_ps12_meas.cdc.sysmon.read_calibrated_voltage.return_value = 12.0
  assert calsys_dcm_ps12_meas.get(mock_command) == [12.0]


def test_calsys_dcm_ps15_meas_pos_initialization(calsys_dcm_ps15_meas_pos):
  assert calsys_dcm_ps15_meas_pos.cdc is not None
  assert calsys_dcm_ps15_meas_pos.name == 'CALSYS.DCM.PS15.MEASPOS'
  assert calsys_dcm_ps15_meas_pos.access == 'r'
  assert calsys_dcm_ps15_meas_pos.val_type == float
  assert calsys_dcm_ps15_meas_pos.description == 'Accessing this property triggers a measurement of the positive 15V power supply and returns the value in Volt.'


def test_calsys_dcm_ps15_meas_pos_get(calsys_dcm_ps15_meas_pos, mock_command):
  calsys_dcm_ps15_meas_pos.cdc.sysmon.read_calibrated_voltage.return_value = 15.0
  assert calsys_dcm_ps15_meas_pos.get(mock_command) == [15.0]


def test_calsys_dcm_ps15_meas_neg_initialization(calsys_dcm_ps15_meas_neg):
  assert calsys_dcm_ps15_meas_neg.cdc is not None
  assert calsys_dcm_ps15_meas_neg.name == 'CALSYS.DCM.PS15.MEASNEG'
  assert calsys_dcm_ps15_meas_neg.access == 'r'
  assert calsys_dcm_ps15_meas_neg.val_type == float
  assert calsys_dcm_ps15_meas_neg.description == 'Accessing this property triggers a measurement of the negative 15V power supply and returns the value in Volt.'


def test_calsys_dcm_ps15_meas_neg_get(calsys_dcm_ps15_meas_neg, mock_command):
  calsys_dcm_ps15_meas_neg.cdc.sysmon.read_calibrated_voltage.return_value = -15.0
  assert calsys_dcm_ps15_meas_neg.get(mock_command) == [-15.0]
