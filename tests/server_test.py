# pylint: disable=missing-docstring
#

import mock


import fgc_ncrp_server


def test_init_ok():
  cdc = mock.Mock()
  gateway = mock.Mock()
  logger = mock.Mock()
  port = 1
  # add the [Server] section to the config
  config = {
      'Server': {
          'queue_capacity': 10,
          'num_workers': 2
      }
  }
  fgc_ncrp_server.Server(cdc, gateway, logger, port, config)


def test_accept_connection_ok():
  cdc = mock.Mock()
  gateway = mock.Mock()
  logger = mock.Mock()
  port = 1
  config = {
      'Server': {
          'queue_capacity': 10,
          'num_workers': 2
      }
  }
  # create the server
  server = fgc_ncrp_server.Server(cdc, gateway, logger, port, config)
  # create a mock socket
  clientsocket = mock.Mock()
  serversocket = mock.Mock()
  serversocket.accept.return_value = (clientsocket, mock.Mock())
  # call the accept_connection method
  server.accept_connection(serversocket)
  # assert that the accept method was called
  assert serversocket.accept.called
