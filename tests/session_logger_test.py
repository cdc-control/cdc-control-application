import pytest
import os
from session_logger import SessionLogger


@pytest.fixture
def session_logger(tmpdir):
  testing_config = {
      'logger': {
          'log_dir': './logs',
          'log_level': 'DEBUG',
          'file_log_level': 'DEBUG',
          'console_log_level': 'DEBUG',
          'max_log_files': 10,
          'max_log_size': 1024 * 1024
      },
      'sysmon': {
          'onewire_device_check': True,
          'onewire_mem_address': 0x43c00000,
          'max_temp': 80,
          'min_temp': 0,
          'voltage_max_alarm': 1.1,
          'voltage_min_alarm': 0.9
      },
      'onewire_ids': {
          'Nullmeter': 11385099912896426024,
          'PrimRel': 12033618259470891048,
          'LVPSU': 5908722766250548264,
          '1T_driver': 8574853745653653800,
          'Backplane': 7493989835080814888,
          'FADEM': 14195346080605196584,
          'Modamp': 3098476598770918696,
          'Actfilt': 2017612688198891304,
          'Controller': 2449958252426477352
      },
      'CDC': {
          'stepping': 'True',
      }
  }

  return SessionLogger(testing_config['logger'])


def test_session_logger(session_logger):
  # Log some messages
  session_logger.debug("Debug message")
  session_logger.info("Info message")
  session_logger.warning("Warning message")
  session_logger.error("Error message")

  # Retrieve and assert the logged messages
  logs = session_logger.retrieve_logs()
  assert len(logs) == 4
  expected_log_message = "Debug message"
  assert any(log.strip().endswith(expected_log_message) for log in logs)
  expected_log_message = "Info message"
  assert any(log.strip().endswith(expected_log_message) for log in logs)
  expected_log_message = "Warning message"
  assert any(log.strip().endswith(expected_log_message) for log in logs)
  expected_log_message = "Error message"
  assert any(log.strip().endswith(expected_log_message) for log in logs)

  # Check if the log file exists
  log_file_path = os.path.join(
      session_logger.log_dir, session_logger.current_log_file_name)
  assert os.path.exists(log_file_path)


@pytest.fixture(scope="session", autouse=True)
def cleanup_logs():
  yield
  # delete the log files
  for file in os.listdir('logs'):
    if file.endswith(".log"):
      os.remove(os.path.join('logs', file))
