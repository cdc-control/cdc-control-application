# pylint: disable=protected-access
# pylint: disable=missing-docstring


# this program starts the cdc.py program and creates a test client to send commands to it

from time import sleep

import os

import socket
import random
from multiprocessing import Process
import pytest
import mock
import cdc

# messages contains all the tests and the expected responses
properties = [
    ("! s     foo.bar     aaa,bbb,ccc\n", "$ !\n66 unknown property\n;"),
    ("!t4g s     foo.bar     aaa,bbb,ccc\n", "$t4g !\n66 unknown property\n;"),
    ("!too_long_tag_45678901234567890123 g foo\n", "$ !\n14 syntax error\n;"),
    ("zzz bb\n", "$ !\n14 syntax error\n;"),
    ("! s\n", "$ !\n14 syntax error\n;"),
    ("! s %$\n", "$ !\n14 syntax error\n;"),
    ("! s aaa%$\n", "$ !\n14 syntax error\n;"),
    ("! s foo.bar(10)\n", "$ !\n66 unknown property\n;"),
    ("! s foo.bar.blorb.nap(10)\n", "$ !\n66 unknown property\n;"),
    ("! s Device:foo.bar.blorb.nap(10)\n", "$ !\n38 unknown dev\n;"),
    ("! s 1:CALSYS.CDC.ADC.GAIN_1A 1.0\n",   "$ !\n34 bad state\n;"),
    ("! s 1:CALSYS.CDC.NULL.NR 1001\n",   "$ !\n28 out of limits\n;"),
    ("! s 1:CALSYS.CDC.NULL.NR FD\n", "$ !\n18 bad integer\n;"),
    ("!tag s 1:CALSYS.CDC.NULL.NR 1\n", "$tag .\n\n;"),
    ("! s CDC:CALSYS.CDC.NULL.NR 0\n", "$ .\n\n;"),
    ("! s CDC:CALSYS.CDC.NULL.NR 500\n", "$ .\n\n;"),
    ("! s CDC:CALSYS.CDC.NULL.NR 12\n",  "$ .\n\n;"),
    ("!tag g CDC:CALSYS.CDC.NULL.NR \n", "$tag .\n100\n;"),
]

names = ['test_unknown_property', 'test_unknown_property_with_tag', 'test_tag_too_long', 'test_no_delimiters', 'test_no_property', 'test_forbidden_symbol', 'test_forbidden_symbols', 'test_unknown_property_user',
         'test_long_unknown_property', 'test_device_unknown_property', 'test_out_of_limits', 'test_bad_float', 'test_tag', 'test_ok', 'test_lower_case', 'test_verb_upper_case', 'test_ok_float_without_point', 'test_get_ok']


@pytest.mark.parametrize('fgc_property', properties)
def test_properties(fgc_property):
  # pick a random port every time over 40000
  server_port = 10000 + int(random.random() * 54000)
  host_name = socket.gethostbyname("localhost")

  # mock spidev to avoid errors
  with mock.patch('spidev.SpiDev'), mock.patch('cdc_sysmon.devmem'), mock.patch('cdc_sysmon.SystemMonitor.run'):
    # start the server
    test_process = Process(target=cdc.main, args=(["", server_port],))
    test_process.start()

    assert wait_for_server(host_name, server_port)

    # start the tets: connect to the server and send the message
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as test_socket:
      test_socket.connect((host_name, server_port))
      # send message to server
      test_socket.sendall(fgc_property[0].encode())
      data = test_socket.recv(1024)
      test_socket.close()
    # assert
    assert data.decode() == fgc_property[1]
    test_process.terminate()


def test_set_get():
  # pick a random port every time over 10000
  server_port = 10000 + int(random.random() * 54000)
  host_name = socket.gethostbyname("localhost")

  # mock spidev to avoid errors
  with mock.patch('spidev.SpiDev'), mock.patch('cdc_sysmon.devmem'), mock.patch('cdc_sysmon.SystemMonitor.run'):
      # start the server
    test_process = Process(target=cdc.main, args=(["", server_port],))
    test_process.start()

    assert wait_for_server(host_name, server_port)

    # generate an int between 0 and 1000
    value = int(random.random() * 1000)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as test_socket:
      test_socket.connect((host_name, server_port))
      # send message to server
      # set to the random value
      test_socket.sendall(
          f"! s CDC:CALSYS.CDC.NULL.NR {value}\n".encode())
      data = test_socket.recv(1024)
      test_socket.close()

    # now we get the same property
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as test_socket:
      test_socket.connect((host_name, server_port))
      # send message to server
      test_socket.sendall("!tag g CDC:CALSYS.CDC.NULL.NR\n".encode())
      data = test_socket.recv(1024)
      test_socket.close()
    assert data.decode() == f"$tag .\n{value}\n;"
    test_process.terminate()

# define the function for waiting for the server to start


def wait_for_server(host, port, timeout=1):
  # wait for the server to start, use timeout only if not in debug mode
  #
  # mock spidev to avoid errors
  with mock.patch('spidev.SpiDev'), mock.patch('cdc_sysmon.devmem'), mock.patch('cdc_sysmon.SystemMonitor.run'):
    while timeout > 0:
      try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as test_socket:
          test_socket.connect((host, port))
          test_socket.close()
        return True
      # catch the exception of the server not started yet
      except ConnectionRefusedError:
        if not __debug__:
          timeout -= 0.05
        sleep(0.05)
    return False

# in this test, the client drops the connection before the server has sent the response. The server should not crash


def test_drop_connection():
  # pick a random port every time over 10000
  server_port = 10000 + int(random.random() * 54000)
  host_name = socket.gethostbyname("localhost")
  # mock spidev to avoid errors
  with mock.patch('spidev.SpiDev'), mock.patch('cdc_sysmon.devmem'), mock.patch('cdc_sysmon.SystemMonitor.run'):
    # start the server
    test_process = Process(target=cdc.main, args=(["", server_port],))
    test_process.start()

    assert wait_for_server(host_name, server_port)

    # start the tets: connect to the server and send the message
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as test_socket:
      test_socket.connect((host_name, server_port))
      # send message to server
      test_socket.sendall("!tag g CDC:CALSYS.CDC.NULL.NR\n".encode())
      test_socket.close()
    # assert
    assert test_process.is_alive()
    test_process.terminate()

# in this test, the client floods the server with messages. The server should not crash


def test_flood():
  # pick a random port every time over 10000
  server_port = 10000 + int(random.random() * 54000)
  host_name = socket.gethostbyname("localhost")

  # mock spidev to avoid errors
  with mock.patch('spidev.SpiDev'), mock.patch('cdc_sysmon.devmem'), mock.patch('cdc_sysmon.SystemMonitor.run'):
      # start the server
    test_process = Process(target=cdc.main, args=(["", server_port],))
    test_process.start()

    assert wait_for_server(host_name, server_port)

    # start the tets: connect to the server and send the message
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as test_socket:
      test_socket.connect((host_name, server_port))
      # send message to server 500 times
      for _ in range(500):
        test_socket.sendall("!tag g CDC:CALSYS.CDC.NULL.NR\n".encode())
      test_socket.close()
    # assert
    assert test_process.is_alive()
    test_process.terminate()

# # test to check the behaviour if the file is missing and open fails. Open is mocked with the mock_open function
# def test_config_file_missing():
#   with mock.patch('builtins.open', mock.mock_open()) as mock_file:
#     # make the mock file raise the OSError exception
#     mock_file.side_effect = OSError

#     # start the server
#     p = Process(target=cdc.main, args=(["", 10000],))
#     p.start()
#     sleep(1)
#     #
#     assert not p.is_alive()

# cleanup function that deletes the log files


@pytest.fixture(scope="session", autouse=True)
def cleanup_logs():
  yield
  # delete the log files
  for file in os.listdir('logs'):
    if file.endswith(".log"):
      os.remove(os.path.join('logs', file))


if __name__ == "__main__":
  pytest.main()
