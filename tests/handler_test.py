# pylint: disable=missing-docstring
# pylint: disable=protected-access

import mock
import pytest

from fgc_ncrp_handler import handler
from fgc_ncrp_parser.fgc_ncrp import ncrp


@pytest.fixture
def mock_objects():
  clientsocket = mock.Mock()
  cdc = mock.Mock()
  gateway = mock.Mock()
  logger = mock.Mock()

  return clientsocket, cdc, gateway, logger


@pytest.fixture
def parser_mock():
  with mock.patch('fgc_ncrp_handler.parser.parse') as mock_parser:
    yield mock_parser


def test_handler_set_device(mock_objects, parser_mock):
  clientsocket, cdc, gateway, logger = mock_objects
  cdc.set.return_value = ncrp.errors.OkRsp()
  parser_mock.return_value = ncrp.Command(
      '', 's', 'CDC', 'CALSYS.CDC.ADC.GAIN_1A', '', '', '', '1.5')
  handler(clientsocket, cdc, gateway, logger,
          config={'CDC': {'name': 'CDC', 'ID': '1'}})
  cdc.set.assert_called_once_with(parser_mock.return_value)
  clientsocket.send.assert_called_once_with(b'$ !\n1 ok rsp\n;')


def test_unknown_dev(mock_objects, parser_mock):
  clientsocket, cdc, gateway, logger = mock_objects
  gateway.get.return_value = 1.5
  parser_mock.return_value = ncrp.Command(
      '', 'G', '4', 'MOCK.PROPERTY', '', '', '', '')
  handler(clientsocket, cdc, gateway, logger,
          config={'CDC': {'name': 'CDC', 'ID': '1'}})
  clientsocket.send.assert_called_once_with(b'$ !\n38 unknown dev\n;')


def test_get_dev(mock_objects, parser_mock):
  clientsocket, cdc, gateway, logger = mock_objects
  cdc.get.return_value = 1.5
  parser_mock.return_value = ncrp.Command(
      '', 'G', 'CDC', 'CALSYS.CDC.ADC.GAIN_1A', '', '', '', '')
  handler(clientsocket, cdc, gateway, logger,
          config={'CDC': {'name': 'CDC', 'ID': '1'}})
  clientsocket.send.assert_called_once_with(b'$ .\n1.5\n;')


def test_gateway_set(mock_objects, parser_mock):
  clientsocket, cdc, gateway, logger = mock_objects
  gateway.set.return_value = None
  parser_mock.return_value = ncrp.Command(
      '', 'S', '0', 'MOCK.PROPERTY', '', '', '', '2.0')
  handler(clientsocket, cdc, gateway, logger,
          config={'CDC': {'name': 'CDC', 'ID': '1'}})
  clientsocket.send.assert_called_once_with(b'$ .\n\n;')


def test_gateway_get(mock_objects, parser_mock):
  clientsocket, cdc, gateway, logger = mock_objects
  gateway.get.return_value = 2.0
  parser_mock.return_value = ncrp.Command(
      '', 'G', '0', 'MOCK.PROPERTY', '', '', '', '')
  handler(clientsocket, cdc, gateway, logger,
          config={'CDC': {'name': 'CDC', 'ID': '1'}})
  clientsocket.send.assert_called_once_with(b'$ .\n2.0\n;')


def test_parser_raises_error(mock_objects):
  clientsocket, cdc, gateway, logger = mock_objects
  with mock.patch('fgc_ncrp_handler.parser.parse') as mock_parser:
    mock_parser.side_effect = ncrp.errors.UnknownCmd()
    handler(clientsocket, cdc, gateway, logger,
            config={'CDC': {'name': 'CDC', 'ID': '1'}})
    clientsocket.send.assert_called_once_with(b'$ !\n9 unknown cmd\n;')


def test_parser_raises_unknown_exception(mock_objects):
  clientsocket, cdc, gateway, logger = mock_objects
  with mock.patch('fgc_ncrp_handler.parser.parse') as mock_parser:
    mock_parser.side_effect = Exception()
    handler(clientsocket, cdc, gateway, logger,
            config={'CDC': {'name': 'CDC', 'ID': '1'}})
    clientsocket.send.assert_called_once_with(b'$ !\n35 log waiting\n;')


def test_get_raises_error(mock_objects):
  clientsocket, cdc, gateway, logger = mock_objects
  cdc.get.side_effect = ncrp.errors.BadInteger()
  with mock.patch('fgc_ncrp_handler.parser.parse') as mock_parser:
    mock_parser.return_value = ncrp.Command(
        '', 'G', 'CDC', 'CALSYS.CDC.ADC.GAIN_1A', '', '', '', '')
    handler(clientsocket, cdc, gateway, logger,
            config={'CDC': {'name': 'CDC', 'ID': '1'}})
  clientsocket.send.assert_called_once_with(b'$ !\n18 bad integer\n;')


def test_set_raises_error(mock_objects):
  clientsocket, cdc, gateway, logger = mock_objects
  cdc.set.side_effect = ncrp.errors.BadInteger()
  with mock.patch('fgc_ncrp_handler.parser.parse') as mock_parser:
    mock_parser.return_value = ncrp.Command(
        '', 'S', 'CDC', 'CALSYS.CDC.ADC.GAIN_1A', '', '', '', '1.5')
    handler(clientsocket, cdc, gateway, logger,
            config={'CDC': {'name': 'CDC', 'ID': '1'}})
  clientsocket.send.assert_called_once_with(b'$ !\n18 bad integer\n;')


def test_unknown_cmd(mock_objects):
  clientsocket, cdc, gateway, logger = mock_objects
  with mock.patch('fgc_ncrp_handler.parser.parse') as mock_parser:
    mock_parser.return_value = ncrp.Command(
        '', 'S', 'CDC', 'CALSYS.CDC.ADC.GAIN_1A', '', '', '', '1.5')
    mock_parser.return_value.verb = 'X'
    handler(clientsocket, cdc, gateway, logger,
            config={'CDC': {'name': 'CDC', 'ID': '1'}})
  clientsocket.send.assert_called_once_with(b'$ !\n9 unknown cmd\n;')
