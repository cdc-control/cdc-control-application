# pylint: disable=missing-docstring


import mock
import pytest
import os

import cdc


@pytest.fixture
def mock_classes():
  patches = {
      'server': mock.patch('fgc_ncrp_server.Server'),
      'cdc': mock.patch('cdc.CDC'),
      'thread': mock.patch('threading.Thread'),
      'sysmon': mock.patch('cdc_sysmon.SystemMonitor'),
      'sleep': mock.patch('time.sleep', side_effect=exit)
  }

  mocks = {k: v.start() for k, v in patches.items()}
  yield mocks

  # Explicitly stop all patches
  for patch in patches.values():
    patch.stop()


def test_cdc_exception(mock_classes):
  with mock.patch('cdc.CDC', side_effect=Exception), pytest.raises(Exception):
    cdc.main(['cdc.py', 1905])


def test_cdc_invalid_port(mock_classes):
  with pytest.raises(SystemExit):
    cdc.main(['cdc.py', 'invalid_port'])
  mock_classes['server'].assert_called_with(
      mock.ANY, mock.ANY, mock.ANY, 1905, mock.ANY)


def test_cdc_valid_port(mock_classes):
  with pytest.raises(SystemExit):
    cdc.main(['cdc.py', 1905])
  mock_classes['server'].assert_called_with(
      mock.ANY, mock.ANY, mock.ANY, 1905, mock.ANY)


def test_cdc_out_of_range_port(mock_classes):
  with pytest.raises(SystemExit):
    cdc.main(['cdc.py', 70000])
  mock_classes['server'].assert_called_with(
      mock.ANY, mock.ANY, mock.ANY, 1905, mock.ANY)


def test_cdc_server_thread_dies(mock_classes):
  mock_classes['thread'].return_value.is_alive.return_value = False
  with pytest.raises(SystemExit):
    cdc.main(['cdc.py', 1905])
  mock_classes['server'].assert_called_with(
      mock.ANY, mock.ANY, mock.ANY, 1905, mock.ANY)


def test_cdc_hardware_init_fails(mock_classes):
  mock_classes['cdc'].return_value.hardware_init.side_effect = RuntimeError
  with pytest.raises(RuntimeError):
    cdc.main(['cdc.py', 1905])
  mock_classes['server'].assert_called_with(
      mock.ANY, mock.ANY, mock.ANY, 1905, mock.ANY)


@pytest.fixture(scope="session", autouse=True)
def cleanup_logs():
  yield
  # delete the log files
  for file in os.listdir('logs'):
    if file.endswith(".log"):
      os.remove(os.path.join('logs', file))
