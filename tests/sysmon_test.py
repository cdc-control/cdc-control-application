import sys
import os

import configparser

import mock
import pytest
from io import StringIO

from cdc_sysmon import SystemMonitor


@pytest.fixture
def mock_cdc_instance():
  return mock.MagicMock()


@pytest.fixture
def mock_logger():
  return mock.MagicMock()


@pytest.fixture
def system_monitor(mock_cdc_instance, mock_logger):
  mock_config = {
      'fadem': {
          'spi_number': "0",
          'spi_device': "0"
      },
      'primrel': {
          'spi_number': "1",
          'spi_device': "0"
      },
      'nullmeter': {
          'spi_number': "2",
          'spi_device': "0"
      },
      'sysmon': {
          'onewire_device_check': "True",
          'onewire_mem_address': "0x43c00000",
          'num_modules': "9",
          'modules_ini': "modules.ini",
          'max_temp': "80",
          'min_temp': "0",
          'voltage_max_alarm': "1.1",
          'voltage_min_alarm': "0.9"
      },
      'debug': {
          'sysmon_logging': 'False',
          'nulling_logging': 'True',
      },
      'logger': {
          'log_dir': 'logs',
          'log_level': 'DEBUG',
          'file_log_level': 'DEBUG',
          'console_log_level': 'DEBUG',
          'max_log_files': '10',
          'max_log_size': '1048576'
      }
  }
  with mock.patch('cdc_sysmon.devmem'):
    return SystemMonitor(mock_cdc_instance, mock_logger, mock_config)


def test_is_voltage_in_range_positive(system_monitor):
  voltage = 5.1
  nominal_voltage = 5.0
  assert system_monitor.is_voltage_in_range(voltage, nominal_voltage)


def test_is_voltage_in_range_negative(system_monitor):
  voltage = -16.0
  nominal_voltage = -15.0
  assert system_monitor.is_voltage_in_range(voltage, nominal_voltage)


def test_is_voltage_in_range_out_of_range(system_monitor):
  voltage = 6.0
  nominal_voltage = 5.0
  assert not system_monitor.is_voltage_in_range(voltage, nominal_voltage)


def test_read_calibrated_voltage(system_monitor):
  rail = '5V'
  system_monitor.read_raw_voltage = mock.Mock(return_value=5.0)
  system_monitor.get_calibration_gain = mock.Mock(return_value=0.9)
  assert system_monitor.read_calibrated_voltage(rail) == 5.0 * 0.9


def test_monitor_voltage_out_of_range(system_monitor):
  system_monitor.read_calibrated_voltage = mock.Mock(return_value=4.8)
  system_monitor.is_voltage_in_range = mock.Mock(return_value=False)
  system_monitor.monitor_voltage()
  assert system_monitor.cdc.state == system_monitor.cdc.CDCStates.ERROR


def test_monitor_temperature_missing_modules(system_monitor):
  system_monitor.num_modules = 9
  system_monitor.read_onewire_device_data = mock.Mock(return_value=[{}, {}, {}])
  with pytest.raises(ValueError):
    system_monitor.monitor_temperature()
  assert system_monitor.cdc.set_state.call_args_list == [
      mock.call(system_monitor.cdc.CDCStates.ERROR)
  ]


def test_monitor_temperature_out_of_range(system_monitor):
  system_monitor.num_modules = 3
  system_monitor.read_onewire_device_data = mock.Mock(return_value=[
      {'ID': '123', 'Temperature': 110, 'Name': 'Module A'},
      {'ID': '456', 'Temperature': 70, 'Name': 'Module B'},
      {'ID': '789', 'Temperature': 105, 'Name': 'Module C'}
  ])
  with pytest.raises(ValueError):
    system_monitor.monitor_temperature()

  assert system_monitor.cdc.set_state.call_args_list == [
      mock.call(system_monitor.cdc.CDCStates.ERROR),
  ]


def test_read_raw_voltage(system_monitor):
  rail = '5V'
  system_monitor.read_raw_adc = mock.Mock(return_value=1234)
  system_monitor.get_calibration_gain = mock.Mock(return_value=1.0)
  voltage = system_monitor.read_raw_voltage(rail)
  assert voltage == 1234 * system_monitor.cora_counts_to_volts * \
      system_monitor.voltage_rails[rail]['nominal_gain']


def test_get_calibration_gain_calibrated(system_monitor):
  rail = '5V'
  system_monitor.cdc.properties['CALSYS.DCM.PS05.CAL'].value = [0.9]
  calibration_gain = system_monitor.get_calibration_gain(rail)
  assert calibration_gain == 0.9


def test_get_calibration_gain_uncalibrated(system_monitor):
  rail = '5V'
  system_monitor.cdc.properties['CALSYS.DCM.PS05.CAL'].value = [0]
  calibration_gain = system_monitor.get_calibration_gain(rail)
  assert calibration_gain == 1


def test_is_temperature_in_range_within_range(system_monitor):
  temperature = 50
  assert system_monitor.is_temperature_in_range(temperature) == True


def test_is_temperature_in_range_out_of_range(system_monitor):
  temperature = 110
  assert system_monitor.is_temperature_in_range(temperature) == False


def test_read_onewire_device_data(system_monitor):
  mock_device_data = [
      {'ID': 123, 'Temperature': 50, 'Name': 'Module A'},
      {'ID': 456, 'Temperature': 55, 'Name': 'Module B'},
      {'ID': 789, 'Temperature': 60, 'Name': 'Module C'}
  ]
  system_monitor.onewire_lock = mock.MagicMock()
  system_monitor.onewire = mock.MagicMock()
  system_monitor.onewire.start_readout.return_value = 3
  system_monitor.onewire.get_device_data_by_index.side_effect = [
      {'ID': 123, 'Temperature': 50},
      {'ID': 456, 'Temperature': 55},
      {'ID': 789, 'Temperature': 60},
  ]
  system_monitor.modules_map = {
      123: 'Module A',
      456: 'Module B',
      789: 'Module C'
  }

  device_data = system_monitor.read_onewire_device_data()
  assert device_data == mock_device_data


@mock.patch.object(SystemMonitor, 'write_new_ids_to_ini')
def test_read_onewire_device_data_missing_modules(mocked_write_new_ids_to_ini, system_monitor):
    system_monitor.onewire_lock = mock.MagicMock()
    system_monitor.onewire = mock.MagicMock()
    system_monitor.onewire.start_readout.return_value = 3
    system_monitor.onewire.get_device_data_by_index.side_effect = [
        {'ID': 0x123, 'Temperature': 50},
        {'ID': 0x456, 'Temperature': 55},
        {'ID': 0x789, 'Temperature': 60},
    ]

    system_monitor.modules_map = {
        0x123: 'Module A',
        0x555: 'Module B',
        0x789: 'Module C'
    }

    device_data = system_monitor.read_onewire_device_data()
    assert device_data == [
        {'ID': 0x123, 'Temperature': 50, 'Name': 'Module A'},
        {'ID': 0x456, 'Temperature': 55},
        {'ID': 0x789, 'Temperature': 60, 'Name': 'Module C'}
    ]

def test_modules_ini_file_parsing(system_monitor):
  system_monitor.read_ids_from_ini(ini_file='modules.ini')
  assert system_monitor.modules_map == {
      11385099912896426024: 'nullmeter',
      12033618259470891048: 'primrel',
      5908722766250548264: 'lvpsu',
      8574853745653653800: '1t_driver',
      7493989835080814888: 'backplane',
      14195346080605196584: 'fadem',
      3098476598770918696: 'modamp',
      2017612688198891304: 'actfilt',
      2449958252426477352: 'controller'
  }


def test_read_raw_adc_with_mocked_file(system_monitor):
    # Mock the open function to return a file object with a read method
  mock_file = mock.mock_open(read_data="1234")
  with mock.patch('builtins.open', mock_file):
    rail = '5V'
    file_number = system_monitor.voltage_rails[rail]['rail_file']
    expected_file_path = f"/sys/bus/iio/devices/iio:device1/in_voltage{file_number}_raw"

    # Call the method under test
    result = system_monitor.read_raw_adc(rail)

    # Verify that the file was opened with the expected path
    mock_file.assert_called_once_with(expected_file_path, 'r')

    # Verify that the file was read and converted to an integer
    assert result == 1234


def test_read_raw_adc_error_with_mocked_file(system_monitor):
  # Mock the open function to raise a FileNotFoundError
  with mock.patch('builtins.open', side_effect=FileNotFoundError):
    rail = '5V'
    # Call the method under test
    with pytest.raises(FileNotFoundError):
      system_monitor.read_raw_adc(rail)


def test_print_monitored_voltages(system_monitor):
    # Redirect stdout to a StringIO object
  captured_output = StringIO()
  sys.stdout = captured_output

  # Mock the read_calibrated_voltage method to return a specific voltage
  system_monitor.read_calibrated_voltage = mock.Mock(
      side_effect=[4.8, 12.1, 15.0, -15.05])

  # Call the method under test
  system_monitor._print_monitored_voltages()

  # Reset stdout
  sys.stdout = sys.__stdout__

  # Verify the output
  expected_output = "5V: 4.8V (5.0V)\n12V: 12.1V (12.0V)\n+15V: 15.0V (15.0V)\n-15V: -15.05V (-15.0V)\n"
  assert captured_output.getvalue() == expected_output
