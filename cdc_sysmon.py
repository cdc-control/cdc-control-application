"""This file contains the system monitor class, which is responsible for
monitoring the voltage supply to the system and the temperature and presence of
all the modules.
It is a thread that is started by the main program and runs in the background.
It is responsible for monitoring the voltage supply to the system and setting
the CDC class state variable to CDCStates.ERROR if out of range (percentage).
It is also responsible for monitoring the temperature of the system and setting
the state to error if the temperature is out of range or if any module is
missing (9 onewire devices, configurable).
It reads the voltage from the XADC that is represented in the file system as a
file.
It reads the temperature from the onewire bus, that has python driver.
It gets the calibration data from the CDC class for the voltage.
The voltage monitor rails are 5V, 12V, +15V and -15V. """

import threading
import configparser
from time import sleep
import datetime

from drivers.onewire import onewire
from drivers.devmem import devmem


class SystemMonitor():
  """This class is responsible for monitoring the voltage supply to the system,
  the temperature and presence of all the modules. """

  def __init__(self, cdc_instance, logger, config):
    """
    Args:
        cdc_instance (CDC): The CDC instance  
        logger (Logger): The logger instance
        config (ConfigParser): The configuration
    """
    # we want to save the init timestamp of this class, to create a temperature logger that will name the log file with the init timestamp. we get the system time for that
    self.init_timestamp = datetime.datetime.now()

    # we save a log called temperature.log in the logs folder, with the init timestamp as the name. rather than relying on a logger, we just write to the file directly
    if config['debug']['sysmon_logging'].upper() == 'TRUE':
      self.sysmon_logging = True
      self.temperature_log_file = open(
          f"{config['logger']['log_dir']}/temperature_{self.init_timestamp.strftime('%Y-%m-%d_%H-%M-%S')}.log", "w+", encoding='utf-8')
      # we then close the file, and open it later when we want to write to it
      self.temperature_log_file.close()
      # we log also voltage rails
      self.voltage_log_file = open(
          f"{config['logger']['log_dir']}/voltage_{self.init_timestamp.strftime('%Y-%m-%d_%H-%M-%S')}.log", "w+", encoding='utf-8')
      # we then close the file, and open it later when we want to write to it
      self.voltage_log_file.close()
    else:
      self.sysmon_logging = False

    self.cdc = cdc_instance
    self.logger = logger
    self.onewire_lock = threading.Lock()
    self.onewire = onewire(
        devmem(int(config["sysmon"]["onewire_mem_address"], 0), 0x10000), 0)
    self.num_modules = int(config["sysmon"]["num_modules"])
    self.modules_ini = config["sysmon"]["modules_ini"]
    self.modules_map = self.read_ids_from_ini(self.modules_ini)
    self.max_temperature = int(config["sysmon"]["max_temp"])
    self.min_temperature = int(config["sysmon"]["min_temp"])
    self.max_voltage = float(config["sysmon"]["voltage_max_alarm"])
    self.min_voltage = float(config["sysmon"]["voltage_min_alarm"])
    self.onewire_devices = []
    self.cora_counts_to_volts = 3.3 / 4096  # nominal
    self.voltage_rails = {
        '5V': {
            'calibration_property': 'CALSYS.DCM.PS05.CAL',
            'nominal_voltage': 5.0,
            'nominal_gain': (2.2 + 2.2) / 2.2,
            'additive_offset': 0,
            'rail_file': '8',
            'last_read_voltage': 0.0},
        '12V': {
            'calibration_property': 'CALSYS.DCM.PS12.CAL',
            'nominal_voltage': 12.0,
            'nominal_gain': (12 + 2.2) / 2.2,
            'additive_offset': 0,
            'rail_file': '10',
            'last_read_voltage': 0.0},
        '+15V': {
            'calibration_property': 'CALSYS.DCM.PS15.CALPOS',
            'nominal_voltage': 15.0,
            'nominal_gain': (12 + 2.2) / 2.2,
            'additive_offset': 0,
            'rail_file': '9',
            'last_read_voltage': 0.0},
        '-15V': {
            'calibration_property': 'CALSYS.DCM.PS15.CALNEG',
            'nominal_voltage': -15.0,
            'nominal_gain': (12 + 2.2) / 2.2,
            'additive_offset': -18.5,
            'rail_file': '11',
            'last_read_voltage': 0.0}
    }

    self.fault_checkers = {
        'PBC_TMP': self.cdc.primrel.get_pbc_temp_fault,
        'PBC_CAL': lambda: not self.cdc.primrel.get_pbc_cal(),
        'PBC_CMPL': self.cdc.primrel.get_pbc_compliance,
        'PBC_CHR': lambda: not self.cdc.primrel.get_pbc_charge(),
        'VB_PWR': lambda: self.cdc.primrel.get_vb_status() == self.cdc.primrel.VBStatus.VB_OFF,
        'VB_TMP': lambda: self.cdc.primrel.get_vb_status() == self.cdc.primrel.VBStatus.VB_TEMP_FAULT,
        'PA_TMP': self.cdc.primrel.get_pwr_amp_status,
        'MOD_TMP': lambda: not self.cdc.primrel.get_mod_amp_status()
    }

    self.warning_checkers = {
        'PBC_TMP': self.cdc.primrel.get_pbc_temp_warning,
        'CLP_ON': lambda: False,
        'CLP_OFF': lambda: False
    }

  def run(self):
    """This is the main loop of the system monitor. It is responsible for
    calling the monitor_voltage, monitor_temperature and check_warnings_and_faults methods
    """

    while True:
      try:
        self.monitor_voltage()
      except Exception as exc:
        self.logger.error(
            f"System monitor error while monitoring voltage: {exc}")
      try:
        self.monitor_temperature()
      except Exception as exc:
        self.logger.error(
            f"System monitor error while monitoring temperature: {exc}")
      try:
        self.check_warnings_and_faults()
      except Exception as exc:
        self.logger.error(
            f"System monitor error while checking warnings and faults: {exc}")
      try:
        self.update_modules_caches()
      except Exception as exc:
        self.logger.error(
            f"System monitor error while updating modules caches: {exc}")
      sleep(5)

  def update_modules_caches(self):
    """This just calls get_state for fadem and primrel in order to update the state cache for the http server. This is very much non-critical, so skip if the lock is taken"""
    if not self.cdc.lock.locked():
      with self.cdc.lock:
        self.cdc.fadem.get_state()
        self.cdc.primrel.get_state()
        self.cdc.primrel.get_vb_status()
        self.cdc.primrel.get_on_off_relays()
        self.cdc.primrel.get_pol_relays()

  def check_warnings_and_faults(self):
    """This method is responsible for checking the warnings and faults of the system. 

    Raises:
        TimeoutError: If the lock on the onewire is not acquired, raise an exception
    """
    # execute the check_warnings_and_faults after getting the lock on the onewire.
    # To avoid the system monitor to hang, there is a timeout of 1 second.
    # If the lock is not acquired, the system monitor will skip the check_warnings_and_faults
    if self.onewire_lock.acquire(timeout=1):
      try:
        # Check faults
        for fault, fault_checker in self.fault_checkers.items():
          if fault_checker():  # If fault condition is met
            self.cdc.properties['STATUS.FAULTS'].set_fault(fault)
          else:  # If fault condition is not met
            self.cdc.properties['STATUS.FAULTS'].clear_fault(fault)

        # if there is any fault, set the cdc.set_fault_led
        if self.cdc.properties['STATUS.FAULTS'].value[0]:
          self.cdc.set_faults_led(1)
        else:
          self.cdc.set_faults_led(0)

        # Check warnings
        for warning, warning_checker in self.warning_checkers.items():
          if warning_checker():  # If warning condition is met
            self.cdc.properties['STATUS.WARNINGS'].set_warning(warning)
          else:  # If warning condition is not met
            self.cdc.properties['STATUS.WARNINGS'].clear_warning(warning)
      finally:
        self.onewire_lock.release()
    else:
      raise TimeoutError(
          "Timeout while acquiring the lock on the onewire")

  def _print_monitored_voltages(self):
    """This method is used for debugging purposes. It prints the monitored voltages."""
    for rail, properties in self.voltage_rails.items():
      calibrated_voltage = self.read_calibrated_voltage(rail)
      nominal_voltage = properties['nominal_voltage']
      print(f"{rail}: {calibrated_voltage}V ({nominal_voltage}V)")

  def monitor_voltage(self):
    """This method is responsible for monitoring the voltage supply to the system and setting the CDC class state variable to CDCStates.ERROR if out of range (percentage).
    """
    calibrated_voltage = {}
    for rail, properties in self.voltage_rails.items():
      calibrated_voltage[rail] = self.read_calibrated_voltage(rail)
      nominal_voltage = properties['nominal_voltage']
      if not self.is_voltage_in_range(calibrated_voltage[rail], nominal_voltage):
        # write to STDERR the cause of the error, and then set the state to ERROR
        self.logger.error(
            f'System Monitor: Voltage rail {rail} is out of range: {calibrated_voltage[rail]}V ({nominal_voltage}V)')
        self.cdc.state = self.cdc.CDCStates.ERROR

    if self.sysmon_logging:
      with self.voltage_log_file.open("a", encoding='utf-8') as self.voltage_log_file:
        # we begin the log line with the current timestamp
        self.voltage_log_file.write(
            f"{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\t")
        # then we write the voltage of each rail
        self.voltage_log_file.write("\t".join(
            [f"{rail}: {calibrated_voltage[rail]}V\t" for rail in self.voltage_rails]))
        # then we write a newline
        self.voltage_log_file.write("\n")

  def read_raw_adc(self, rail):
    """This method reads the raw ADC value from the file system.

    Args:
        rail (str): The voltage rail to read from. It can be 5V, 12V, +15V or -15V

    Returns:
        int: The raw ADC value
    """
    file_number = self.voltage_rails[rail]['rail_file']
    adc_file = f"/sys/bus/iio/devices/iio:device1/in_voltage{file_number}_raw"
    # try opening the file, if the file does not exist, return None
    with open(adc_file, 'r') as file:
      voltage = int(file.read())
    return voltage

  def read_raw_voltage(self, rail):
    """
    This method reads the raw voltage from the file system mapped ADC value.

    Args:
        rail (str): The voltage rail to read from. It can be 5V, 12V, +15V or -15V

    Returns:
        float: The raw voltage
    """
    counts = self.read_raw_adc(rail)
    nominal_gain = self.voltage_rails[rail]['nominal_gain']
    nominal_voltage = counts * self.cora_counts_to_volts * nominal_gain + \
        self.voltage_rails[rail]['additive_offset']
    return nominal_voltage

  def read_calibrated_voltage(self, rail):
    """
    This method reads the calibrated voltage from the file system mapped ADC value.

    Args:
        rail (str): The voltage rail to read from. It can be 5V, 12V, +15V or -15V

    Returns:
        float: The calibrated voltage

    """

    counts = self.read_raw_voltage(rail)
    calibration_gain = self.get_calibration_gain(rail)
    calibrated_voltage = counts * calibration_gain
    self.voltage_rails[rail]['last_read_voltage'] = calibrated_voltage
    return calibrated_voltage

  def get_calibration_gain(self, rail):
    """
    This method gets the calibration gain from the CDC class.

    Args:
        rail (str): The voltage rail to read from. It can be 5V, 12V, +15V or -15V

    Returns:
        float: The calibration gain

    """
    calibration_property = self.voltage_rails[rail]['calibration_property']
    gain = self.cdc.properties[calibration_property].value[0]
    if gain == 0:
      return 1
    return gain

  def is_voltage_in_range(self, voltage, nominal_voltage):
    """
    This method checks if the voltage is in range, by comparing it to the self.max_voltage and self.min_voltage attributes.

    Args:
        voltage (float): The voltage to check
        nominal_voltage (float): The nominal voltage

    Returns:
        bool: True if the voltage is in range, False otherwise

    """
    voltage_range_min = self.min_voltage * \
        nominal_voltage  # % below the nominal voltage
    voltage_range_max = self.max_voltage * \
        nominal_voltage  # % above the nominal voltage

    if nominal_voltage >= 0:  # For positive voltages
      return voltage >= voltage_range_min and voltage <= voltage_range_max
    else:  # For negative voltages
      return voltage <= voltage_range_min and voltage >= voltage_range_max

  def monitor_temperature(self):
    """This method is responsible for monitoring the temperature of the system and setting the state to error if the temperature is out of range or if any module is missing (9 onewire devices, configurable).


    Raises:
        TimeoutError: If the lock on the onewire is not acquired, raise an exception
    """

    data = self.read_onewire_device_data()

    if len(data) != self.num_modules:
      self.cdc.set_state(self.cdc.CDCStates.ERROR)
      self.logger.error(f"System Monitor: found {len(data)} modules, expected {self.num_modules}")
      raise ValueError(f"System Monitor: found {len(data)} modules, expected {self.num_modules}")

    if self.sysmon_logging:
      with open(self.temperature_log_file.name, "a", encoding='utf-8') as self.temperature_log_file:
        # we begin the log line with the current timestamp
        self.temperature_log_file.write(
            f"{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\t")
        # then we write the temperature of each module
        self.temperature_log_file.write("\t".join(
            [f"{device['Name'] if 'Name' in device else device['ID']}: {device['Temperature']}\t" for device in data]))
        # then we write a newline
        self.temperature_log_file.write("\n")

    for device in data:
      if not self.is_temperature_in_range(device["Temperature"]):
        self.logger.error(
            f'System Monitor: Temperature of module {device["Name"] if "Name" in device else device["ID"]} is out of range: {device["Temperature"]}C')
        self.cdc.set_state(self.cdc.CDCStates.ERROR)
        raise ValueError(
            f'System Monitor: Temperature of module {device["Name"] if "Name" in device else device["ID"]} is out of range: {device["Temperature"]}C')

  def read_onewire_device_data(self):
    with self.onewire_lock:
      device_count = self.onewire.start_readout()
      data = []
      found_devices_ids = []
      mismatched_devices = []

      for i in range(device_count):
        ow_dev = self.onewire.get_device_data_by_index(i)
        found_devices_ids.append(ow_dev['ID'])
        if ow_dev['ID'] in self.modules_map:
          data.append({
              "ID": ow_dev['ID'],
              "Temperature": ow_dev['Temperature'],
              "Name": self.modules_map[ow_dev['ID']]
          })
        else:
          mismatched_devices.append(ow_dev)
          self.cdc.logger.info(
              f'device ID {hex(ow_dev["ID"]).upper()} not found in modules.ini. Please add it to modules.ini and restart the CDC.')
          data.append(ow_dev)

      if len(mismatched_devices) == 1:
        # If only one device ID is mismatched, update the modules.ini file
        missing_id = next(ID for ID in self.modules_map if ID not in found_devices_ids)
        self.cdc.logger.info(f"Replacing missing ID {hex(missing_id).upper()} with new ID {hex(mismatched_devices[0]['ID']).upper()} in modules.ini.")
        self.modules_map[mismatched_devices[0]['ID']] = self.modules_map.pop(missing_id)
        changed_devices = [(self.modules_map[mismatched_devices[0]['ID']], mismatched_devices[0]['ID'])]
        self.write_new_ids_to_ini(changed_devices)
      elif len(mismatched_devices) > 1:
        # If more than one device ID is mismatched, log an error and do nothing
        self.cdc.logger.error("More than one device ID is mismatched. Cannot determine module swap.")
        
      self.onewire_devices = data
      return data


  def read_ids_from_ini(self, ini_file='modules.ini'):
    """This method reads the onewire IDs from the ini file and returns them as a dictionary.

    Args:
        ini_file (str, optional): The ini file to read from. Defaults to 'modules.ini'.

    Returns:
        dict: The dictionary containing the onewire IDs
    """

    config = configparser.ConfigParser()
    config.read(ini_file, encoding='utf-8')

    modules_map = {}
    if 'onewire_ids' in config.sections():
      for key in config['onewire_ids']:
        modules_map[key] = int(config['onewire_ids'][key], 16)

    # swap keys and values
    modules_map = {v: k for k, v in modules_map.items()}

    return modules_map

  def write_new_ids_to_ini(self, changed_devices):
    """This method writes the new IDs to the ini file.

    Args:
        changed_devices (list): A list of tuples containing the module name and the new ID
    """
    config = configparser.ConfigParser()
    config.read(self.modules_ini, encoding='utf-8')
    for module, new_id in changed_devices:
      config['onewire_ids'][module] = hex(new_id).upper()
    with open(self.modules_ini, 'w', encoding='utf-8') as configfile:
      config.write(configfile)

  def is_temperature_in_range(self, temperature):
    """This method checks if the temperature is in range, by comparing it to the self.max_temperature and self.min_temperature attributes.

    Args:
        temperature (float): The temperature to check

    Returns:
        bool: True if the temperature is in range, False otherwise

    """
    return temperature <= self.max_temperature and temperature >= self.min_temperature
