"FGC NCRP receiving server. It accepts a TCP connection from a client and creates a thread to handle the connection. Multiple clients can connect to the server at the same time. The server receives the reference for the CDC class and passes it to the client thread."
# The server passess the reference to the CDC class to the client thread. The client thread then uses the reference to call the CDC class methods to send the data to the CDC.

import socket
import queue
from threading import Thread
import fgc_ncrp_handler as handler


class Server:
  """FGC NCRP receiving server. It accepts a TCP connection from a client and queues the request with a cap. Multiple clients can connect to the server at the same time. The server receives the reference for the CDC class and passes it to the client thread.

  The server passess the reference to the CDC class to the client thread. The client thread then uses the reference to call the CDC class methods to send the data to the CDC.

  """

  def __init__(self, cdc, gateway, logger, port, config):
    """
    Initialize the Server object.

    Args:
      cdc: Reference to the CDC class.
      gateway: Reference to the gateway class.
      logger: Reference to the logger object.
      port: Port number to listen on.
      config: Reference to the config object.


    """

    self.cdc = cdc
    self._gateway = gateway
    self._logger = logger
    self._port = port
    self._config = config

    # Create a thread-safe queue with a cap
    self.queue_capacity = int(self._config['Server']['queue_capacity']) or 10
    self.client_queue = queue.Queue(maxsize=self.queue_capacity)

    # Start worker threads
    self.num_workers = int(self._config['Server']['num_workers']) or 2
    for _ in range(self.num_workers):
      worker_thread = Thread(target=self.worker_function)
      worker_thread.daemon = True  # Daemon threads exit when the program terminates
      worker_thread.start()

  def main(self):
    """Server function. It accepts a TCP connection from a client and queues the request.

    Runs in an infinite loop.

    """
    self._logger.debug("Server: Starting server on port: " + str(self._port))
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(('0.0.0.0', self._port))
    serversocket.listen(5)
    serversocket.settimeout(1)

    while True:
      self.accept_connection(serversocket)

  def accept_connection(self, serversocket: socket.socket):
    """Accepts a TCP connection from a client and queues the request.

    Args:
      serversocket: Socket object to accept the connection on.

    """
    try:
      client_socket, _ = serversocket.accept()
    except socket.timeout:
      return
    else:
      try:
        self._logger.debug(
            f"Server: Accepted connection from {client_socket.getpeername()}")

        # Put the client socket into the queue, with a timeout
        self.client_queue.put(
            (client_socket, self.cdc, self._gateway, self._logger, self._config), timeout=5)

      except queue.Full:
        self._logger.warn('Server: Client queue is full. Rejecting connection.')
        client_socket.sendall('SERVER BUSY'.encode())
        client_socket.close()

      except Exception as exc:
        self._logger.error(f"Exception occurred during client handling: {exc}")
        client_socket.sendall(f'UNHANDLED ERROR: {exc}'.encode())
        client_socket.close()

  def worker_function(self):
    """Worker function that handles client connections from the queue.

    Runs in an infinite loop.

    """
    while True:
      client_socket, cdc, gateway, logger, config = self.client_queue.get()
      try:
        handler.handler(client_socket, cdc, gateway, logger, config)
      except Exception as exc:
        logger.error(f"Exception occurred during client handling: {exc}")
      finally:
        client_socket.close()
        self.client_queue.task_done()
