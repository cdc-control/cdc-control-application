"""this is the main file for the CDC project.
it is a python script that is invoked at startup.
the TCP server thread is started first and accepts 
connections from the client. The tcp server thread then 
starts the client threads that interact with the CDC class
that is instanciated here. The CDC class is responsible 
for the actual CDC functionality.
this program also starts a PID thread for nulling the offset, 
a thread for monitoring the temperature of the system and 
a thread for monitoring the voltage supply to the system.
this program is in charge of monitoring the threads and 
restarting them if they fail. """

import threading
import time
import sys
import configparser

import fgc_ncrp_server

import cdc_http

import session_logger


# import the CDC class
from fgcd_cdc import CDC
from fgcd_gateway_serial import GatewaySerial


def set_system_time(cdc_ntp_server='0.ch.pool.ntp.org'):
  """This function sets the system time to the current time using the NTP protocol. If the NTP server is not reachable, the system time is not set and the function returns.

  Args:
      cdc_ntp_server (str, optional): the NTP server to use. Defaults to '0.ch.pool.ntp.org'.

  """

  try:
    import ntplib
    import os

    cdc_ntp_client = ntplib.NTPClient()
    response = cdc_ntp_client.request(cdc_ntp_server, version=3)
    os.system('date ' + time.strftime('%m%d%H%M%Y.%S',
              time.localtime(response.tx_time)))
  except Exception as exc:
    print(f'Failed to set system time, exception: {exc}')


def read_config_file():
  """read the config.ini file in the current directory and return the config parser object

  Returns:
      configparser.ConfigParser: the config parser object

  """
  config = configparser.ConfigParser()
  config.read('config.ini', encoding='utf-8')
  return config


def main(argv):
  """This is the main function of the CDC program, 
  the program should be invoked with the port number
  as the first argument. If no port number is given, 
  the default port number is 1905.

  Args:
      argv (list): list of arguments passed to the program. argv[0] is the name of the program, argv[1] is the port number

  Raises:
      exc: any exception raised by the CDC class init function is re-raised

  """

  config = read_config_file()

  set_system_time(config['NTP']['server'])
  # create the logger
  logger = session_logger.SessionLogger(config['logger'])

  logger.debug("CDC: Starting CDC program")

  logger.debug("CDC: Creating CDC class")
  try:
    cdc = CDC(logger, config)
  except Exception as exc:
    # print reason for failure and exit
    logger.error(
        "CDC: Failed to create CDC class, exiting. Exception: " + str(exc))
    raise exc  # re-raise the exception
  # else check that the second argument is a valid port number if it exists, if not default to 59999
  try:
    port = int(argv[1])
    if port < 0 or port > 65535:
      raise ValueError
  except (IndexError, ValueError):
    logger.warning("Invalid port number, using default port 1905")
    port = 1905

  logger.debug("CDC: Creating gateway")
  gateway = GatewaySerial()

  # TODO actually handle the thread failure
  def thread_failure_handler(args, logger):
    "This function is called when a thread dies, logs the reason and restarts the thread"
    logger.error(f'Thread died: {args}')

  # attach the logger too the failure handler
  threading.excepthook = lambda args: thread_failure_handler(args, logger)

  server = fgc_ncrp_server.Server(cdc, gateway, logger, port, config)

  # create a thread to handle the server
  server_thread = threading.Thread(target=server.main, daemon=True)
  # start the server thread, as main of the server file

  # create the http server
  http_server = threading.Thread(
      target=cdc_http.run_server, args=(cdc, cdc.sysmon), daemon=True)

  # try doing hardware init on the CDC before starting the server
  try:
    cdc.hardware_init()
  except Exception as exc:
    logger.error(
        f"CDC: Failed to initialize hardware, exiting. Exception: {exc}")
    raise exc

  http_server.start()
  server_thread.start()

  while True:
    if not server_thread.is_alive():
      server_thread = threading.Thread(target=server.main, daemon=True)
      server_thread.start()
    if not http_server.is_alive():
      http_server = threading.Thread(
          target=cdc_http.run_server, args=(cdc, cdc.sysmon), daemon=True)
      http_server.start()
    time.sleep(5)


# if invoked from the command line, run the main function
if __name__ == "__main__":
  main(sys.argv)
