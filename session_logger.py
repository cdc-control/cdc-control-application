"""This module contains the SessionLogger class."""

import logging
from logging.handlers import RotatingFileHandler
from collections import deque
from datetime import datetime
import threading


class SessionLogger:
  """This class implements the session logger. It is used to log the session data to a file and to the console."""

  def __init__(self, config):
    """Initialize the SessionLogger object.

    Args:
      config: Reference to the config object.

    Config:
      log_dir: Directory to store the log files.
      max_log_files: Maximum number of log files to keep.
      max_log_size: Maximum size of each log file.
      log_level: Log level for the logger.
      file_log_level: Log level for the file handler.
      console_log_level: Log level for the console handler.

    """
    self.log_dir = config["log_dir"] or './logs'
    current_datetime = datetime.now()
    self.current_log_file_name = f"session_{current_datetime.strftime('%Y-%m-%d_%H-%M-%S')}.log"
    self.max_log_files = int(config["max_log_files"]) or 10
    self.max_log_size = int(config["max_log_size"]) or 1024 * 1024
    self.log_buffer = deque(maxlen=int(config["max_log_files"]) or 10)
    self.logger_level_name = config["log_level"] or 'DEBUG'
    self.file_level_name = config["file_log_level"] or 'DEBUG'
    self.console_level_name = config["console_log_level"] or 'DEBUG'
    self.logger = self._setup_logger()
    self.lock = threading.Lock()

  def _setup_logger(self):
    """Setup the logger. It creates the logger object and adds the file and console handlers."""

    logger = logging.getLogger('session_logger')
    logger.setLevel(self.logger_level_name)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    log_file_path = f"{self.log_dir}/{self.current_log_file_name}"

    file_handler = RotatingFileHandler(
        log_file_path, maxBytes=self.max_log_size, backupCount=self.max_log_files)
    file_handler.setLevel(self.file_level_name)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(self.console_level_name)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    return logger

  def _log(self, level, message):
    """Log the message to the logger object, at the specified level.

    Args:
      level: Log level.
      message: Message to log.

    """
    with self.lock:
      getattr(self.logger, level)(message)

  def debug(self, message):
    """Log the message at the debug level."""
    self._log('debug', message)

  def info(self, message):
    """Log the message at the info level."""
    self._log('info', message)

  def warning(self, message):
    """Log the message at the warning level."""
    self._log('warning', message)

  def error(self, message):
    """Log the message at the error level."""
    self._log('error', message)

  def retrieve_logs(self):
    """Retrieve the logs from the current log file."""
    log_lines = []
    log_file_path = f"{self.log_dir}/{self.current_log_file_name}"
    with self.lock:
      with open(log_file_path, 'r', encoding='utf-8') as log_file:
        log_lines.extend(log_file.readlines())
      self.log_buffer.extend(log_lines)
    return log_lines

  def print_logs(self):
    """Print the logs from the current log file."""
    with self.lock:
      for log_line in self.log_buffer:
        print(log_line.strip())
